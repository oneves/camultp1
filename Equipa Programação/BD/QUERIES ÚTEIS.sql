				
--------------------------------------------------------------------------------------------------------
------------- OBTER TODAS AS COMBINAÇÕES POSSÍVEIS DE CONFIGURAÇÕES DO PÚBLICO ALVO --------------------
--------------------------------------------------------------------------------------------------------
SELECT DISTINCT base.CCnf_FK_IdChallenge,
				ageRange.ARan_PK_IdAgeRange, 
				ageRange.ARan_DescAgeRange, 
				ageRange.ARan_MinRange, 
				ageRange.ARan_MaxRange,
				jobSec.JobS_PK_IdJobSector, 
				jobSec.JobS_DescJobSector, 
				jobSec.JobS_DescJobSector_ENG,
				eduLev.EduL_PK_IdEducationLevel, 
				eduLev.EduL_DescEducationLevel, 
				eduLev.EduL_DescEducationLevel_ENG,
				gender.Gend_PK_IdGender, 
				gender.Gend_DescGender, 
				gender.Gend_DescGender_ENG,
				marit.MStt_PK_IdMaritalStatus, 
				marit.MStt_DescMaritalSatus, 
				marit.MStt_DescMaritalSatus_ENG,
				country.Cntr_PK_IdCountry, 
				country.Cntr_DescCountry, 
				country.Cntr_DescCountry_ENG,
				city.City_PK_IdCity, 
				city.City_FK_IdCountry, 
				city.City_DescCity, 
				city.City_DescCity_ENG,
				child.CRan_PK_IdChildrenRange, 
				child.CRan_DescChildrenRange, 
				child.CRan_MinRange, 
				child.CRan_MaxRange
FROM TChallengeTargetConfig as base
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, ARan_PK_IdAgeRange, ARan_DescAgeRange, ARan_MinRange, ARan_MaxRange
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType
			INNER JOIN TAgeRange ON CSct_PK_IdSectorType = 1
									AND CCnf_IdSector = ARan_PK_IdAgeRange) as ageRange
ON base.CCnf_FK_IdChallenge = ageRange.CCnf_FK_IdChallenge
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, JobS_PK_IdJobSector, JobS_DescJobSector, JobS_DescJobSector_ENG 
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType					
			INNER JOIN TJobSectors ON CSct_PK_IdSectorType = 2
									AND CCnf_IdSector = JobS_PK_IdJobSector) as jobSec
ON base.CCnf_FK_IdChallenge = jobSec.CCnf_FK_IdChallenge
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, EduL_PK_IdEducationLevel, EduL_DescEducationLevel, EduL_DescEducationLevel_ENG  
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType						
			INNER JOIN TEducationLevel ON CSct_PK_IdSectorType = 3
								AND CCnf_IdSector = EduL_PK_IdEducationLevel) as eduLev
ON base.CCnf_FK_IdChallenge = eduLev.CCnf_FK_IdChallenge
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, Gend_PK_IdGender, Gend_DescGender, Gend_DescGender_ENG
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType						
			INNER JOIN TGender ON CSct_PK_IdSectorType = 4
								AND CCnf_IdSector = Gend_PK_IdGender) as gender
ON base.CCnf_FK_IdChallenge = gender.CCnf_FK_IdChallenge
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, MStt_PK_IdMaritalStatus, MStt_DescMaritalSatus, MStt_DescMaritalSatus_ENG 
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType						
			INNER JOIN TMaritalStatus ON CSct_PK_IdSectorType = 5
								AND CCnf_IdSector = MStt_PK_IdMaritalStatus) as marit
ON base.CCnf_FK_IdChallenge = marit.CCnf_FK_IdChallenge								
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, Cntr_PK_IdCountry, Cntr_DescCountry, Cntr_DescCountry_ENG 
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType						
			INNER JOIN TCountry ON CSct_PK_IdSectorType = 6
								AND CCnf_IdSector = Cntr_PK_IdCountry) as country
ON base.CCnf_FK_IdChallenge = country.CCnf_FK_IdChallenge	
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, City_PK_IdCity, City_FK_IdCountry, City_DescCity, City_DescCity_ENG
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType	
			INNER JOIN TCity ON CSct_PK_IdSectorType = 7
								AND CCnf_IdSector = City_PK_IdCity) as city
ON base.CCnf_FK_IdChallenge = city.CCnf_FK_IdChallenge
LEFT JOIN (SELECT CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector, CRan_PK_IdChildrenRange, CRan_DescChildrenRange, CRan_MinRange, CRan_MaxRange 
			FROM TChallengeTargetConfig
			INNER JOIN TTChallengeSectors ON CCnf_FK_IdSectorType = CSct_PK_IdSectorType	
			INNER JOIN TChildrenRange ON CSct_PK_IdSectorType = 9
								AND CCnf_IdSector = CRan_PK_IdChildrenRange) as child
ON base.CCnf_FK_IdChallenge = child.CCnf_FK_IdChallenge
					
					

--------------------------------------------------------------------------------------------------------
------------- USERS QUE COINCIDEM COM O PÚBLICO ALVO --------------------
--------------------------------------------------------------------------------------------------------					
SELECT Chal_PK_IdChallenge, 
		Chal_Title, 
		Chal_Title_ENG, 
		Chal_Description, 
		Chal_Description_ENG, 
		Chal_BeginDate, 
		Chal_EndDate, 
		Chal_VoucherURL,
		Chal_LogoURL,
		Chal_InsertedOn, 
		Chal_AllowGuests,
		User_PK_IdUser,
		User_Name,
		User_UserName,
		User_Email,
		User_TotalPoints
FROM VChallengeTarget
INNER JOIN TChallenge ON Chal_PK_IdChallenge = CCnf_FK_IdChallenge					
INNER JOIN TUsers ON (ARan_PK_IdAgeRange IS NULL OR (ARan_MinRange IS NOT NULL AND ARan_MaxRange IS NOT NULL AND (DATEDIFF(CURRENT_DATE,User_DateOfBirth)/365) BETWEEN ARan_MinRange AND ARan_MaxRange))
				AND (JobS_PK_IdJobSector IS NULL OR JobS_PK_IdJobSector = User_FK_IdJobSector)
				AND (EduL_PK_IdEducationLevel IS NULL OR EduL_PK_IdEducationLevel = User_FK_IdEducationLevel)
				AND (Gend_PK_IdGender IS NULL OR Gend_PK_IdGender = User_FK_IdGender)
				AND (MStt_PK_IdMaritalStatus IS NULL OR MStt_PK_IdMaritalStatus = User_IdMaritalStatus)
                AND (Cntr_PK_IdCountry IS NULL OR Cntr_PK_IdCountry = User_FK_IdCountry)
				AND (City_PK_IdCity IS NULL OR City_PK_IdCity = User_FK_IdCity)
				AND (CRan_PK_IdChildrenRange IS NULL OR (ARan_MinRange IS NOT NULL AND ARan_MaxRange IS NOT NULL AND User_NumOfChildren BETWEEN CRan_MinRange AND CRan_MaxRange))
WHERE Chal_DeletedOn IS NULL
AND User_DeletedOn IS NULL

				
				


