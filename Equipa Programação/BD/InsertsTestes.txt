INSERT INTO TCountry (Cntr_DescCountry, Cntr_DescCountry_ENG) VALUES ('Portugal','Portugal')

INSERT INTO TCity (`City_FK_IdCountry`,`City_DescCity`,`City_DescCity_ENG`) VALUES (1, 'Porto', 'Oporto'), (1, 'Lisboa', 'Lisbon')

INSERT INTO `TEducationLevel` (`EduL_DescEducationLevel`,`EduL_DescEducationLevel_ENG`) 
VALUES ('Educação primária','	Primary education'),
('Ensino preparatório','Lower secondary education'),
('Ensino secundário','Upper secondary education'),
('Ensino pós-secundário não superior','Post-secondary non-tertiary education'),
('Licenciatura ou equivalente','Bachelor or equivalent'),
('Mestrado ou equivalente','Master or equivalent'),
('Doutoramento ou equivalente','Doctoral or equivalent')

INSERT INTO `TGender` (`Gend_DescGender`,`Gend_DescGender_ENG`) VALUES ('Feminino', 'Female'),  ('Masculino', 'Male'),  ('Outro', 'Other')

INSERT INTO `TJobSectors` (`JobS_DescJobSector`,`JobS_DescJobSector_ENG`)
VALUES
('Contabilidade, banca e finanças','Accountancy, banking and finance'),
('Negócios, consultoria e gestão','Business, consulting and management'),
('Caridade e trabalho voluntário','Charity and voluntary work'),
('Artes e Design','Creative arts and design'),
('Energia e serviços públicos','Energy and utilities'),
('Engenharia e fabrico','Engineering and manufacturing'),
('Ambiente e agricultura','Environment and agriculture'),
('Cuidados de saúde','Healthcare'),
('Tecnologias da Informação','Information technology'),
('Direito','Law'),
('Forças da lei e segurança','Law enforcement and security'),
('Lazer, desporto e turismo','Leisure, sport and tourism'),
('Marketing, publicidade e Relações Públicas','Marketing, advertising and PR'),
('Mídia e Internet','Media and internet'),
('Imóveis e Construção','Property and construction'),
('Serviços Públicos e Administração','Public services and administration'),
('recrutamento e RH','Recruitment and HR'),
('Retalho','Retail'),
('Vendas','Sales'),
('Ciência e Farmacêuticos','Science and pharmaceuticals'),
('Assistência Social','Social care'),
('Ensino e Educação','Teaching and education'),
('Transporte e Logística','Transport and logistics')

INSERT INTO `TMaritalStatus` (`MStt_DescMaritalSatus`,`MStt_DescMaritalSatus_ENG`) VALUES
('Casado(a)','Married'),
('União de facto','Living common law'),
('Víuvo(a)','Widowed'),
('Separado(a)','Separated'),
('Divorciado(a)','Divorced'),
('Solteiro(a)','Single')

INSERT INTO `TPets` (`Pets_DescPet`,`Pets_DescPet_ENG`) VALUES 
('Cão','Dog'),
('Gato','Cat'),
('Pássaro','Bird'),
('Peixe','Fish')

INSERT INTO `TTMedia` (`TMed_DescMediaType`,`TMed_DescMediaType_ENG`) VALUES 
('Texto','Text'), 
('Imagem','Image'),
('Vídeo','Video'),
('Áudio','Audio')

INSERT INTO `TTUsers` (`TUsr_DescTypeUser`,`TUsr_DescTypeUser_ENG`) VALUES ('Adminsitrador','Administrator'), ('Investigador','Investigator'), ('Utilizador','User')



INSERT INTO `TUsers` (

`User_PK_IdUser` ,
`User_FK_IdTypeUser` ,
`User_Name` ,
`User_Email` ,
`User_UserName` ,
`User_Password` ,
`User_DateOfBirth` ,
`User_FK_IdJobSector` ,
`User_FK_IdEducationLevel` ,
`User_FK_IdGender` ,
`User_IdMaritalStatus` ,
`User_FK_IdCountry` ,
`User_FK_IdCity` ,
`User_ZipCode` ,
`User_NumOfChildren` ,
`User_InsertedOn` ,
`User_DeletedOn` ,
`User_TotalPoints`
)
VALUES (
NULL ,  '1',  'Alexandre Bastos', '1970133@isep.ipp.pt',  'admin',  '123', NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL ,  '2016-03-28 13:30:10', NULL , NULL
)

INSERT INTO `camul`.`TUsers` (`User_PK_IdUser`, `User_FK_IdTypeUser`, `User_Name`, `User_Email`, `User_UserName`, `User_Password`, `User_DateOfBirth`, `User_FK_IdJobSector`, `User_FK_IdEducationLevel`, `User_FK_IdGender`, `User_IdMaritalStatus`, `User_FK_IdCountry`, `User_FK_IdCity`, `User_ZipCode`, `User_NumOfChildren`, `User_InsertedOn`, `User_DeletedOn`, `User_TotalPoints`) VALUES (NULL, '2', 'Teste Investigador', 'investigador@camul.pt', 'invest', '123', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-28 14:07:00', NULL, NULL);

INSERT INTO `camul`.`TUsers` (`User_PK_IdUser`, `User_FK_IdTypeUser`, `User_Name`, `User_Email`, `User_UserName`, `User_Password`, `User_DateOfBirth`, `User_FK_IdJobSector`, `User_FK_IdEducationLevel`, `User_FK_IdGender`, `User_IdMaritalStatus`, `User_FK_IdCountry`, `User_FK_IdCity`, `User_ZipCode`, `User_NumOfChildren`, `User_InsertedOn`, `User_DeletedOn`, `User_TotalPoints`) VALUES (NULL, '3', 'User1', 'user1@camul.pt', 'user1', '123', '1955-07-01', '6', '2', '1', '5', '1', '2', '4325-124', '3', '2016-03-28 14:31:00', NULL, NULL);

INSERT INTO `camul`.`TUsers` (`User_PK_IdUser`, `User_FK_IdTypeUser`, `User_Name`, `User_Email`, `User_UserName`, `User_Password`, `User_DateOfBirth`, `User_FK_IdJobSector`, `User_FK_IdEducationLevel`, `User_FK_IdGender`, `User_IdMaritalStatus`, `User_FK_IdCountry`, `User_FK_IdCity`, `User_ZipCode`, `User_NumOfChildren`, `User_InsertedOn`, `User_DeletedOn`, `User_TotalPoints`) VALUES (NULL, '3', 'User2', 'user2@camul.pt', 'user2', '123', '1963-08-02', '11', '2', '1', '5', '1', '2', '4325-124', '3', '2016-03-28 14:31:00', NULL, NULL);

INSERT INTO `TTMediaMetaData` (`TMDt_DescMediaMetaData`,`TMDt_DescMediaMetaData_ENG`) VALUES 
('Contar caras por faixa etária','Count faces by age'),
('Contar uma palavra','Count a word'),
('Contar emoções','Count emotions'),
('Contar género','Count gender')

INSERT INTO `TTMediaMetaDataDetails` (`MDDt_DescMediaMetaDataDetails`,`MDDt_DescMediaMetaDataDetails_ENG`)
VALUES
('Raiva','Anger'), 
('Desprezo','Contempt'), 
('Desgosto','Disgust'), 
('Medo','Fear'), 
('Felicidade','Happiness'), 
('Neutro','Neutral'), 
('Tristeza','Sadness'), 
('Surpresa','Surprise')

INSERT INTO `TTMediaMetaDataDetails` (`MDDt_DescMediaMetaDataDetails`,`MDDt_DescMediaMetaDataDetails_ENG`,`MDDt_MinValue`,`MDDt_MaxValue`)
VALUES
('Contar pessoas com menos de 18 anos', 'Count people with less than 18 years',NULL,'18'),
('Contar pessoas entre 18 e 40 anos','Count people between 18 and 40 years','18','40'),
('Contar pessoas entre 40 e 65 anos','Count people between 40 and 65 years','40','65'),
('Contar pessoas com mais de 65 anos','Count people over 65 years','65',NULL),
('Contar uma palavra', 'Count a word', NULL, NULL),
('Masculino', 'Male',NULL,NULL),
('Feminino','Female',NULL,NULL)

INSERT INTO `TRMediaMetaData` 
(`TRMD_XK_IdMediaType`, `TRMD_XK_IdMediaMetaData`, `TRMD_XK_IdMediaDataDetails`) 
VALUES 
(1,2,13),
(4,2,13),
(2,1,9),
(2,1,10),
(2,1,11),
(2,1,12),
(2,3,1),
(2,3,2),
(2,3,3),
(2,3,4),
(2,3,5),
(2,3,6),
(2,3,7),
(2,3,8),
(2,4,14),
(2,4,15),
(3,1,9),
(3,1,10),
(3,1,11),
(3,1,12),
(3,3,1),
(3,3,2),
(3,3,3),
(3,3,4),
(3,3,5),
(3,3,6),
(3,3,7),
(3,3,8),
(3,4,14),
(3,4,15)

INSERT INTO `camul`.`TChallenge` 
(`Chal_PK_IdChallenge`, `Chal_Title`, `Chal_Title_ENG`, `Chal_Description`, 
`Chal_Description_ENG`, `Chal_BeginDate`, `Chal_EndDate`, `Chal_DeletedOn`, `Chal_FK_IdUserInvestigator`, 
`Chal_VoucherURL`, `Chal_LogoURL`, `Chal_InsertedOn`) VALUES 
(NULL, 'Teste ao Challenge 1', 'Teste ao Challenge 1 em Inglês', 
'Descrição do Teste ao Challenge 1', 'Descrição do Teste ao Challenge 1 em Inglês', 
'2016-03-20', '2016-03-31', NULL, '2', 'http://someURL.jpg', 'http://someURL.jpg', '2016-03-28 17:06:06');

INSERT INTO `TTChallengeSectors` (`CSct_DescSectorType`,`CSct_DescSectorType_ENG`,`CSct_TableNameInfo`)
VALUES ('Faixa etária', 'Age range','TAgeRange'),
('Setor de emprego','Job sector','TJobSectors'),
('Nível educacional','Education level','TEducationLevel'),
('Género','Gender','TGender'),
('Estado civil','Matrimonial status','TMaritalStatus'),
('País','Country','TCountry'),
('Cidade','City','TCity'),
('Código Postal','Zip Code',NULL),
('N.º de filhos','Number of children','TChildrenRange')

INSERT INTO `TAgeRange` (`ARan_DescAgeRange`,`ARan_MinRange`,`ARan_MaxRange`)
VALUES ('[0-15]',0,15),
('[16-18]',16,18),
('[19-25]',19,25),
('[26-39]',26,39),
('[40-65]',40,65),
('[66-79]',66,79),
('[80-99]',80,99)

INSERT INTO `TChildrenRange` (`CRan_DescChildrenRange`,`CRan_MinRange`,`CRan_MaxRange`)
VALUES ('[0]', 0, 0),
('[1]', 1, 1),
('[2-3]', 2, 3),
('[4+]', 4, 1000)

INSERT INTO TChallengeTargetConfig VALUES 
(1,1,2), /*TAgeRange*/
(1,1,3), /*TAgeRange*/
(1,1,4), /*TAgeRange*/
(1,2,0), /*TJobSectors -- 0 Todos*/
(1,3,3), /*TEducationLevel*/
(1,3,5), /*TEducationLevel*/
(1,4,0), /*TGender -- 0 Todos*/
(1,5,6), /*TMaritalStatus*/
(1,6,1), /*TCountry*/
(1,7,1), /*TCity*/
(1,7,2), /*TCity*/
(1,8,NULL),
(1,9,0) /*TChildrenRange*/

INSERT INTO `TChallengeResultsConfig` (`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_Values`) VALUES (1, 2, 3, ''),
(1, 3, 3, ''),
(1, 4, 2, 'Gostei;Amei;Comprei')

INSERT INTO `camul`.`TChallengeResults` (`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`) VALUES 
(1, 3, 3, 1, 4, 0), /*Raiva*/
(1, 3, 3, 2, 4, 0), /*Desprezo*/
(1, 3, 3, 3, 4, 0), /*Desgosto*/
(1, 3, 3, 4, 4, 0), /*Medo*/
(1, 3, 3, 5, 4, 2), /*Felicidade*/
(1, 3, 3, 6, 4, 0), /*Neutro*/
(1, 3, 3, 7, 4, 0), /*Tristeza*/
(1, 3, 3, 8, 4, 1), /*Surpresa*/


INSERT INTO `camul`.`TChallengeResults` (`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`) VALUES 
(1, 3, 3, 1, 4, 0), /*Raiva*/
(1, 3, 3, 2, 4, 0), /*Desprezo*/
(1, 3, 3, 3, 4, 0), /*Desgosto*/
(1, 3, 3, 4, 4, 0), /*Medo*/
(1, 3, 3, 5, 4, 2), /*Felicidade*/
(1, 3, 3, 6, 4, 0), /*Neutro*/
(1, 3, 3, 7, 4, 0), /*Tristeza*/
(1, 3, 3, 8, 4, 1), /*Surpresa*/
(1, 4, 2, 13, 4,2) /*Contar uma palavra*/

SELECT * 
FROM  `TRMediaMetaData` 
INNER JOIN TTMedia ON TMed_PK_IdMediaType = TRMD_XK_IdMediaType
INNER JOIN TTMediaMetaData ON TMDt_PK_IdMediaMetaData = TRMD_XK_IdMediaMetaData
INNER JOIN TTMediaMetaDataDetails ON MDDt_PK_IdMediaMetaDataDetails = TRMD_XK_IdMediaDataDetails