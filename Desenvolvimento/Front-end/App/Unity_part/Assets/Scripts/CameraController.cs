﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class CameraController : MonoBehaviour
{

    private WebCamTexture webcamTexture;

    // window where the input image is shown
    public RawImage videoInput;

    // window where the picture taken is shown
    //public RawImage picture;
    // convert frames to base64
    private List<string> base64_1 = new List<string>();
    string base64 = "";
    //audio base64
    string audiobase64 = "";
    string audiolang = "en-EN";
    // windows where the choosen frames from the video are shown
    //private List<RawImage> frames = new List<RawImage>();
    private List<string> textures = new List<string>();
    private float frameTimer;
    private int i;

    //A boolean that flags whether there's a connected microphone
    private bool micConnected = false;

    //The maximum and minimum available recording frequencies
    private int minFreq;
    private int maxFreq;

    //A handle to the attached AudioSource
    private AudioSource goAudioSource;



    private string audiost = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/audioAnalysisURL";
    private string text = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/textAnalysisURL";
    //private string image = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/imageAnalysis";
    private string image = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/imageAnalysisURL";
    private string video = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/videoAnalysis";


    int idCH = Challenges.idCH;
    int idUser = Login.IdUser;
    private List<ChallengesInfo> challengeList = STlist.GetList();

    public Text txt;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public InputField inp1;
    public Dropdown drop1;
    public Text error;
    bool sendvid = false;
    bool sendimag = false;
    bool sendtxt = false;
    bool sendaud = false;
    private int idCHinUse;
    private int j;
    private float frameTimerA;
    string[] tagsArray;
    string selectedtag = "";
    string imgname = "";
    string imgpath = "";
    private List<string> imgpath_mov = new List<string>();
    string imgname2;
    string audiopath;
    private List<string> chal_true = new List<string>();
    string[] TPch;
    private Lang LMan;
    public void OnEnable()
    {
        LMan = new Lang(LangChange.currentLang);

    }

    void OnDestroy()
    {
        webcamTexture.Stop();
    }
    // Use this for initialization
    void Start()
    {

        for (int k = 0; k <= challengeList.Count - 1; k++)
        {
            if (challengeList[k].Id == idCH)
                idCHinUse = k;
        }

        string types = challengeList[idCHinUse].Type;
        types = types.TrimStart('[');
        types = types.TrimEnd(']');
        TPch = types.Split(',');
        foreach (string t in TPch)
        {
            //Debug.Log(t);
            if (t == "1")
            {
                inp1.gameObject.SetActive(true);
                sendtxt = true;
            }
            else if (t == "2")
            {
                btn2.gameObject.SetActive(true);
                sendimag = true;

            }
            else if (t == "3")
            {
                btn1.gameObject.SetActive(true);
                sendvid = true;
            }
            else if (t == "4")
            {
                btn3.gameObject.SetActive(true);
                sendaud = true;
            }

        }
        string tags = challengeList[idCHinUse].Tags;
        Debug.Log("Tags: " + tags);
        tagsArray = tags.Split(',');

        frameTimer = 1f;
        i = 6;
        j = 11;


        webcamTexture = new WebCamTexture();

        videoInput.texture = webcamTexture;
        webcamTexture.Play();

        //Check if there is at least one microphone connected
        if (Microphone.devices.Length <= 0)
        {
            //Throw a warning message at the console if there isn't
            Debug.LogWarning("Microphone not connected!");
            error.text = "Microphone not connected!";
        }
        else //At least one microphone is present
        {
            //Set 'micConnected' to true
            micConnected = true;

            //Get the default microphone recording capabilities
            Microphone.GetDeviceCaps(null, out minFreq, out maxFreq);

            //According to the documentation, if minFreq and maxFreq are zero, the microphone supports any frequency...
            if (minFreq == 0 && maxFreq == 0)
            {
                //...meaning 44100 Hz can be used as the recording sampling rate
                maxFreq = 44100;
            }

            //Get the attached AudioSource component
            goAudioSource = this.GetComponent<AudioSource>();

        }
        fillCombo();
    }

    public void change()
    {
        if (LangChange.currentLang == "EN")
        {
            fillCombo();
            audiolang = "en-EN";

        }
        else if (LangChange.currentLang == "PT")
        {
            fillCombo();
            audiolang = "pt-PT";
        }

    }
    // Update is called once per frame
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("main_title_txt");
        btn1.GetComponentInChildren<Text>().text = LMan.getString("video_txt");
        btn2.GetComponentInChildren<Text>().text = LMan.getString("image_txt");
        btn3.GetComponentInChildren<Text>().text = LMan.getString("audio_txt");
        btn4.GetComponentInChildren<Text>().text = LMan.getString("send_txt");

        if (i < 5)
        {
            frameTimer -= Time.deltaTime;
            if (frameTimer <= 0)
            {
                string texture = takeFrame(i);
                error.text = "Recording";
                //frames[i].texture = texture;
                textures.Add(texture);
                base64_1.Add("http://10.8.154.245/mobilediary/uploads/" + textures[i]);
                frameTimer = 1f;
                i++;
            }
        }
        else if (i == 5)
        {
            i++;
            error.text = "";
            // convert frames to base64
            //base64_1 = convertTextureToBase64(textures[0]);
            //base64_2 = convertTextureToBase64(textures[1]);
            //base64_3 = convertTextureToBase64(textures[2]);
            //base64_4 = convertTextureToBase64(textures[3]);
            //base64_5 = convertTextureToBase64(textures[4]);
        }

        if (j < 10)
        {
            frameTimerA -= Time.deltaTime;
            if (frameTimerA <= 0)
            {
                error.text = "Recording";
                frameTimerA = 1f;
                j++;
            }
        }
        else if (j == 10)
        {
            j++;

            Microphone.End(null); //Stop the audio recording
            audiopath = Application.persistentDataPath + "/" + idUser + "-" + idCH;
            SavWav.Save(audiopath, goAudioSource.clip);
            Byte[] bytes = File.ReadAllBytes(audiopath + ".wav");
            //audiobase64 = convertAudioToBase64(ConvertAudio(goAudioSource.clip));
            //audiobase64 = "http://10.8.154.245/mobilediary/uploads/" + idCHinUse + "-" + idUser+".wav";
            //var f= File.CreateText("audiofile.txt");
            //f.Write(audiobase64);
            //f.Close();
            error.text = "";

        }

        if (chal_true.Count == TPch.Length)
        {
            bool cctrue = false;
            foreach (string ctrue in chal_true)
            {
                if (ctrue == "true")
                    cctrue = true;
                if (ctrue == "false")
                    cctrue = false;
            }

            if (cctrue)
            {
                if (idUser > 0)
                {
                    SceneManager.LoadScene("Final");
                }
                else
                {
                    SceneManager.LoadScene("Final2");
                }
            }
            else if (cctrue == false)
            {
                SceneManager.LoadScene("Final3");
            }

        }
    }
    public void takePicture()
    {
        Texture2D photo = new Texture2D(webcamTexture.width, webcamTexture.height);
        photo.SetPixels(webcamTexture.GetPixels());
        photo.Apply();


        photo = rotateTexture(photo);
        photo = rotateTexture(photo);
        photo = rotateTexture(photo);
        //picture.texture = photo;
        byte[] bytes = photo.EncodeToPNG();
        imgpath = Application.persistentDataPath + "/" + idUser + "-" + idCH + ".png";
        File.WriteAllBytes(imgpath, bytes);
        imgname = idUser + "-" + idCH + ".png";

        // convert image to base64
        //base64 = convertTextureToBase64(photo);
        error.text = "Picture captured";
        //UploadFile(imgpath, "http://10.8.154.245/mobilediary/targetMedia.php");
    }

    public Texture2D rotateTexture(Texture2D image)
    {

        Texture2D target = new Texture2D(image.height, image.width, image.format, false);    //flip image width<>height, as we rotated the image, it might be a rect. not a square image

        Color32[] pixels = image.GetPixels32(0);
        pixels = rotateTextureGrid(pixels, image.width, image.height);
        target.SetPixels32(pixels);
        target.Apply();

        //flip image width<>height, as we rotated the image, it might be a rect. not a square image

        return target;
    }


    public Color32[] rotateTextureGrid(Color32[] tex, int wid, int hi)
    {
        Color32[] ret = new Color32[wid * hi];      //reminder we are flipping these in the target

        for (int y = 0; y < hi; y++)
        {
            for (int x = 0; x < wid; x++)
            {
                ret[(hi - 1) - y + x * hi] = tex[x + y * wid];         //juggle the pixels around

            }
        }

        return ret;
    }

    IEnumerator UploadFileCo(string localFileName, string uploadURL)
    {
        error.text = "Uploading, please wait";
        WWW localFile = new WWW("file:///" + localFileName);
        yield return localFile;
        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else
        {
            error.text = LMan.getString("error_pic_txt");
            Debug.Log("Open file error: " + localFile.error);
            yield break; // stop the coroutine here
        }
        WWWForm postForm = new WWWForm();
        // version 1
        //postForm.AddBinaryData("theFile",localFile.bytes);
        // version 2
        postForm.AddBinaryData("theFile", localFile.bytes, localFileName, "image/png");
        WWW upload = new WWW(uploadURL, postForm);
        yield return upload;

        if (upload.error == null && imgname != "")
        {
            //error.text = upload.text;

            base64 = "http://10.8.154.245/mobilediary/uploads/" + imgname;
            Debug.Log("upload done :" + upload.text);

            //error.text = "Upload done";
            File.Delete(imgpath);
            sendImage(idCH.ToString(), idUser.ToString(), base64);
        }
        else
        {
            error.text = LMan.getString("error_net");

            if (imgname == "")
            {
                error.text = LMan.getString("error_pic_txt");
            }
            //Debug.Log("Error during upload: " + upload.error);
        }
    }
    void UploadFile(string localFileName, string uploadURL)
    {
        StartCoroutine(UploadFileCo(localFileName, uploadURL));
    }

    IEnumerator UploadFileCosound(string localFileName, string uploadURL)
    {
        error.text = "Uploading, please wait";
        WWW localFile = new WWW("file:///" + localFileName);
        yield return localFile;
        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else
        {
            Debug.Log("Open file error: " + localFile.error);
            yield break; // stop the coroutine here
        }
        WWWForm postForm = new WWWForm();
        // version 1
        //postForm.AddBinaryData("theFile",localFile.bytes);
        // version 2
        postForm.AddBinaryData("theFile", localFile.bytes, localFileName, "audio/wav");
        WWW upload = new WWW(uploadURL, postForm);
        yield return upload;

        if (upload.error == null)
        {
            //error.text = upload.text;

            audiobase64 = "http://10.8.154.245/mobilediary/uploads/" + idUser + "-" + idCH + ".wav";
            Debug.Log("upload done :" + upload.text);
            //error.text = "Upload done";
            File.Delete(audiopath + ".wav");
            sendaudio(idCH.ToString(), idUser.ToString(), audiobase64);
        }
        else
        {
            error.text = LMan.getString("error_net");
            if (audiobase64 == "")
            {
                error.text = LMan.getString("error_audio_txt");
            }
            Debug.Log("Error during upload: " + upload.error);
        }
    }
    void UploadFilesound(string localFileName, string uploadURL)
    {
        StartCoroutine(UploadFileCosound(localFileName, uploadURL));
    }
    void UploadFile(string localFileName, string uploadURL, int n)
    {
        StartCoroutine(UploadFileCo(localFileName, uploadURL, n));
    }

    IEnumerator UploadFileCo(string localFileName, string uploadURL, int n)
    {
        error.text = "Uploading, please wait";
        WWW localFile = new WWW("file:///" + localFileName);
        yield return localFile;
        if (localFile.error == null)
            Debug.Log("Loaded file successfully");
        else
        {
            Debug.Log("Open file error: " + localFile.error);
            yield break; // stop the coroutine here
        }
        WWWForm postForm = new WWWForm();
        postForm.AddBinaryData("theFile", localFile.bytes, localFileName, "image/png");
        WWW upload = new WWW(uploadURL, postForm);
        yield return upload;
        if (upload.error == null && textures[n] != "")
        {

            //error.text = upload.text;
            //base64_1.Add("http://10.8.154.245/mobilediary/uploads/" + textures[n]);
            File.Delete(imgpath_mov[n]);
            Debug.Log("upload done :" + upload.text);
        }
        else
        {
            error.text = LMan.getString("error_net");

            if (base64_1[n] == "")
            {
                error.text = LMan.getString("error_pic_txt");
            }
            //Debug.Log("Error during upload: " + upload.error);
        }
    }
    // method to take a video - associate with a button (uses the update() method...)
    public void takeVideo()
    {
        i = 0;
        frameTimer = 1f;
    }

    private string takeFrame(int numb)
    {
        Texture2D photo = new Texture2D(webcamTexture.width, webcamTexture.height);
        photo.SetPixels(webcamTexture.GetPixels());
        photo.Apply();
        //rotate 3x90º
        photo = rotateTexture(photo);
        photo = rotateTexture(photo);
        photo = rotateTexture(photo);
        //picture.texture = photo;
        byte[] bytes = photo.EncodeToPNG();
        imgpath_mov.Add(Application.persistentDataPath + "/" + idUser + "-" + idCH + "-" + numb + ".png");
        File.WriteAllBytes(imgpath_mov[numb], bytes);
        imgname2 = idUser + "-" + idCH + "-" + numb + ".png";
        //UploadFile(imgpath_mov[numb], "http://10.8.154.245/mobilediary/targetMedia.php", numb);
        return imgname2;
    }

    private string convertTextureToBase64(Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();
        return Convert.ToBase64String(bytes);
    }

    private string convertAudioToBase64(byte[] audio)
    {
        return Convert.ToBase64String(audio);
    }

    public void sendVideo(string chId, string usrId, string frame1, string frame2, string frame3, string frame4, string frame5)
    {
        Dictionary<string, string> videoHeader = new Dictionary<string, string>();
        videoHeader.Add("url1", frame1);
        videoHeader.Add("url2", frame2);
        videoHeader.Add("url3", frame3);
        videoHeader.Add("url4", frame4);
        videoHeader.Add("url5", frame5);
        videoHeader.Add("feedback", selectedtag);
        Dictionary<string, string> videoData = new Dictionary<string, string>();
        videoData.Add("challengeID", chId);
        videoData.Add("userID", usrId);
        StartCoroutine(post(video, videoHeader, videoData));
        error.text = LMan.getString("vid_load");
    }


    public void sendImage(string chId, string usrId, string content)
    {

        Dictionary<string, string> imageHeader = new Dictionary<string, string>();
        imageHeader.Add("url", content);
        imageHeader.Add("feedback", selectedtag);
        Dictionary<string, string> imageData = new Dictionary<string, string>();
        imageData.Add("challengeID", chId);
        imageData.Add("userID", usrId);
        StartCoroutine(post(image, imageHeader, imageData));
    }

    public void sendaudio(string chId, string usrId, string content)
    {
        Debug.Log("Audio Analysis");
        Dictionary<string, string> audioHeader = new Dictionary<string, string>();
        audioHeader.Add("url", content);
        audioHeader.Add("feedback", selectedtag);
        Dictionary<string, string> audioData = new Dictionary<string, string>();
        audioData.Add("challengeID", chId);
        audioData.Add("userID", usrId);
        audioData.Add("speechLang", audiolang);
        StartCoroutine(post(audiost, audioHeader, audioData));

    }

    public void sendtext(string chId, string usrId, string content)
    {
        Debug.Log("Text Analysis");
        Dictionary<string, string> textHeader = new Dictionary<string, string>();
        textHeader.Add("url", content);
        textHeader.Add("feedback", selectedtag);
        Dictionary<string, string> textData = new Dictionary<string, string>();
        textData.Add("challengeID", chId);
        textData.Add("userID", usrId);
        StartCoroutine(post(text, textHeader, textData));

    }
    public IEnumerator post(string url, Dictionary<string, string> header, Dictionary<string, string> data)
    {
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> entry in header)
        {
            form.AddField(entry.Key, entry.Value);
        }

        Dictionary<String, String> headers = form.headers;
        byte[] rawData = form.data;
        foreach (KeyValuePair<string, string> entry in data)
        {
            headers[entry.Key] = entry.Value;
        }

        WWW www = new WWW(url, rawData, headers);
        yield return www;

        //.. process results from WWW request here...
        if (www.error != null)
        {
            error.text = LMan.getString("error_net");
            Debug.Log(www.error); 
        }
        else
        {
            Debug.Log(www.text);
            chal_true.Add(www.text);
            //if (www.text == "true")
            //{

            //    if (idUser > 0)
            //    {
            //        SceneManager.LoadScene("Final");
            //    }
            //    else
            //    {
            //        SceneManager.LoadScene("Final2");
            //    }
            //}
            //else if (www.text == "false")
            //{
            //    SceneManager.LoadScene("Final3");
            //}

        }
    }

    public void send()
    {


        

        if (sendvid)
        {
            for (int i = 0; i <= imgpath_mov.Count - 1; i++)
            {

                UploadFile(imgpath_mov[i], "http://10.8.154.245/mobilediary/targetMedia.php", i);
                if (base64_1[0] != "" && i == 4)
                    sendVideo(idCH.ToString(), idUser.ToString(), base64_1[0], base64_1[1], base64_1[2], base64_1[3], base64_1[3]);
            }

        }

        if (sendimag)
        {
            UploadFile(imgpath, "http://10.8.154.245/mobilediary/targetMedia.php");
        }

        if (sendtxt)
            if (inp1.text == "")
            {
                error.text = LMan.getString("error_txt_txt");
            }
            else
        if (sendtxt)
                sendtext(idCH.ToString(), idUser.ToString(), inp1.text);


        if (sendaud)
            //UploadFilesound(audiopath + ".wav", "http://10.8.154.245/mobilediary/targetMedia.php");
            UploadFilesound(audiopath + ".wav", "http://10.8.154.245/mobilediary/targetMediaAudio.php");



    }

    public void recordaudio()
    {
        j = 0;
        frameTimerA = 1f;
        if (micConnected)
        {
            //If the audio from any microphone isn't being captured
            if (!Microphone.IsRecording(null))
            {

                //Start recording and store the audio captured from the microphone at the AudioClip in the AudioSource
                goAudioSource.clip = Microphone.Start(null, true, 20, maxFreq);


            }

        }
        else // No microphone
        {
            //Print a red "Microphone not connected!" message at the center of the screen

            error.text = "Microphone not connected!";
        }
    }

    public void cancel()
    {
        SceneManager.LoadScene("Challenges");

    }

    private void fillCombo()
    {
        if (tagsArray.Length != 0)
        {
            drop1.ClearOptions();
            //comboBox
            List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();

            for (int i = 0; i <= tagsArray.Length - 1; i++)
            {
                if (LangChange.currentLang == "EN")
                {

                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = tagsArray[i];
                    list.Add(txt);


                }
                else if (LangChange.currentLang == "PT")
                {
                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = tagsArray[i];
                    list.Add(txt);

                }

            }

            drop1.AddOptions(list);
        }
        selectedtag = drop1.options[drop1.value].text;
        //Debug.Log("selected: " + selectedjob);
        drop1.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler(drop1);
        });

    }
    private void myDropdownValueChangedHandler(Dropdown target)
    {
        selectedtag = target.options[target.value].text;
        //Debug.Log("selected: " + selectedjob);
    }
}
