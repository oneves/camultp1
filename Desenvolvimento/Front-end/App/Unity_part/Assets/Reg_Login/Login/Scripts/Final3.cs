using UnityEngine;
using UnityEngine.UI;

public class Final3 : MonoBehaviour {
    public Text txt;
    public Text lbl1;

    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }

    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("CH_false_txt");
        lbl1.text = LMan.getString("THK_txt");
        
    }

     
	}
    
