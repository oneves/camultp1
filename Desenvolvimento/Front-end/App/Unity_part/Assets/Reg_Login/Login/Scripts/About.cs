using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class About : MonoBehaviour
{
    public Text txt;
    public Text lbl1;
    private Lang LMan;
    public void OnEnable()
    {
        LMan = new Lang(LangChange.currentLang);
    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("about_txt");
        lbl1.text = LMan.getString("aboutinfo_txt");
        
    }
    void Start()
    {
        
    }

   

}

