using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Enter2 : MonoBehaviour {
    
    public Text txt;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    private Lang LMan;
    public void OnEnable()
    {
        LMan = new Lang(LangChange.currentLang);
    }


    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("main_txt");
        btn1.GetComponentInChildren<Text>().text = LMan.getString("reg_txt");
        btn2.GetComponentInChildren<Text>().text = LMan.getString("login_txt");
        btn3.GetComponentInChildren<Text>().text = LMan.getString("about_txt");
        btn4.GetComponentInChildren<Text>().text = LMan.getString("tut_txt");
        btn5.GetComponentInChildren<Text>().text = LMan.getString("exit_txt");
    }
	

    public void reg()
    {
        SceneManager.LoadScene("CreateAccountScene");
    }
    public void log()
    {
        SceneManager.LoadScene("LoginScene");
    }

    public void abt()
    {
        SceneManager.LoadScene("About");
    }

    public void Tut()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void exit()
    {
        Application.Quit();
    }
}
    
