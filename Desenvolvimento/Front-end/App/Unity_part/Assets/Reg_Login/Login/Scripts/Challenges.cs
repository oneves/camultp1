using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Challenges : MonoBehaviour {
   
    int idUser = Login.IdUser;
    public static int idCH = 0;
    private string challenge = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/get_myChallenges";
    private List<Texture2D> image = new List<Texture2D>();
    public Text txt;
    public GameObject btnpref;
    public RectTransform rectT;
    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("ch_txt2");
    }

    void Start()
    {
        getChallenge();
        Debug.Log(idUser);
        
    }
	
    void myFunctionForOnClickEvent(int id)
    {
        idCH = id;
        Debug.Log(idCH);
        SceneManager.LoadScene("Objective");
    }
    public void getChallenge()
    {
        getuserCh();    
    }
    private void getuserCh()
    {
        Debug.Log("User Challenges");
        Dictionary<string, string> challengesHeader = new Dictionary<string, string>();
        challengesHeader.Add("dummy", "something");
        Dictionary<string, string> challengeData = new Dictionary<string, string>();
        challengeData.Add("usrID", idUser.ToString());
        StartCoroutine(post(challenge, challengesHeader, challengeData));
    }

    public IEnumerator post(string url, Dictionary<string, string> header, Dictionary<string, string> data)
    {
        WWWForm form = new WWWForm();
        foreach (KeyValuePair<string, string> entry in header)
        {
            form.AddField(entry.Key, entry.Value);
        }

        Dictionary<String, String> headers = form.headers;
        byte[] rawData = form.data;
        foreach (KeyValuePair<string, string> entry in data)
        {
            headers[entry.Key] = entry.Value;
        }

        WWW www = new WWW(url, rawData, headers);
        yield return www;

        //.. process results from WWW request here...
        if (www.error != null)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.text);
            if(www.text!="")
            STlist.SetList(spliter(www.text));
           
            if(STlist.GetList()!=null)
            if (STlist.GetList().Count != 0)
            {
                for (int i = 0; i <= STlist.GetList().Count-1; i++)
                {
                    image.Add(new Texture2D(128,128));
                    var www2 = new WWW("http://10.8.154.245/mobilediary/" + STlist.GetList()[i].Img2);

                    // wait until the download is done
                    yield return www2;
                    // assign the downloaded image to the main texture of the object
                    www2.LoadImageIntoTexture(image[i]);
                }
                    for (int k = 0; k <= STlist.GetList().Count - 1; k++)
                    {

                        GameObject Btnimg = Instantiate(btnpref);
                       
                        Btnimg.transform.SetParent(rectT, false);
                        Btnimg.GetComponent<Image>().sprite= Sprite.Create(image[k],  new Rect(0, 0, image[k].width, image[k].height), new Vector2(0f,0f));
                        Button t = Btnimg.GetComponent<Button>();
                        AddListener(t, STlist.GetList()[k].Id);
                        

                    }
                    
            }
        }
    }

    void AddListener(Button b, int id)
    {
        b.onClick.AddListener(() => { myFunctionForOnClickEvent(id); });
    }
    private List<ChallengesInfo> spliter(string result)
    {
        List<ChallengesInfo> nova = new List<ChallengesInfo>();
        string[] Services = result.Split(';');
        foreach (string service in Services)
        {
            ChallengesInfo info = new ChallengesInfo();
            string[] infos = service.Split('#');
            if (infos.Length == 11)
            {
                info.Id = int.Parse(infos[0]);
                info.Titulo_PT = infos[1];
                info.Titulo_EN = infos[2];
                info.Desc_PT = infos[3];
                info.Desc_EN = infos[4];
                info.Data_inicio = infos[5];
                info.Data_fim = infos[6];
                info.Img1 = infos[7];
                info.Img2 = infos[8];
                info.Tags = infos[9];
                info.Type = infos[10];
                nova.Add(info);
            }
            else if (infos.Length == 10)
            {
                info.Id = int.Parse(infos[0]);
                info.Titulo_PT = infos[1];
                info.Titulo_EN = infos[2];
                info.Desc_PT = infos[3];
                info.Desc_EN = infos[4];
                info.Data_inicio = infos[5];
                info.Data_fim = infos[6];
                info.Img1 = infos[7];
                info.Tags = infos[8];
                info.Type = infos[9];
                nova.Add(info);
            }
        } 
        return nova;
    }
}