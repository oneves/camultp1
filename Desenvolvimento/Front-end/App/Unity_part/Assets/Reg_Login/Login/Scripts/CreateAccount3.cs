using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Net;
using System.IO;
using UnityEngine.UI;

public class CreateAccount3 : MonoBehaviour
{


    private string errorMsg = "";
    public string selectedEduc = "";
    public string selectedMarit = "";
    public string selectedChild = "";
    public Dropdown drop1;
    public Dropdown drop2;
    public Dropdown drop3;
    public Text lbl1;
    public Text lbl2;
    public Text lbl3;
    public Text error;
    public Button btn1;
    public Button btn2;
    private static List<WebServiceInfo> educationList = new List<WebServiceInfo>();
    private static List<WebServiceInfo> maritialList = new List<WebServiceInfo>();

    private string register = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/RegisterRegularUser";
    private string education = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/get_educationalLvls";
    private string maritalStatus = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/get_maritalStatus";
    private Lang LMan;
    private static int idEduc=0;
    private static int idMat=0;
    private static int idChild=0;

    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    private void Start()
    {
        getCombo();
        fillCombo();
        fillCombo2();
        fillCombo3();

        drop1.value = idEduc;

        drop2.value = idMat;
        drop3.value = idChild;

       
    }

    public void change()
    {
        if (LangChange.currentLang == "EN")
        {
            fillCombo();
            fillCombo2();
            fillCombo3();

        }
        else if (LangChange.currentLang == "PT")
        {
            fillCombo();
            fillCombo2();
            fillCombo3();

        }

    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        lbl1.text = LMan.getString("educ_txt") + "*";
        lbl2.text = LMan.getString("maritial_txt") + "*";
        lbl3.text = LMan.getString("child_txt");

        btn1.GetComponentInChildren<Text>().text = LMan.getString("back_txt");
        btn2.GetComponentInChildren<Text>().text = LMan.getString("reg_txt");
    }
    private void fillCombo()
    {
        if (educationList.Count != 0)
        {
            drop1.ClearOptions();
            //comboBox
            List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();

            for (int i = 0; i <= educationList.Count - 1; i++)
            {
                if (LangChange.currentLang == "EN")
                {

                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = educationList[i].Desc_EN;
                    list.Add(txt);
                }
                else if (LangChange.currentLang == "PT")
                {
                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = educationList[i].Desc_PT;
                    list.Add(txt);
                }
            }

            drop1.AddOptions(list);
        }
        selectedEduc = drop1.options[drop1.value].text;

        drop1.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler(drop1);
        });


    }


    private void myDropdownValueChangedHandler(Dropdown target)
    {
        selectedEduc = target.options[target.value].text;
        idEduc = target.value;
    }

    private void fillCombo2()
    {
        if (maritialList.Count != 0)
        {
            drop2.ClearOptions();
            //comboBox
            List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();

            for (int i = 0; i <= maritialList.Count - 1; i++)
            {
                if (LangChange.currentLang == "EN")
                {

                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = maritialList[i].Desc_EN;
                    list.Add(txt);
                }
                else if (LangChange.currentLang == "PT")
                {
                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = maritialList[i].Desc_PT;
                    list.Add(txt);
                }
            }

            drop2.AddOptions(list);
        }
        selectedMarit = drop2.options[drop2.value].text;

        drop2.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler2(drop2);
        });


    }
    private void myDropdownValueChangedHandler2(Dropdown target)
    {
        selectedMarit = target.options[target.value].text;
        idMat = target.value;
    }

    private void fillCombo3()
    {
        drop3.ClearOptions();
        //comboBox
        List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();
        for (int i = 0; i < 6; i++)
        {
            Dropdown.OptionData txt = new Dropdown.OptionData();
            if (i == 5)
                txt.text = "+" + i.ToString();

            txt.text = i.ToString();
            list.Add(txt);
        }

        drop3.AddOptions(list);

        selectedChild = drop3.options[drop3.value].text;

        drop3.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler3(drop3);
        });

    }
    private void myDropdownValueChangedHandler3(Dropdown target)
    {
        selectedChild = target.options[target.value].text;
        idChild = target.value;
    }

    public void back()
    {
        SceneManager.LoadScene("CreateAccountScene2");
    }
    public void reg()
    {
        if ((selectedEduc != "")
                && (selectedMarit != "")
                && (selectedChild != ""))
        {
            //Debug.Log(CreateAccount.NameUser);
            StartCoroutine(registeToServer());

        }
        else {
            errorMsg = "Fill all the fields with (*) correctly";

        }
    }
    private IEnumerator registeToServer()
    {
        Debug.Log("Register");
        Dictionary<string, string> registerHeader = new Dictionary<string, string>();
        registerHeader.Add("dummy", "something");
        Dictionary<string, string> registerData = new Dictionary<string, string>();
        registerData.Add("userName", CreateAccount.NameUser);
        registerData.Add("password", CreateAccount2.password);
        registerData.Add("email", CreateAccount2.email);
        registerData.Add("name", CreateAccount.NameUser);
        registerData.Add("birthdate", CreateAccount.date);
        registerData.Add("jobSector", CreateAccount.selectedjob);
        registerData.Add("educatLvl", selectedEduc);
        registerData.Add("gender", CreateAccount.selectedgender);
        registerData.Add("maritStat", selectedMarit);
        registerData.Add("country", CreateAccount2.selectedCountry);
        registerData.Add("city", CreateAccount2.selectedCity);
        registerData.Add("zipCode", CreateAccount2.postalcode);
        registerData.Add("nrChildr", selectedChild);
        StartCoroutine(post(register, registerHeader, registerData));
        yield return new WaitForSeconds(1f);

    }
    public void getCombo()
    {
        getFromREST();
    }



    private void getFromREST()
    {

        string result = get(education);

        educationList = spliter(result);

        string result2 = get(maritalStatus);

        maritialList = spliter(result2);
    }

    private List<WebServiceInfo> spliter(string result)
    {
        List<WebServiceInfo> nova = new List<WebServiceInfo>();
        string[] Services = result.Split(';');

        foreach (string service in Services)
        {


            WebServiceInfo info = new WebServiceInfo();
            string[] infos = service.Split('#');

            info.Id = int.Parse(infos[0]);

            info.Desc_PT = infos[1];

            info.Desc_EN = infos[2];

            nova.Add(info);
        }
        return nova;
    }

    public string get(string url)
    {
        try
        {
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;

            using (HttpWebResponse resp = req.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(resp.GetResponseStream());
                return reader.ReadToEnd();
            }
        }
        catch (Exception) { }
        return "";
    }

    public IEnumerator post(string url, Dictionary<string, string> header, Dictionary<string, string> data)
    {
        WWWForm form = new WWWForm();
        foreach (KeyValuePair<string, string> entry in header)
        {
            form.AddField(entry.Key, entry.Value);
        }

        Dictionary<String, String> headers = form.headers;
        byte[] rawData = form.data;
        foreach (KeyValuePair<string, string> entry in data)
        {
            headers[entry.Key] = entry.Value;
        }
        WWW www = new WWW(url, rawData, headers);
        yield return www;

        //.. process results from WWW request here...
        if (www.error != null)
        {
            Debug.Log(www.error);
        }
        else
        {
            switch (int.Parse(www.text))
            {
                case 1:
                    //errorMsg = "registration was successful";
                    SceneManager.LoadScene("RegComplete");
                    break;

                case -1:
                    errorMsg = "UserName already in use";
                    break;
                case -2:
                    errorMsg = "jobSector not available / failed to connect to DB";
                    break;
                case -3:
                    errorMsg = "educatLvl not available";
                    break;
                case -4:
                    errorMsg = "gender not available";
                    break;
                case -5:
                    errorMsg = "maritStat not available";
                    break;
                case -6:
                    errorMsg = "country not available";
                    break;
                case -7:
                    errorMsg = "city not available";
                    break;
                case -8:
                    errorMsg = "SQL insert has failed...?";
                    break;
            }
            Debug.Log(www.text);
            error.text = errorMsg;
            //SceneManager.LoadScene("About");
        }
    }
}
