﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


  static  class STlist
    {
    static List<ChallengesInfo> _list; // Static List instance

    static void ListTest()
    {
        //
        // Allocate the list.
        //
        _list = new List<ChallengesInfo>();
    }

    public static void SetList(List<ChallengesInfo> nlist)
    {
        _list = nlist;
    }

    public static List<ChallengesInfo> GetList()
    {
        return _list;
    }
    public static void Add(ChallengesInfo value)
    {
        //
        // Record this value in the list.
        //
        _list.Add(value);
    }

    
}

