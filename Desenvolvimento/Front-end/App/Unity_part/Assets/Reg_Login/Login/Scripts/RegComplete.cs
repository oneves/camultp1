using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RegComplete : MonoBehaviour {

    private int i;
    private float frameTimer;
    public Text txt;
    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    void Update() {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("reg_complete_txt");
        if (i < 2)
        {
            frameTimer -= Time.deltaTime;
            if (frameTimer <= 0)
            {
                
                frameTimer = 1f;
                i++;
            }
        }
        else if (i == 2)
        {
            i++;

            SceneManager.LoadScene("LoginScene");
        }

    }

	}
    
