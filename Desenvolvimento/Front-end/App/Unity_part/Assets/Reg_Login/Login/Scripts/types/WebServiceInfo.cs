﻿class WebServiceInfo
    {
        int id;
        string desc_PT;
        string desc_EN;

        public string Desc_EN
        {
            get
            {
                return desc_EN;
            }

            set
            {
                desc_EN = value;
            }
        }

        public string Desc_PT
        {
            get
            {
                return desc_PT;
            }

            set
            {
                desc_PT = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }

