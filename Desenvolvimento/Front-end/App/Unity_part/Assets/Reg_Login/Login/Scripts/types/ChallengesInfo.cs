﻿class ChallengesInfo
{
        int id;
    string titulo_PT;
    string titulo_EN;
        string desc_PT;
        string desc_EN;
    string data_inicio;
    string data_fim;
    string img1;
    string img2;
    string type;
    string tags;

        public string Desc_EN
        {
            get
            {
                return desc_EN;
            }

            set
            {
                desc_EN = value;
            }
        }

        public string Desc_PT
        {
            get
            {
                return desc_PT;
            }

            set
            {
                desc_PT = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

    public string Titulo_PT
    {
        get
        {
            return titulo_PT;
        }

        set
        {
            titulo_PT = value;
        }
    }

    public string Titulo_EN
    {
        get
        {
            return titulo_EN;
        }

        set
        {
            titulo_EN = value;
        }
    }

    public string Data_inicio
    {
        get
        {
            return data_inicio;
        }

        set
        {
            data_inicio = value;
        }
    }

    public string Data_fim
    {
        get
        {
            return data_fim;
        }

        set
        {
            data_fim = value;
        }
    }

    public string Img1
    {
        get
        {
            return img1;
        }

        set
        {
            img1 = value;
        }
    }

    public string Img2
    {
        get
        {
            return img2;
        }

        set
        {
            img2 = value;
        }
    }

    public string Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public string Tags
    {
        get
        {
            return tags;
        }

        set
        {
            tags = value;
        }
    }
}

