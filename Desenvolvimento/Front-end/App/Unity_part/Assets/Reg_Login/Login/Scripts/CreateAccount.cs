using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Linq;
using UnityEngine.UI;

public class CreateAccount : MonoBehaviour {

	private string errorMsg = "";
	public static string NameUser = "";
	public static string date = "";
    public static string selectedjob="";
    public static string selectedgender = "";
    private static List<WebServiceInfo> jobList = new List<WebServiceInfo>();
    private static List<WebServiceInfo> genderList = new List<WebServiceInfo>();
    private string jobs = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/get_JobSectors";
    private string gender = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/get_gender";
   
    public Dropdown drop1;
    public Text txt;
    public Text lbl1;
    public Text lbl2;
    public Text lbl3;
    public Text lbl4;
    public Button btn1;
    public InputField inp1;
    public InputField inp2;
    public InputField inp3;
    public InputField inp4;
    public Dropdown drop2;
    public Text error;
    private static int idjob=0;
    private static int idgender = 0;
    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    
    private void Start()
    {
       
        getJob();
        fillCombo();
        fillGender();

        inp1.text = NameUser;
        if (date != "")
        {
            string[] dates = date.Split('-');
            inp2.text = dates[0];
            inp3.text = dates[1];
            inp4.text = dates[2];
        }
        
            drop1.value = idjob;
       
            drop2.value = idgender;
        
    }
    public void change()
    {
        if (LangChange.currentLang == "EN")
        {
            fillCombo();
            fillGender();
           
        }
        else if (LangChange.currentLang == "PT")
        {
            fillCombo();
            fillGender();
           
        }

    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("reg_txt");
        lbl1.text = LMan.getString("name_txt");
        lbl2.text = LMan.getString("date_txt");
        lbl3.text = LMan.getString("job_txt");
        lbl4.text = LMan.getString("gender_txt");
        btn1.GetComponentInChildren<Text>().text = LMan.getString("next_txt");

    }
    private void fillCombo()
    {
        if (jobList.Count != 0)
        {
            drop1.ClearOptions();
            //comboBox
            List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();
            
            for (int i = 0; i <= jobList.Count - 1; i++)
            {
                if (LangChange.currentLang == "EN")
                {

                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = jobList[i].Desc_EN;
                    list.Add(txt);

                   
                }
                else if (LangChange.currentLang == "PT")
                {
                    Dropdown.OptionData txt = new Dropdown.OptionData();
                    txt.text = jobList[i].Desc_PT;
                    list.Add(txt);
                 
                }

            }

            drop1.AddOptions(list);
        }
        selectedjob = drop1.options[drop1.value].text;
        //Debug.Log("selected: " + selectedjob);
        drop1.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler(drop1);
        });

    }
    private void myDropdownValueChangedHandler(Dropdown target)
    {
        selectedjob = target.options[target.value].text;
        idjob = target.value;
        //Debug.Log("selected: " + selectedjob);
    }

    private void fillGender()

    {

        if (genderList.Count != 0)
        {
            drop2.ClearOptions();
            //comboBox
            List<Dropdown.OptionData> list2 = new List<Dropdown.OptionData>();
            for (int i = 0; i <= genderList.Count() - 1; i++)
            {


                if (LangChange.currentLang == "EN")
                {
                    Dropdown.OptionData txt2 = new Dropdown.OptionData();
                    txt2.text = genderList[i].Desc_EN;
                    list2.Add(txt2);


                }
                else if (LangChange.currentLang == "PT")
                {
                    Dropdown.OptionData txt2 = new Dropdown.OptionData();
                    txt2.text = genderList[i].Desc_PT;
                    list2.Add(txt2);

                }


            }
            drop2.AddOptions(list2);
        }
            selectedgender = drop2.options[drop2.value].text;
            //Debug.Log("selected: " + selectedgender);
            drop2.onValueChanged.AddListener(delegate
        {
            myDropdownValueChangedHandler2(drop2);
        });

        }
    
    private void myDropdownValueChangedHandler2(Dropdown target)
    {
        selectedgender = target.options[target.value].text;
        idgender = target.value;
        //Debug.Log("selected: " + selectedgender);
    }

    public void NameInput(InputField nameField)
    {
        NameUser = nameField.text;
        //Debug.Log(NameUser);
    }

    public void dateInput()
    {
        string dt = inp2.text + "-" + inp3.text + "-" + inp4.text;
        date = dt;
        //Debug.Log(date);
    }
   

    public void next()
    {
        NameInput(inp1);
        dateInput();
        if ((NameUser != "")
                && (date != "")
                && (selectedjob != ""))
        {
            SceneManager.LoadScene("CreateAccountScene2");
        }
        else {
            errorMsg = "Fill all the fields with (*) correctly";
            error.text = errorMsg;
        }
       
    }
    public void getJob()
    {

        getFromREST();
        
    
    }

   

    private void getFromREST()
    {
              
        string result = get(jobs);

        jobList = spliter(result);

        string result2 = get(gender);

        genderList = spliter(result2);
    }

    private List<WebServiceInfo> spliter(string result)
    {
        List<WebServiceInfo> nova = new List<WebServiceInfo>();
        string[] Services = result.Split(';');
       
        foreach (string service in Services)
        {
            
               
            WebServiceInfo info = new WebServiceInfo();
            string[] infos = service.Split('#');

            info.Id = int.Parse(infos[0]);

            info.Desc_PT = infos[1];

            info.Desc_EN = infos[2];

            nova.Add(info);
        }
        return nova;
    }

    public string get(string url)
    {
        try
        {
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;

            using (HttpWebResponse resp = req.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(resp.GetResponseStream());
                return reader.ReadToEnd();
            }
        }
        catch (Exception) { }
        return "";
    }
}

    
