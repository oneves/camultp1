using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Objective : MonoBehaviour {

    int idCH = Challenges.idCH;
   
    private List<ChallengesInfo> challengeList = STlist.GetList();
    public Text txt;
    public Text lbl1;
    public Text lbl2;
    public Text lbl3;
    public Text lbl4;
    public Text tp1;
    public Text tp2;
    public Text tp3;
    public Text tp4;
    public Button btn1;
    
    private int idCHinUse;
    string[] TPch;

    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        fillLabels();
        fillTip();
    }

    void Start() {
        for (int i = 0; i <= challengeList.Count - 1; i++)
        {
            if (challengeList[i].Id == idCH)
                idCHinUse = i;
        }
        
        //Debug.Log(challengeList[idCH].Type);
        //Debug.Log(idCHinUse);
    }
    void fillLabels()
    {
        txt.text = LMan.getString("chdesc_txt");
       
        
            if (LangChange.currentLang == "EN")
            {
            lbl1.text = challengeList[idCHinUse].Titulo_EN;
            lbl2.text = challengeList[idCHinUse].Desc_EN;
            lbl3.text = LMan.getString("st_date_txt") + challengeList[idCHinUse].Data_inicio;
            lbl4.text = LMan.getString("en_date_txt") + challengeList[idCHinUse].Data_fim;
            btn1.GetComponentInChildren<Text>().text = LMan.getString("next_txt");

        }
            else if (LangChange.currentLang == "PT")
            {
            lbl1.text = challengeList[idCHinUse].Titulo_PT;
            lbl2.text = challengeList[idCHinUse].Desc_PT;
            lbl3.text = LMan.getString("st_date_txt") + challengeList[idCHinUse].Data_inicio;
            lbl4.text = LMan.getString("en_date_txt") + challengeList[idCHinUse].Data_fim;
            btn1.GetComponentInChildren<Text>().text = LMan.getString("next_txt");
        }

        
    }

    void fillTip()
    {
        string types = challengeList[idCHinUse].Type;
        types = types.TrimStart('[');
        types = types.TrimEnd(']');
        TPch = types.Split(',');
        foreach (string t in TPch)
        {
            //Debug.Log(t);
            if (t == "1")
            {
                tp1.gameObject.SetActive(true);
                tp1.text = LMan.getString("text");
            }
            else if (t == "2")
            {
                tp2.gameObject.SetActive(true);
                tp2.text = LMan.getString("img");

            }
            else if (t == "3")
            {
                tp3.gameObject.SetActive(true);
                tp3.text = LMan.getString("vid");
            }
            else if (t == "4")
            {
                tp4.gameObject.SetActive(true);
                tp4.text = LMan.getString("aud");
            }

        }

    }
	

    public void Next()
    {
        SceneManager.LoadScene("camera");
    }

}

