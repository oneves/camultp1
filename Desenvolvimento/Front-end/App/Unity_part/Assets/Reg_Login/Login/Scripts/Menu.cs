﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
     
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public GameObject panel;
    private float frameTimer;
    private int i;
    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }
    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        btn1.GetComponentInChildren<Text>().text = LMan.getString("about_txt");
        btn2.GetComponentInChildren<Text>().text = LMan.getString("ch_txt");
        btn3.GetComponentInChildren<Text>().text = LMan.getString("tut_txt");
        btn4.GetComponentInChildren<Text>().text = LMan.getString("reg_txt");
        btn5.GetComponentInChildren<Text>().text = LMan.getString("login_txt");
        btn6.GetComponentInChildren<Text>().text = LMan.getString("exit_txt");

        if (i < 5)
        {
            frameTimer -= Time.deltaTime;
            if (frameTimer <= 0)
            {

                frameTimer = 1f;
                i++;
            }
        }
        else if (i == 5)
        {
            i++;


            panel.gameObject.SetActive(false);
        }
    }
    

    public void reg()
    {
        SceneManager.LoadScene("CreateAccountScene");
    }
    public void log()
    {
        SceneManager.LoadScene("LoginScene");
    }

    public void abt()
    {
        SceneManager.LoadScene("About");
    }

    public void Tut()
    {
        SceneManager.LoadScene("Tutorial");
    }

    public void exit()
    {
        Application.Quit();
    }
    public void Chall()
    {
       if(Login.IdUser<=0) Login.IdUser = -1;
        SceneManager.LoadScene("Challenges");
    }

    public void openmenu()
    {
        panel.gameObject.SetActive(true);

    }
}

