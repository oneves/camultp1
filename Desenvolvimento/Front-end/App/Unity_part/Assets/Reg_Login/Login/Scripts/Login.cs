using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public class Login : MonoBehaviour {

  
    public static int IdUser = 0;
    private string email = "";
	private string password = "";

   
    private static String url = "http://10.8.154.245:8080/Backend_C2016/webresources/REST/login";
    public Text txt;
    public Text lbl1;
    public Text lbl2;
    public Button btn1;
    public Button btn2;
    public InputField inp1;
    public InputField inp2;
    public Text error;
    private Lang LMan;
    public void OnEnable()
    {
         LMan = new Lang(LangChange.currentLang);
    }




    void Update()
    {
        LMan.setLanguage(LangChange.currentLang);
        txt.text = LMan.getString("login_txt");
        lbl1.text = LMan.getString("user_txt");
        lbl2.text = "Password";
        btn1.GetComponentInChildren<Text>().text = LMan.getString("guest_txt");
        btn2.GetComponentInChildren<Text>().text = LMan.getString("login_txt");
       
    }
    public void UserInput(InputField userField)
    {
        email = userField.text;
        //Debug.Log(email);
    }

    public void PassInput(InputField passField)
    {
        password = passField.text;
        //Debug.Log(password);
    }


	void Start() {
       

    }

	

    public void guest()
    {
        IdUser = -1;
        SceneManager.LoadScene("Challenges");
    }
    public void log()
    {
        UserInput(inp1);
        PassInput(inp2);
        Dictionary<string, string> loginHeader = new Dictionary<string, string>();
        loginHeader.Add("dummy", "something");
        Dictionary<string, string> loginData = new Dictionary<string, string>();
        loginData.Add("userName", email);
        loginData.Add("pass", password);
        StartCoroutine(post(url, loginHeader, loginData));
    }

    public IEnumerator post(string url, Dictionary<string, string> header, Dictionary<string, string> data)
    {
        WWWForm form = new WWWForm();

        foreach (KeyValuePair<string, string> entry in header)
        {
            form.AddField(entry.Key, entry.Value);
        }

        Dictionary<String, String> headers = form.headers;
        byte[] rawData = form.data;
        foreach (KeyValuePair<string, string> entry in data)
        {
            headers[entry.Key] = entry.Value;
        }

        WWW www = new WWW(url, rawData, headers);
        yield return www;

        //.. process results from WWW request here...
        if (www.error != null)
        {
            Debug.Log(www.error);
            
            error.text = LMan.getString("error_log");
        }
        else
        {
            IdUser =int.Parse (www.text);
            if (IdUser == -1)
                error.text = LMan.getString("error_log");

            if (IdUser != -1)
            {
                Debug.Log(www.text);
                SceneManager.LoadScene("Challenges");
            }
        }
    }
}
