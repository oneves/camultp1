<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Mobile Diary</title>
		<link rel="stylesheet" type="text/css" href="styles.css">
		<!--JQuery-->
		<script src="import/jquery-1.12.0.min.js"></script>
		<script src="import/jquery-1.12.2.min.js"></script>
		<script src="import/jquery-1.12.2.js"></script>
		<!--Calendar-->
		<link rel="stylesheet" href="import/jquery-ui.css" />
		<script src="import/jquery-ui.js"></script>
		<!--TimePicker-->
		<link rel="stylesheet" href="import/time/jquery-ui-timepicker.css" />
		<script src="import/time/jquery-ui-timepicker.js"></script>
		<script src="import/time/date.js"></script>
		<script type="text/javascript" src="noty-2.3.8/js/noty/packaged/jquery.noty.packaged.js"></script>
	</head>
	<body>
		<div id="header1">build to serve you</div>
		<div id="header2"></div>
		<!--************************ DIV PARA MOSTRAR AS MENSAGENS ************************ -->
		<script type="text/javascript">
			
			function generate(type, text) {
				var n = noty({
					text        : text,
					type        : type,
					dismissQueue: true,
					timeout: 5000,
					layout      : 'bottomCenter',
					theme       : 'defaultTheme'
				});
				console.log('html: ' + n.options.id);
				
				/*setTimeout(function () {
					$.noty.closeAll();
				}, 5000);*/
				
			}
			
			
			//generate('alert');
			//generate('information');
			//generate('error');
			//generate('warning');
			//generate('notification');
			//generate('success');
			
			
			
		</script>
		<div>
			<?php
				error_reporting(0);
				
				/******************* DEFINIÇÃO DAS FUNÇÕES **********************/
				
				include("server_functions.php");
				
				/******************* LINGUAGEM E TRADUÇÕES **********************/
				if (isset($_POST['language']) && $_POST['language'] != '' && $_POST['language'] != $_SESSION["language"]) 
				{
					//echo 'DEBUG: Language: '.$_SESSION['language'];
					
					if (!isset($_SESSION["userType"]) || $_SESSION["userType"] == "")
					echo '<script>document.getElementById("logout").style.visibility = "hidden";</script>';
					else
					echo '<script>document.getElementById("logout").style.visibility = "visible";</script>';
					
					$_SESSION["language"] = $_POST['language'];
					
					/*if(!isset($_SESSION['languageSel']) || $_SESSION['languageSel'] == '')
						$_SESSION['languageSel'] =  $_SESSION["language"];
					
					echo 'DEBUG: Language2: '.$_SESSION['languageSel'];*/
				}
				//else $_SESSION['language'] = 'PT';
				
				switch ($_SESSION['language']) {
					case 'ENG': $lang_file = 'lang.en.php';
					break;
					case 'PT':  $lang_file = 'lang.pt.php';
					break;
					default: $lang_file = 'lang.pt.php';
				}
				
				include $lang_file;
				
				/******************* OPERAÇÕES **********************/
				
				if (isset($_POST['task'])) {
					switch($_POST['task'])
					{
						case 'login': @login($_POST['username'], $_POST['password']);
						if (!isset($_SESSION['userIdUser']))
						{
							$Message = $lang['COULDNTLOGIN'];
							echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
						}	
						else
						echo '<script>document.getElementById("logout").style.visibility = "visible";</script>';
						break;
						case 'logout': @logout();
						break;
						case 'createChallenge': 
						
						/*echo $_SESSION["LAST_CHALNamePT"].' '.$_POST["input_title_pt"].'</br>';
						echo $_SESSION["LAST_CHALNameENG"].' '.$_POST["input_title_eng"].'</br>';
						echo $_SESSION["LAST_CHALDescPT"].' '.$_POST["input_desc_pt"].'</br>';
						echo $_SESSION["LAST_CHALDescENG"].' '.$_POST["input_desc_eng"].'</br>';
						echo $_SESSION["LAST_CHALdtIni"].' '.$_POST["dtIni"].'</br>';
						echo $_SESSION["LAST_CHALdtEnd"].' '.$_POST["dtFim"].'</br>';
						echo $_SESSION["LAST_CHALtags"].' '.$_POST["tags"].'</br>';
						echo $_SESSION["LAST_CHALfriends"].' '.$_POST["invite"].'</br>';
						
						echo (isset($_SESSION["LAST_CHALNamePT"]) && $_SESSION["LAST_CHALNamePT"] == $_POST["input_title_pt"] &&
							isset($_SESSION["LAST_CHALNameENG"]) && $_SESSION["LAST_CHALNameENG"] == $_POST["input_title_eng"] &&
							isset($_SESSION["LAST_CHALDescPT"]) && $_SESSION["LAST_CHALDescPT"] == $_POST["input_desc_pt"] &&
							isset($_SESSION["LAST_CHALDescENG"]) && $_SESSION["LAST_CHALDescENG"] == $_POST["input_desc_eng"] &&
							isset($_SESSION["LAST_CHALdtIni"]) && $_SESSION["LAST_CHALdtIni"] == $_POST["dtIni"] &&
							isset($_SESSION["LAST_CHALdtEnd"]) && $_SESSION["LAST_CHALdtEnd"] == $_POST["dtFim"] &&
							isset($_SESSION["LAST_CHALtags"]) && $_SESSION["LAST_CHALtags"] == $_POST["tags"] &&
							isset($_SESSION["LAST_CHALfriends"]) && $_SESSION["LAST_CHALfriends"] == $_POST["invite"]);
							    );*/
							
						if (isset($_SESSION["LAST_CHALNamePT"]) && $_SESSION["LAST_CHALNamePT"] == $_POST["input_title_pt"] &&
							isset($_SESSION["LAST_CHALNameENG"]) && $_SESSION["LAST_CHALNameENG"] == $_POST["input_title_eng"] &&
							isset($_SESSION["LAST_CHALDescPT"]) && $_SESSION["LAST_CHALDescPT"] == $_POST["input_desc_pt"] &&
							isset($_SESSION["LAST_CHALDescENG"]) && $_SESSION["LAST_CHALDescENG"] == $_POST["input_desc_eng"] &&
							isset($_SESSION["LAST_CHALdtIni"]) && $_SESSION["LAST_CHALdtIni"] == $_POST["dtIni"] &&
							isset($_SESSION["LAST_CHALdtEnd"]) && $_SESSION["LAST_CHALdtEnd"] == $_POST["dtFim"] &&
							isset($_SESSION["LAST_CHALtags"]) && $_SESSION["LAST_CHALtags"] == $_POST["tags"] &&
							isset($_SESSION["LAST_CHALfriends"]) && $_SESSION["LAST_CHALfriends"] == $_POST["invite"])
							{
								$_SESSION["PutChallengeSuccess"] = 0;
								//echo 'DEBUG: Is postback';
							}
							else
							{
							
						
						$target_dir = "uploads/";
						$date = date("Y-m-d H:i:s");
						
						$id = generateRandomString(10);
						
						
						/****** Imagem do challenge **************/
						
						$target_file = $target_dir . $_SESSION['userIdUser'].'_'.$id.'_'. basename($_FILES["myfiles"]["name"]);
						
						$URL_image = $target_file;
						
						$uploadOk = 1;
						$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
						
						//echo "$imageFileType: ".$imageFileType."<br/>";
						
						//echo "POST[submit]: ".$_POST["submit"]."<br/>";
						
						// Check if image file is a actual image or fake image
						if(isset($_POST["upload"])) {
							$check = getimagesize($_FILES["myfiles"]["tmp_name"]);
							if($check !== false) {
								//echo "File is an image - " . $check["mime"] . ".";
								$uploadOk = 1;
								} else {
								$Message = $lang['FILEISNOTANIMAGELOGO'];
								echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
								$uploadOk = 0;
							}
						}
						
						// Check if file already exists
						if (file_exists($target_file)) {
							//$Message = "Sorry, file already exists.";
							//echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Check file size
						if ($_FILES["myfiles"]["size"] > 500000) {
							$Message = $lang['FILEISTOOLARGELOGO'];
							echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Allow certain file formats
						if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
							//Sorry, only JPG, JPEG, PNG & GIF files are allowed
							//$Message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
							//echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Check if $uploadOk is set to 0 by an error
						if ($uploadOk == 0) {
							$Message = $lang['FILEWASNOTUPLOADEDLOGO'];
							// if everything is ok, try to upload file
							} else {
							if (move_uploaded_file($_FILES["myfiles"]["tmp_name"], $target_file)) {
								//$Message = "The file ". basename( $_FILES["myfiles"]["name"]). " has been uploaded.";
								//echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
								} else {
								$Message = $lang['FILEWASNOTUPLOADEDLOGO'];
								echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							}
						}
						
						/****** Imagem do voucher **************/
						$id2 = generateRandomString(10);
						
						//echo '$id2: '.$id2."<br/>";
						
						$target_file = $target_dir . $_SESSION['userIdUser'].'_'.$id2.'_'. basename($_FILES["myfiles2"]["name"]);
						
						$URL_voucher = $target_file;
						//echo "$target_file: ".$target_file."<br/>";
						
						$uploadOk = 1;
						$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
						
						//echo "$imageFileType: ".$imageFileType."<br/>";
						
						//echo "POST[submit]: ".$_POST["submit"]."<br/>";
						
						// Check if image file is a actual image or fake image
						if(isset($_POST["upload"])) {
							$check = getimagesize($_FILES["myfiles2"]["tmp_name"]);
							if($check !== false) {
								//echo "File is an image - " . $check["mime"] . ".";
								$uploadOk = 1;
								} else {
								$Message = $lang['FILEISNOTANIMAGEVOUCHER'];
								echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
								$uploadOk = 0;
							}
						}
						
						// Check if file already exists
						if (file_exists($target_file)) {
							//$Message = "Sorry, file already exists.";
							//echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Check file size
						if ($_FILES["myfiles2"]["size"] > 500000) {
							$Message = $lang['FILEISTOOLARGEVOUCHER'] ;
							echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Allow certain file formats
						if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
						&& $imageFileType != "gif" ) {
							//$Message = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
							//echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							$uploadOk = 0;
						}
						
						// Check if $uploadOk is set to 0 by an error
						if ($uploadOk == 0) {
							$Message = $lang['FILEWASNOTUPLOADEDVOUCHER'];
							echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							// if everything is ok, try to upload file
							} else {
							if (move_uploaded_file($_FILES["myfiles2"]["tmp_name"], $target_file)) {
								//$Message = "The file ". basename( $_FILES["myfiles2"]["name"]). " has been uploaded.";
								//echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
								} else {
								$Message = $lang['FILEWASNOTUPLOADEDVOUCHER'];
								echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
							}
						}
						
						$chalNamePT = $_POST['input_title_pt'];
						$chalNameENG = $_POST['input_title_eng'];
						$descPT = $_POST['input_desc_pt'];
						$descENG = $_POST['input_desc_eng']; 
						$tpUtil = $_POST['userType']; //Guest or registed
						
						$arr_ageRange = array();
						foreach ($_POST['cmbAgeRange'] as $ages)
						array_push($arr_ageRange, $ages);
						
						$arr_gender = array();
						foreach ($_POST['cmbGender'] as $genders)
						array_push($arr_gender, $genders);
						
						$arr_marital = array();
						foreach ($_POST['cmbMaritalStatus'] as $marital)
						array_push($arr_marital, $marital);
						
						$country = $_POST['cmbCountry'];
						
						$arr_city = array();
						foreach ($_POST['cmbcity'] as $cities)
						array_push($arr_city, $cities);
						
						$arr_studies = array();
						foreach ($_POST['cmbEducationLevel'] as $studies)
						array_push($arr_studies, $studies);
						
						$arr_childrange = array();
						foreach ($_POST['cmbChildNumber'] as $childs)
						array_push($arr_childrange, $childs);
						
						$arr_jobs = array();
						foreach ($_POST['cmbJobSector'] as $jobs)
						array_push($arr_jobs, $jobs);
						
						$dtIni = $_POST['dtIni']; 
						$dtEnd = $_POST['dtFim']; 
						
						$arr_textID = array();
						foreach ($_POST['chkTexts'] as $textID)
						if ($textID != "") array_push($arr_textID, $textID);
						
						$arr_textText = array();
						foreach ($_POST['txtTexts'] as $textText)
						if ($textText != "") array_push($arr_textText, $textText);
						
						$arr_image = array();
						foreach ($_POST['conta_img'] as $image)
						array_push($arr_image, $image);
						
						$arr_video = array();
						foreach ($_POST['conta_vid'] as $video)
						array_push($arr_video, $video);
						
						$arr_audioID = array();
						foreach ($_POST['chkAudio'] as $audioID)
						if ($audioID != "") array_push($arr_audioID, $audioID);
						
						$arr_audioText = array();
						foreach ($_POST['txtAudio'] as $audioText)
						if ($arr_audioText != "") array_push($arr_audioText, $audioText);
						
						
						$tags = $_POST['tags']; 
						$friends = $_POST['invite'];
						
						/*echo 'DEBUG index<br/>';
							
							if (!isset($chalNamePT) || $chalNamePT == "") 
							echo "chalNamePT: falta<br/>";
							else 
							echo "chalNamePT: tem<br/>";
							
							if (!isset($chalNameENG) || $chalNameENG == "") 
							echo "chalNameENG: falta<br/>";
							else 
							echo "chalNameENG: tem<br/>";
							
							if (!isset($descPT) || $descPT == "") 
							echo "descPT: falta<br/>";
							else 
							echo "descPT: tem<br/>";
							
							if (!isset($descENG) || $descENG == "") 
							echo "descENG: falta<br/>";
							else 
							echo "descENG: tem<br/>";
							
							if (!isset($tpUtil) || $tpUtil == "") 
							echo "tpUtil: falta<br/>";
							else 
							echo "tpUtil: tem<br/>";
							
							if (!isset($URL_image) || $URL_image == "") 
							echo "URL_image: falta<br/>";
							else 
							echo "URL_image: tem<br/>";
							
							if (!isset($URL_voucher) || $URL_voucher == "") 
							echo "URL_voucher: falta<br/>";
							else 
							echo "URL_voucher: tem<br/>";
							
							if (count($arr_ageRange) == 0) 
							echo "arr_ageRange: falta<br/>";
							else 
							echo "arr_ageRange: tem<br/>";
							
							if (count($arr_gender) == 0) 
							echo "arr_gender: falta<br/>";
							else 
							echo "arr_gender: tem<br/>";
							
							if (count($arr_marital) == 0) 
							echo "arr_marital: falta<br/>";
							else 
							echo "arr_marital: tem<br/>";
							
							if (!isset($country) || $country == "") 
							echo "country: falta<br/>";
							else 
							echo "country: tem<br/>";
							
							if (count($arr_city) == 0) 
							echo "arr_city: falta<br/>";
							else 
							echo "arr_city: tem<br/>";
							
							if (count($arr_studies) == 0) 
							echo "arr_studies: falta<br/>";
							else 
							echo "arr_studies: tem<br/>";
							
							if (count($arr_childrange) == 0) 
							echo "arr_childrange: falta<br/>";
							else 
							echo "arr_childrange: tem<br/>";
							
							if (count($arr_jobs) == 0) 
							echo "arr_jobs: falta<br/>";
							else 
							echo "arr_jobs: tem<br/>";
							
							if (!isset($dtIni) || $dtIni == "") 
							echo "dtIni: falta<br/>";
							else 
							echo "dtIni: tem<br/>";
							
							if (!isset($dtEnd) || $dtEnd == "") 
							echo "dtEnd: falta<br/>";
							else 
							echo "dtEnd: tem<br/>";
							
							if (count($arr_audioID) == 0) 
							echo "arr_audioID: falta<br/>";
							else 
							echo "arr_audioID: tem<br/>";
							
							if (count($arr_audioText) == 0) 
							echo "arr_audioText: falta<br/>";
							else 
							echo "arr_audioText: tem<br/>";
							
							if (count($arr_image) == 0) 
							echo "arr_image: falta<br/>";
							else 
							echo "arr_image: tem<br/>";
							
							if (count($arr_video) == 0) 
							echo "arr_video: falta<br/>";
							else 
							echo "arr_video: tem<br/>";
							
							if (count($arr_video) == 0) 
							echo "arr_video: falta<br/>";
							else 
							echo "arr_video: tem<br/>";
							
							if (count($arr_textID) == 0) 
							echo "arr_textID: falta<br/>";
							else 
							echo "arr_textID: tem<br/>";
							
							if (count($arr_textText) == 0) 
							echo "arr_textText: falta<br/>";
							else 
						echo "arr_textText: tem<br/>";*/
						
						
						if (!isset($chalNamePT) || $chalNamePT == "" || 
						!isset($chalNameENG) || $chalNameENG == "" || 
						!isset($descPT) || $descPT == ""|| 
						!isset($descENG) || $descENG == ""|| 
						!isset($tpUtil) || $tpUtil == ""|| 
						!isset($URL_image) || $URL_image == ""|| 
						!isset($URL_voucher) || $URL_voucher == ""|| 
						count($arr_ageRange) == 0 || 
						count($arr_gender) == 0 ||
						count($arr_marital) == 0 ||
						!isset($country) || $country == ""|| 
						count($arr_city) == 0 || 
						count($arr_studies) == 0 ||
						count($arr_childrange) == 0 ||
						count($arr_jobs) == 0 ||
						!isset($dtIni) || $dtIni == ""|| 
						!isset($dtEnd) || $dtEnd == "" 
						)
						{
							$Message = $lang['ALLFIELDSOBLIGATORY'];
							echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
							
						}
						else
						{
							if ((count($arr_audioID) > 0 && count($arr_audioText) > 0 && count($arr_audioID) == count($arr_audioText)) ||
							count($arr_image) > 0 ||
							count($arr_video) > 0 ||
							(count($arr_textID) > 0 && count($arr_textText) > 0 && count($arr_textID) == count($arr_textText)))
							{
								
								@putChallenge ($chalNamePT,
								$chalNameENG,
								$descPT, 
								$descENG, 
								$tpUtil, //Guest or registed
								$URL_image, 
								$URL_voucher, 
								$arr_ageRange, 
								$arr_gender, 
								$arr_marital, 
								$country, 
								$arr_city, 
								$arr_studies, 
								$arr_childrange, 
								$arr_jobs, 
								$dtIni, 
								$dtEnd, 
								$arr_audioID, 
								$arr_audioText,
								$arr_image, 
								$arr_video, 
								$arr_textID, 
								$arr_textText,
								$tags, 
								$friends);
								
							}
							else
							{
								$Message = $lang['ERRORMETADATA'];
								echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
								
							}
							
							
							
						}
							}
						break;
						case 'registerInvest': $name = $_POST['tbAdminName'];
												$email = $_POST['tbAdminEmail'];
												$pass = $_POST['tbAdminPass'];
												$phone = $_POST['tbAdminPhone'];
												$company = $_POST['tbAdminCompany'];
												$country = $_POST['cmbCountry'];
												$city = $_POST['cmbcity'];
												
												if (filter_var($email, FILTER_VALIDATE_EMAIL))
												{
													if ($name != '' && $email != '' && $pass != '' && $phone != '' &&
													$company != '' && $country != '' && $city != '')
														@registerInvest($name, $email, $pass, $phone, $company, $country, $city);
													else
													{
														$Message = $lang['ALLFIELDSOBLIGATORY'];
														echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
													}
												}
												else
												{
													$Message = $lang['EMAILERROR'];
													echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
												}
						break;
						case 'BlockInvestigator': @blockInvestigator($_POST['idInvestigator']);
						break;
						case 'SelectTab': $_SESSION["SelectedTab"] = $_POST['idSelectedTab'];
						//echo 'Tab: '.$_SESSION["SelectedTab"];
						
						break;	
						case 'detailChallenge': $_SESSION["idChallenge"] = $_POST['idChallenge'];
						//echo 'DEBUG: Defini o chellenge '.$_SESSION["idChallenge"];
						//echo 'DEBUG: Language detailChallenge: '.$_SESSION['language'];
						break;
						case 'deleteChallenge': @deleteChallenge($_POST['idChallenge']);
						break;
						case 'EditInvestigator': @editInvestigator($_POST['idInvestigator']);
						break;
						case 'SaveAlterationsInvest': //echo 'OK';
														$idInvest = $_POST['idInvestE'];
														$name = $_POST['tbAdminNameE'];
														$email = $_POST['tbAdminEmailE'];
														$pass = $_POST['tbAdminPassE'];
														$phone = $_POST['tbAdminPhoneE'];
														$company = $_POST['tbAdminCompanyE'];
														$country = $_POST['cmbCountryE'];
														$city = $_POST['cmbcityE'];
														
														/*echo 'idInvest: '.$idInvest;
														echo 'name: '.$name;
														echo 'email: '.$email;
														echo 'pass: '.$pass;
														echo 'phone: '.$phone;
														echo 'company: '.$company;
														echo 'country: '.$country;
														echo 'city: '.$city;*/
						
						
						if ($idInvest != '' && $name != '' && $email != '' && $pass != '' && $phone != '' &&
							$company != '' && $country != '' && $city != '')
							@updateInvest($idInvest, $name, $email, $pass, $phone, $company, $country, $city);
						else
						{
							$Message = $lang['ALLFIELDSOBLIGATORY'];
							echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
						}
						break;
					}
				}
				
				/******************* ESCOLHER O CONTEÚDO DA PÁGINA DE ACORDO COM O UTILIZADOR **********************/
				// Se ainda não fez login mostra a opção de login
				if (!isset($_SESSION["userType"]) || $_SESSION["userType"] == "")
				include("login.php"); 
				else
				{
					if ($_SESSION["userType"] == 1)
					include("admin.php");
					else if ($_SESSION["userType"] == 2)
					include("investigator.php");
					else
					{	
						$Message = "";
						include("login.php"); 
						$Message = "Não tem permissões para utilizar este serviço.";
						echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
					}
				}
			?>
		</div>
		
		<!--************************ COMUNICAÇÃO ENTRE O CLIENT E O SERVER POR POST ************************ -->
		<form name = "frmParameters" action="index.php" method = "POST">
			<input type="hidden" name="task" value="">
			<input type="hidden" name="language" value="">
			<input type="hidden" name="idInvestigator" value="">
			<input type="hidden" name="idSelectedTab" value="">
			<input type="hidden" name="idChallenge" value="1000">
		</form><!-- form -->
		<input type="hidden" id="hdfieldLanguage" value = "<?php echo $_SESSION["language"]; ?>">
		<input type="hidden" id="hdfieldChallengeSuccess" value = "<?php if (isset($_SESSION["PutChallengeSuccess"]) && $_SESSION["PutChallengeSuccess"] == 1 ) echo '1'; else echo '0';?>">

		<!--************************ RODAPÉ ************************ -->
		<?php include("footer.php"); ?> 
	</body>
	<script type="text/javascript">
		/*$(function () {
			$('ul.tabs li:first').addClass('active');
			$('.block article').hide();
			$('.block article:first').show();
			$('ul.tabs li').on('click', function () {
			$('ul.tabs li').removeClass('active');
			$(this).addClass('active')
			$('.block article').hide();
			var activeTab = $(this).find('a').attr('href');
			$(activeTab).show();
			return false;
			});
		})*/
		$(function () {
			if( localStorage['visited']==="undefined" || localStorage['visited']==""  || localStorage['visited']==="" || localStorage['visited']==null || localStorage['visited']===null) {
				$('ul.tabs li:first').addClass('active');
				$('.block article').hide();
				$('.block article:first').show();	
				localStorage['visited'] = $('.tabs .active a').attr('href');
				localStorage['details']=0;
				localStorage['detalhes']=0;
			}
			else{
				$('ul.tabs li:first').addClass('active');
				$('.block article').hide();
				$('.block article:first').show();
				openTab(localStorage['visited'].substring(1,localStorage['visited'].length));
			}
			
			function openTab(tab){
				$('ul.tabs li').removeClass('active');
				$('a[href*='+tab+']').parent('li').addClass('active')
				$('.block article').hide();
				localStorage['visited'] = $('.tabs .active a').attr('href');
				$('#'+tab).show();
				if(tab != "tab2")
				{
					localStorage['details']=0;
					localStorage['detalhes']=0;
				}
				return false;
			}
			
			$('ul.tabs li').on('click', function () {
				$('ul.tabs li').removeClass('active');
				$(this).addClass('active')
				$('.block article').hide();
				var activeTab = $(this).find('a').attr('href');
				localStorage['visited'] = $('.tabs .active a').attr('href');
				$(activeTab).show();
				var tab =localStorage['visited'].substring(1,localStorage['visited'].length);
				
				if(tab == "tab1")
				{
					localStorage['details']=0;
					localStorage['detalhes']=0;
				}
				return false;
			});
			
		})
		
		
		
		$("#user").change(function () {
			if ($("#user option:selected").val() == 2)
            $("#registred").css("display", "");
			else
            $("#registred").css("display", "none");
		});
		
		$("#check_audio").change(function () {
			if ($("#check_audio").is(':checked'))
            $("#sel_audio").css("display", "");
			else
            $("#sel_audio").css("display", "none");
		});
		
		$("#check_video").change(function () {
			if ($("#check_video").is(':checked'))
            $("#sel_video").css("display", "");
			else
            $("#sel_video").css("display", "none");
		});
		
		$("#check_img").change(function () {
			if ($("#check_img").is(':checked'))
            $("#sel_img").css("display", "");
			else
            $("#sel_img").css("display", "none");
		});
		
		$("#check_txt").change(function () {
			if ($("#check_txt").is(':checked'))
            $("#sel_txt").css("display", "");
			else
            $("#sel_txt").css("display", "none");
		});
		
		$("#check_rwd").change(function () {
			if ($("#check_rwd").is(':checked'))
            $("#sel_rwd").css("display", "");
			else
            $("#sel_rwd").css("display", "none");
		});
		
		$("#myfiles").change(function () {
			if ($("#myfiles")[0].files[0].size > 500000)
			{
				message = "<?php echo $lang['FILEISTOOLARGELOGO']; ?>";
				generate('warning', message);
				$("#myfiles").val("");
			}
		});
		$("#myfiles2").change(function () {
			if ($("#myfiles2")[0].files[0].size > 500000)
			{
				message = "<?php echo $lang['FILEISTOOLARGEVOUCHER']; ?>";
				generate('warning', message);
				$("#myfiles2").val("");
			}
		});
		
		
		/*$('#tbl_challenge tr .remove').click(function () {
			$(this).parent().remove();
			return false;
		});*/
		
		
		
		function clean() {
			document.getElementById('username').value = '';
			document.getElementById('password').value = '';
		}
		
		
		function tryLogin() {
			if (document.getElementById('username').value.length==0) {
				generate('error', '<?php echo $lang['USERNAMEERROR']; ?>');
				document.getElementById('username').focus();
				} else if (document.getElementById('password').value.length==0) {
				generate('error', '<?php echo $lang['PASSWORDERROR']; ?>');
				document.getElementById('password').focus();
			} else 
			document.frmLogin.submit();
		}
		
		function logout() {
			document.frmParameters.task.value='logout';
			document.frmParameters.submit();
		}
		
		function setLanguage(lang) {
			document.frmParameters.language.value= lang;
			document.frmParameters.submit();
		}
		
		function openDetail(val)
		{
			elem = document.getElementById(val);
			switch (val)
			{
				
				case "chk_1": x = elem.checked; // Texto
				if (x)
				$("#sel_txt").css("display", "");
				else
				$("#sel_txt").css("display", "none");
				break;
				case "chk_2": x = elem.checked; // Imagem
				if (x)
				$("#sel_img").css("display", "");
				else
				$("#sel_img").css("display", "none");
				break;
				case "chk_3": x = elem.checked; // Vídeo
				if (x)
				$("#sel_video").css("display", "");
				else
				$("#sel_video").css("display", "none");
				break;
				case "chk_4": x = elem.checked; // Audio
				if (x)
				$("#sel_audio").css("display", "");
				else
				$("#sel_audio").css("display", "none");
				break;
			}
		}
		
		$(document).on('change','#cmbCountry',function(){
			var val = $(this).val();
			$.ajax({
				url: 'bindComboCity.php',
				data: {cmbCountry:val, all:'<?php echo $lang['ALL_F']; ?>'},
				type: 'GET',
				dataType: 'html',
				success: function(result){
					$('#cmbcity').html();  
					$('#cmbcity').html(result); 
				}
			});
		});
		
		function tryPutChallenge(){
			
			
			if($('#input_title_pt').val()=="")
			{
				$("#input_title_pt").show();
				$("#input_title_eng").hide();
				$('#title_pt').addClass('bolda');
				$('#title_eng').removeClass('bolda');
				generate('error', '<?php echo $lang['CHALLNAMEPT']; ?>');
				$('#input_title_pt').focus();
				
			} else if ($('#input_title_eng').val()=="")
			{
				$("#input_title_pt").hide();
				$("#input_title_eng").show();
				$('#title_eng').addClass('bolda');
				$('#title_pt').removeClass('bolda');
				generate('error', '<?php echo $lang['CHALLNAMEENG']; ?>');
				$('#input_title_eng').focus();
			} else if ($('#input_desc_eng').val()=="")
			{
				$("#input_desc_pt").hide();
				$("#input_desc_eng").show();
				$('#desc_eng').addClass('bolda');
				$('#desc_pt').removeClass('bolda');
				generate('error', '<?php echo $lang['CHALLDESCENG']; ?>');
				$('#input_desc_eng').focus();
			} else if ($('#input_desc_pt').val()=="")
			{
				$("#input_desc_pt").show();
				$("#input_desc_eng").hide();
				$('#desc_pt').addClass('bolda');
				$('#desc_eng').removeClass('bolda');
				generate('error', '<?php echo $lang['CHALLDESCPT']; ?>');
				$('#input_desc_pt').focus();
			} else if ($('#dtIni').val()=="")
			{
				generate('error', '<?php echo $lang['CHALLDATAINI']; ?>');
				$('#dtIni').focus();
			} else if ($('#dtFim').val()=="")
			{
				generate('error', '<?php echo $lang['CHALLDATAFIM']; ?>');
				$('#dtFim').focus();
			} else if ($('#myfiles').val()=="")
			{
				generate('error', '<?php echo $lang['CHALLIMG']; ?>');
				$('#myfiles').focus();
			} else if ($('#myfiles2').val()=="")
			{
				generate('error', '<?php echo $lang['CHALLVOUCHER']; ?>');
				$('#myfiles2').focus();
			}
			else if ($('#chkMedias input:checked').length == 0) // Validar que pelo menos uma das checkboxes estão validadas
			{
				generate('error', '<?php echo $lang['CHALLMEDIA']; ?>');
				$('#tags').focus();
			}
			else if ($('#chk_1').is(':checked') && $('#sel_txt input:checked').length == 0) // Texto
			{
				generate('error', '<?php echo $lang['CHALLMETATXT']; ?>');
				$('#sel_txt').focus();
			}
			else if ($('#conta_txt').is(':checked') && $('#conta_txt').next().val() == "") // Texto - Validar que a palavra está preenchida
			{
				generate('error', '<?php echo $lang['CHALLWORD']; ?>');
				$('#conta_txt').next().focus();
			}
			else if ($('#chk_2').is(':checked') && $('#sel_img input:checked').length == 0) // Imagem
			{
				generate('error', '<?php echo $lang['CHALLMETAIMG']; ?>');
				$('#sel_txt').focus();
			}
			else if ($('#chk_3').is(':checked') && $('#sel_video input:checked').length == 0) // Vídeo
			{
				generate('error', '<?php echo $lang['CHALLMETAVID']; ?>');
				$('#sel_txt').focus();
			}
			else if ($('#chk_4').is(':checked') && $('#sel_audio input:checked').length == 0) // Audio
			{
				generate('error', '<?php echo $lang['CHALLMETAAUD']; ?>');
				$('#sel_txt').focus();
			}
			else if ($('#conta_aud').is(':checked') && $('#conta_aud').next().val() == "") // Audio - Validar que a palavra está preenchida
			{
				generate('error', '<?php echo $lang['CHALLWORD']; ?>');
				$('#conta_aud').next().focus();
			}
			else if ($('#tags').val()=="")
			{
				generate('error', '<?php echo $lang['CHALLTAGS']; ?>');
				$('#tags').focus();
			}
			else
				document.main_form.submit();
		}
		
		function tryRegisterInvestigator(){
			if($('#tbAdminName').val()!="" &&
			$('#tbAdminEmail').val()!="" &&
			$('#tbAdminPass').val()!="" &&
			$('#tbAdminPhone').val()!="" &&
			$('#tbAdminCompany').val()!="" &&
			$('#cmbCountry').val()!= 0 &&
			$('#cmbcity').val()!= 0 &&
			!isNaN($('#tbAdminPhone').val()) &&
			looksLikeMail($('#tbAdminEmail').val()))
			{
				document.frmRegister.submit();
			}
			else
			{
				if (isNaN($('#tbAdminPhone').val()))
				{
					message = "<?php echo $lang['NUMBERERROR']; ?>";
					generate('warning', message);
					$('#tbAdminPhone').focus();
				}
				else if (!looksLikeMail($('#tbAdminEmail').val()))
				{
					message = "<?php echo $lang['EMAILERROR'];?>";
					generate('warning', message);
				}
				else
				{
					message = "<?php echo $lang['ALLFIELDSOBLIGATORY'];?>";
					generate('warning', message);
				}
			}
		}
		
		function cleanAdmin() {
			$('#tbAdminName').val("");
			$('#tbAdminEmail').val("");
			$('#tbAdminPass').val("");
			$('#tbAdminPhone').val("");
			$('#tbAdminCompany').val("");
			$('#cmbCountry').val(0);
			$('#cmbcity').val(0);
			
		}
		
		function looksLikeMail(str) {
			var lastAtPos = str.lastIndexOf('@');
			var lastDotPos = str.lastIndexOf('.');
			return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
		}
		
		function editInvestigator(id)
		{
			/*//window.alert($('#hdfieldLanguage').val());
				$.ajax({
				url: 'EditInvestigatorData.php',
				data: {idInvestigator:id, lang:$('#hdfieldLanguage').val()},
				type: 'GET',
				dataType: 'html',
				success: function(result){
				$('#editInvestigator').html();  
				$('#editInvestigator').html(result); 
				$("#div_edit").show();
				$("#adminTab2").hide();
				$("#div_search").hide();
				}
			});*/
			
			document.frmParameters.idInvestigator.value = id;
			document.frmParameters.task.value = 'EditInvestigator';
			document.frmParameters.submit();
		}
		

		function blockInvestigator(id)
		{
			var textLanguage = '<?php echo $lang['INVESTCONFIRM'];?>';
			var n = noty({
				text: textLanguage,
				type: 'confirm',
				dismissQueue: false,
				layout: 'center',
				theme: 'defaultTheme',
				buttons: [
				{addClass: 'btn btn-primary', text: '<?php echo $lang['OK'];?>', onClick: function($noty) {
					
					// this = button element
					// $noty = $noty element
					
					$noty.close();
					document.frmParameters.idInvestigator.value = id;
					document.frmParameters.task.value = 'BlockInvestigator';
					document.frmParameters.submit();
					//noty({text: 'You clicked "Ok" button', type: 'success'});
				}
				},
				{addClass: 'btn btn-danger', text: '<?php echo $lang['CANCEL'];?>', onClick: function($noty) {
					$noty.close();
					//noty({text: 'You clicked "Cancel" button', type: 'error'});
				}
				}
				]
				
				
			})

		}
		
		function setSelectedTab(id, server)
		{
			if (id != server)
			{
				document.frmParameters.idSelectedTab.value = id;
				document.frmParameters.task.value = 'SelectTab';
				document.frmParameters.submit();
			}
		}
		
		
		
		function deleteChallenge(id)
		{
			var textLanguage = '<?php echo $lang['CHALLENGECONFIRM'];?>';
			var n = noty({
				text: textLanguage,
				type: 'confirm',
				dismissQueue: false,
				layout: 'center',
				theme: 'defaultTheme',
				buttons: [
				{addClass: 'btn btn-primary', text: '<?php echo $lang['OK'];?>', onClick: function($noty) {
					
					// this = button element
					// $noty = $noty element
					
					$noty.close();
					document.frmParameters.task.value = 'deleteChallenge';
					document.frmParameters.children.idChallenge.value = id;
					document.frmParameters.submit();
					//noty({text: 'You clicked "Ok" button', type: 'success'});
				}
				},
				{addClass: 'btn btn-danger', text: '<?php echo $lang['CANCEL'];?>', onClick: function($noty) {
					$noty.close();
					//noty({text: 'You clicked "Cancel" button', type: 'error'});
				}
				}
				]
				
				
			})
			/*document.frmParameters.task.value = 'deleteChallenge';
				
				document.frmParameters.children.idChallenge.value = id;
			document.frmParameters.submit();*/
		}
		
		$(document).on('change','#current_time',function(){
			var language;
			var val = $(this).val();
			if ($('#hdfieldLanguage').val() != '')
			language = $('#hdfieldLanguage').val()
			else
			language = 'PT';
			$.ajax({
				url: 'bindDataTimeline.php',
				data: {current_time:val, chall:$('#hdfieldIdChallenge').val(), lang:language},
				type: 'GET',
				dataType: 'html',
				success: function(result){
					$('#dados').html();  
					$('#dados').html(result); 
				}
			});
		});
		
		$(document).on('click','#bt_refresh',function(){
			var language;
			var val = $('#current_time').val();
			if ($('#hdfieldLanguage').val() != '')
			language = $('#hdfieldLanguage').val()
			else
			language = 'PT';
			$.ajax({
				url: 'bindDataTimeline.php',
				data: {current_time:val, chall:$('#hdfieldIdChallenge').val(), lang:language},
				type: 'GET',
				dataType: 'html',
				success: function(result){
					$('#dados').html();  
					$('#dados').html(result); 
				}
			});
		});	
		
		$(document).on('click','#bt_export',function(){
			
			var time = $('#current_time').val();
			var chall = $('#hdfieldIdChallenge').val();
			
			$('#idChallange').val(chall);
			$('#refDate').val(time);
			$('#frmExport').submit();
		});	
		
		
		
		function detailChallenge(id)
		{
			document.frmParameters.task.value = 'detailChallenge';
			document.frmParameters.children.idChallenge.value = id;
			document.frmParameters.submit();
		}	
		
</script>
</html>

