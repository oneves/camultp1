<?php
	
if (!isset($_GET['idchallenge']) || $_GET['idchallenge'] == "" && !isset($_GET['lang']) || $_GET['lang'] == "")
{
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>Mobile Diary</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<!--JQuery-->
	<script src="import/jquery-1.12.0.min.js"></script>
	<script src="import/jquery-1.12.2.min.js"></script>
	<script src="import/jquery-1.12.2.js"></script>
	<!--Calendar-->
	<link rel="stylesheet" href="import/jquery-ui.css" />
	<script src="import/jquery-ui.js"></script>
	<!--TimePicker-->
	<link rel="stylesheet" href="import/time/jquery-ui-timepicker.css" />
	<script src="import/time/jquery-ui-timepicker.js"></script>
</head>
<body>
    <div id="header1">build to serve you</div>
    <div id="header2"></div>
	<img id="logo" src="images/logo.png" />
	<div class="conv_title">
        <?php echo "Link inativo/ Desactivated link."; ?>
    </div>
       <div class="conv_desc">
    </div>
    <div>
        <img id="conv_logo" src="images/logo.png" />
    </div>
    <div>
        <img id="conv_logo" src="images/playstore.png" />
    </div>
    <footer>
        Copyright � 2016 UC - CAMUL - TEAM DESIGN & MARKETING
    </footer>
</body>
</html>
<?php
}
else
{
$idchallenge = $_GET['idchallenge'];

/*
$host= gethostname();
$ip = gethostbyname($host);
echo $host;
echo $ip;

$ip = "localhost/mobilediary";*/

$DB_host = "localhost";
$DB_user = "root";
$DB_pass = "Asist2015";
$DB_name = "camul";
		
		try
		{

			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT Chal_Title, Chal_Title_ENG, Chal_Description, Chal_Description_ENG, 
										Chal_BeginDate, Chal_EndDate, Chal_LogoURL
										FROM TChallenge
										WHERE Chal_PK_IdChallenge = :idchallenge
										AND Chal_DeletedOn IS NULL');
			$stmt->bindparam(":idchallenge", $idchallenge);
			
			if ($stmt->execute())
			{
				
			$chal=$stmt->fetch(PDO::FETCH_ASSOC);
			
			if($stmt->rowCount() > 0)
			{
				$title = $chal['Chal_Title'];
				$text = $chal['Chal_Description'];
				$dtini = $chal['Chal_BeginDate'];
				$dtfim = $chal['Chal_EndDate'];
				
				$titleEng = $chal['Chal_Title_ENG'];
				$textEng = $chal['Chal_Description_ENG'];
				$url = $chal['Chal_LogoURL'];
			
				
			}
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
			

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Mobile Diary</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

    <div id="header1">build to serve you</div>
    <div id="header2"></div>
	<!--<div class="lang" ><a href ="javascript:setLanguage('PT')">PT</a> | <a href ="javascript:setLanguage('ENG')">ENG</a></div>-->
    <img id="logo" src="images/logo.png" />
	<div id = "divPT" style="display:<?php if ($_GET['lang'] == 'ENG') echo 'none'; else echo 'block'; ?>;">
	<div class="conv_title">
	
        <?php echo $title; ?>
    </div>
       <div class="conv_desc">
           <?php echo $text; ?>
    </div>
    <div class="conv_data">
        Data Inicio:<?php echo $dtini; ?>  - Data Fim:<?php echo $dtfim; ?>
    </div>
	</div>
	<div id = "divENG" style="display:<?php if ($_GET['lang'] == 'ENG') echo 'block'; else echo 'none'; ?>;">
		<div class="conv_title">
		
			<?php echo $titleEng; ?>
		</div>
		   <div class="conv_desc">
			   <?php echo $textEng; ?>
		</div>
		<div class="conv_data">
			Begin date:<?php echo $dtini; ?>  - End Date:<?php echo $dtfim; ?>
		</div>
	</div>
    <div>
        <img id="conv_logo" src="images/logo.png" />
    </div>
    <div>
        <img id="conv_logo" src="images/playstore.png" />
    </div>
	<body>
	<script type ="text/javascript">
	
	/*function setLanguage(x)
	{
	window.alert(x);
	if (x == 'PT')
	{
		$("#divENG").style = "display:none;";
		$("#divPT").style = "display:block;";
	}
	else
	{
		$("#divPT").style = "display:none;";
		$("#divENG").style = "display:block;";
	}

	}*/
</script>
    <footer>
        Copyright � 2016 UC - CAMUL - TEAM DESIGN & MARKETING
    </footer>

</body>
</html>
<?php
}
?>
