<div class="text"><?php echo $lang['NUMBER_CHILDREN']; ?></div>
<select multiple id = "cmbChildNumber" name = "cmbChildNumber[]" onchange="" >

	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT CRan_PK_IdChildrenRange, CRan_DescChildrenRange FROM TChildrenRange');
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					?>
					<option value = "<?php echo $row["CRan_PK_IdChildrenRange"]; ?>" 
							id="<?php echo $row["CRan_PK_IdChildrenRange"]; ?>" title = "<?php echo $row["CRan_DescChildrenRange"]; ?>">
									<?php echo $row["CRan_DescChildrenRange"]; ?></option>
						
						<?php
							
						}
						
				}
				else
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
				}
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
