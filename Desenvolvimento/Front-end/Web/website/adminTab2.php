	<br>
<div id="adminTab2">
<table id="tb2">
	<tr>
		<td>
			<div class="text"><?php echo $lang['LISTACTIVEINV']; ?></div>
		</td>
		<td><input type="button" id="bt_search" value=""></td>
		<td><input type="text" name="" id="search"></td>
	</tr>
</table>
	<?php
	
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$sql = "SELECT User_PK_IdUser, User_Name, DATE(User_InsertedOn) as date, COUNT(c.Chal_PK_IdChallenge) as count
					FROM TUsers u 
					LEFT JOIN TChallenge c ON u.User_PK_IdUser = c.Chal_FK_IdUserInvestigator
											AND c.Chal_DeletedOn IS NULL	
					WHERE User_FK_IdTypeUser = 2 
					AND User_DeletedOn IS NULL 
					GROUP BY User_PK_IdUser, User_Name, User_InsertedOn;"; 
			
			//echo $sql;
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					
					?>
					<table id="tbl_challenge2">
					
						<?php
						
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							
						?>
						<tr>
							<td class="td1"><?php echo $row["User_Name"];?></td>
							<td class="td2"><?php echo $lang['CREATED']; ?> <?php echo $row["date"];?>, <?php echo $row["count"];?> <?php echo $lang['CHALLENGES']; ?></td>
							<td class="edit" onclick="editInvestigator(<?php echo $row["User_PK_IdUser"];?>);"><?php echo $lang['EDIT']; ?></td>
							<td class="edit" onclick="blockInvestigator(<?php echo $row["User_PK_IdUser"];?>);");">X</td>
						</tr>
						<?php
							
						}
						?>
					
					</table>
					<?php
				}
				else
				{
					echo $lang['NOINVESTIGATORSCREATED'];
					
				}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
			RETURN FALSE;
		}
	
	?>
<div class="dot">....</div>
</div>
<div id="div_edit">
	<div id="fecha3" class="fecha">X</div>
	<div class="text"><?php echo $lang['EDIT']; ?></div>
    <br>
	<div id = "editInvestigator">
	<div class="text"><?php echo $lang['NAME']; ?></div>
		<form id = "frmEditInvestigator" name = "frmEditInvestigator" action = "index.php" method = "POST">
		<input type="text" name="tbAdminNameE" id="tbAdminNameE" value = "<?php echo $_SESSION["InvestNome"];?>" required><br>
		 <table id="tbl_edit">
								<tr>
									<td>
										<div class="text"><?php echo $lang['EMAIL']; ?></div>
										<input type="email" name="tbAdminEmailE" id="tbAdminEmailE" value = "<?php echo $_SESSION["InvestEmail"];?>" required><br>
									</td>
									<td>
										<div class="text"><?php echo $lang['PASSWORD']; ?></div>
										<input type="password" name="tbAdminPassE" id="tbAdminPassE" value = "<?php echo $_SESSION[""];?>" required><br>
									</td>
								</tr>
								<tr>
									<td>
										<div class="text"><?php echo $lang['PHONE']; ?></div>
										<input type="text" name="tbAdminPhoneE" id="tbAdminPhoneE" value = "<?php echo $_SESSION["InvestTelefone"];?>" required><br>
									</td>
									<td>
										<div class="text"><?php echo $lang['COMPANY']; ?></div>
										<input type="text" name="tbAdminCompanyE" id="tbAdminCompanyE"  value = "<?php echo $_SESSION["InvestEmpresa"];?>" required><br>
									</td>
								</tr>
								<tr>
									<td>
										<div class="text"><?php echo $lang['COUNTRY']; ?></div>
											<select id = "cmbCountryE" name = "cmbCountryE">
												<?php
													
													
													
													try
													{
														$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
														$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
														
														$stmt = $DB_con->prepare('SELECT Cntr_PK_IdCountry, Cntr_DescCountry, Cntr_DescCountry_ENG FROM TCountry ORDER BY Cntr_DescCountry');
														if ($stmt->execute())
														{
															
															if($stmt->rowCount() > 0)
															{
															?>
															<option value="0">(<?php if ($_SESSION["userType"] == 1) 
																									echo $lang['SELECT'];
																								else
																									echo $lang['ALL_M']; ?>)</option>
															<?php
																
																while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
																	
																?>
																<option value = "<?php echo $row["Cntr_PK_IdCountry"]; ?>" 
																		id="<?php echo $row["Cntr_PK_IdCountry"]; ?>" 
																			<?php if ($row["Cntr_PK_IdCountry"] == $_SESSION["InvestPais"]) echo 'selected'; ?>>
																					<?php echo $row["Cntr_DescCountry"]; ?></option>
																	
																	<?php
																		
																	}
																	
															}
															else
															{
															?>
															<option value="0" selected>(<?php if ($_SESSION["userType"] == 1) 
																									echo $lang['SELECT'];
																								else
																									echo $lang['ALL_M']; ?>)</option>
															<?php
																
															}
														}
													}
													
													catch(PDOException $e)
													{
														echo $e->getMessage();
													}
													
												?>
												
																	 
											</select>
									</td>
									<td>
										<div class="text"><?php echo $lang['CITY']; ?></div>
											<select id = "cmbcityE" name = "cmbcityE" required>
											<?php
																						

														$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
														$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
														
														$stmt = $DB_con->prepare('SELECT City_PK_IdCity, City_FK_IdCountry, City_DescCity, City_DescCity_ENG 
															FROM TCity WHERE City_FK_IdCountry =:idpais ORDER BY City_DescCity');
														$stmt->bindparam(":idpais", $_SESSION["InvestPais"]);
														
														if ($stmt->execute())
														{
															
															if($stmt->rowCount() > 0)
															{
															?>
															<option value="0">(<?php echo $lang['ALL_F']; ?>)</option>
															
															<?php
																
																while ($row = $stmt->fetch(PDO::FETCH_OBJ)){
																	
																	if (isset($_SESSION["InvestPais"]) && $_SESSION["InvestPais"] != "" && isset($_SESSION["InvestCidade"]) && $_SESSION["InvestCidade"] != "")
																	{
																		if ($row->City_PK_IdCity == $_SESSION["InvestCidade"])
																			$selected = 'selected';
																		else
																			$selected = '';
																	}
																
																echo "<option value='$row->City_PK_IdCity' $selected>$row->City_DescCity</option>";
																	
																		
																	}
																	
															}
															else
															{
															?>
															<option value="0" selected>(<?php echo $todos; ?>)</option>
															<?php
																
															}
														}

												?>
											</select>
									</td>
								</tr>
							</table>
							<input type="hidden" name="idInvestE" value="<?php echo $_SESSION["InvestID"]; ?>">
							<input type="hidden" name="task" value="SaveAlterationsInvest">
							<input type="button" id="bt_save" class="bts" value="<?php echo $lang['SAVE']; ?>" onclick="javascript:saveAlterationsInvest();">
		</form>
	</div>
	</div>
<div id="div_search">
	<div id="fecha2" class="fecha">X</div>
	<div class="text bot"><?php echo $lang['SEARCHRESULT']; ?></div>
	<div id="res_pesquisa"></div>
</div>
</div>
<script type="text/javascript">
	/*$("#bt_search").click(function () {	
		$("#adminTab2").hide();
		$("#div_search").show();
	});*/
	$("#fecha2").click(function () {	
		$("#adminTab2").show();
		$("#div_search").hide();
		localStorage['detalhes']=0;
	});
	$("#fecha3").click(function () {	
		$("#adminTab2").show();
		$("#div_search").hide();
		$("#div_edit").hide();
		localStorage['detalhes']=0;
	});
	$(".edit").click(function () {	
		localStorage['detalhes']=1;
	});
	if(localStorage['detalhes']!=0){
		$("#adminTab2").hide();
		$("#div_search").hide();
		$("#div_edit").show();
	}
	else{
		$("#div_edit").hide();
		$("#div_search").hide();
		$("#adminTab2").show();
	}

	$(document).on('change','#cmbCountryE',function(){
			var val = $(this).val();
			$.ajax({
				url: 'bindComboCity.php',
				data: {cmbCountry:val, all:'<?php echo $lang['ALL_F']; ?>'},
				type: 'GET',
				dataType: 'html',
				success: function(result){
					$('#cmbcityE').html();  
					$('#cmbcityE').html(result); 
				}
			});
		});
	
	
		
	$(document).on('click','#bt_search',function(){
			$("#adminTab2").hide();
			$("#div_search").show();
			var val = $('#search').val();
			if (val != "")
			{
				$.ajax({
					url: 'bindDataSearchInvestigators.php',
					data: {words:val},
					type: 'GET',
					dataType: 'html',
					success: function(result){
						$('#res_pesquisa').html();  
						$('#res_pesquisa').html(result); 
						
					}
				});
			}
		});	
		
function saveAlterationsInvest()
		{
			var textLanguage = '<?php echo $lang['UPDATECONFIRM'];?>';
			var n = noty({
				text: textLanguage,
				type: 'confirm',
				dismissQueue: false,
				layout: 'center',
				theme: 'defaultTheme',
				buttons: [
				{addClass: 'btn btn-primary', text: '<?php echo $lang['OK'];?>', onClick: function($noty) {
					
					// this = button element
					// $noty = $noty element
					
					$noty.close();
					//document.frmEditInvestigator.submit();
					tryEditInvestigator();
					//noty({text: 'You clicked "Ok" button', type: 'success'});
				}
				},
				{addClass: 'btn btn-danger', text: '<?php echo $lang['CANCEL'];?>', onClick: function($noty) {
					$noty.close();
					//noty({text: 'You clicked "Cancel" button', type: 'error'});
				}
				}
				]
				
				
			})
			/*document.frmParameters.task.value = 'deleteChallenge';
				
				document.frmParameters.children.idChallenge.value = id;
			document.frmParameters.submit();*/
		}

		function tryEditInvestigator(){
			if($('#tbAdminNameE').val()!="" &&
			$('#tbAdminEmailE').val()!="" &&
			$('#tbAdminPassE').val()!="" &&
			$('#tbAdminPhoneE').val()!="" &&
			$('#tbAdminCompanyE').val()!="" &&
			$('#cmbCountryE').val()!= 0 &&
			$('#cmbcityE').val()!= 0 &&
			!isNaN($('#tbAdminPhoneE').val()) &&
			looksLikeMail($('#tbAdminEmailE').val()))
			{
				document.frmEditInvestigator.submit();
			}
			else
			{
				if (isNaN($('#tbAdminPhoneE').val()))
				{
					message = "<?php echo $lang['NUMBERERROR']; ?>";
					generate('warning', message);
					$('#tbAdminPhoneE').focus();
				}
				else if (!looksLikeMail($('#tbAdminEmailE').val()))
				{
					message = "<?php echo $lang['EMAILERROR'];?>";
					generate('warning', message);
				}
				else
				{
					message = "<?php echo $lang['ALLFIELDSOBLIGATORY'];?>";
					generate('warning', message);
				}
			}
		}
		
</script>

