<?php
	/*
------------------
Language: English
------------------
*/
 
$lang = array();

$lang['TESTE_ADMIN'] = 'I\'m a administrator!';
$lang['TESTE_INVESTIGADOR'] = 'I\'m a investigator!';

$lang['AGE_RANGE'] = 'Age range';
$lang['JOB_SECTOR'] = 'Job sector';
$lang['EDUCATION_LEVEL'] = 'Education level';
$lang['GENDER'] = 'Gender';
$lang['MARITAL_STATUS'] = 'Marital status';
$lang['COUNTRY'] = 'Country';
$lang['CITY'] = 'City';
$lang['NUMBER_CHILDREN'] = 'Number of children';
$lang['ALL_M'] = 'All';
$lang['ALL_F'] = 'All';
$lang['SELECT'] = 'Select...';
$lang['NAME'] = 'Name';
$lang['EMAIL'] = 'E-mail';
$lang['PASSWORD'] = 'Password';
$lang['PHONE'] = 'Phone';
$lang['COMPANY'] = 'Company';
$lang['REGISTER'] = 'REGISTER INVESTIGATORS';
$lang['CREATE'] = 'CREATE';
$lang['CLEAR'] = 'CLEAR';
$lang['LISTACTIVE'] = 'LIST ACTIVE';
$lang['CREATED'] = 'created at';
$lang['CHALLENGES'] = 'challenges';
$lang['EDIT'] = 'Edit';
$lang['LISTACTIVEINV'] = 'LIST ACTIVE INVESTIGATORS';
$lang['REPLIES'] = 'replies';
$lang['DETAIL'] = 'Detail';
$lang['DETAILS'] = 'Details';
$lang['NEWCHALLENGE'] = 'NEW CHALLENGE';
$lang['LISTACTIVE'] = 'LIST ACTIVE';
$lang['CHALLENGENAME'] = 'Challenge Name';
$lang['DESCRIPTION'] = 'Description';
$lang['IMAGE'] = 'Image';
$lang['BEGINDATE'] = 'Initial date';
$lang['ENDDATE'] = 'Ending Date';
$lang['USERS'] = 'Target Public';
$lang['GUEST'] = 'Guests';
$lang['REGISTERED'] = 'Registered';
$lang['INVITEPARTICIPANTS'] = 'Invite participants';
$lang['OBLIGATORYFIELDS'] = 'Obligatory fields';
$lang['CHOOSEONE'] = 'Choose at least one';
$lang['CHALLENGESUCCESS'] = 'CHALLENGE CREATED WITH SUCCESS!';
$lang['CHALLENGEINSUCCESS'] = 'Unable to create the challenge.';
$lang['CREATENEW'] = 'CREATE NEW CHALLENGE';
$lang['INVITEPARTICIPANTSUPPER'] = 'INVITE PARTICIPANTS';
$lang['PARTICIPANTSINVITER'] = 'Invited participants';
$lang['PARTICIPANTSBLOCKED'] = 'Blocked participants';
$lang['NOINVITES'] = 'No one invited yet';
$lang['1DAY'] = '1 Day';
$lang['5DAYS'] = '5 Days';
$lang['15DAYS'] = '15 Days';
$lang['1HOUR'] = '1 Hour';
$lang['5HOURS'] = '5 Hours';
$lang['1MINUTE'] = '1 Minute';
$lang['5MINUTES'] = '5 Minutes';
$lang['NUMBERPARTICIPANTES'] = 'Number of Participants';
$lang['SHARE'] = 'Share on Facebook';
$lang['NOCHALLENGES'] = 'No challenges created';
$lang['CHALLENGENOTIDENTIFIED'] = 'Challenge not identified';
$lang['BLOCKADICIONALINFORMATION'] = 'Block adicional Information';
$lang['EMAILSSEPARATEDBYCOMMA'] = 'emails separated by comma';
$lang['INSERTEMAIL'] = 'Insert email';
$lang['SUBMIT'] = 'Submit';
$lang['LIST'] = 'List';
$lang['SEARCHRESULT'] = 'Search Result';
$lang['EXPORT'] = 'Export';
$lang['NOINVESTIGATORSCREATED'] = 'No investigators created';
$lang['SAVE'] = 'Save';
$lang['LOGIN'] = 'LOGIN';
$lang['CLEAR'] = 'CLEAR';
$lang['USERNAME'] = 'Username';
$lang['NORESULTS'] = 'No results';
$lang['EMAILNOTFOUND'] = 'Email not found on database';
$lang['LOGINERROR'] = 'Wrong username or password.';
$lang['INVITESSEND'] = 'Invite(s) successfully sent.';
$lang['ERRORMETADATA'] = 'Must define metadata.';
$lang['ALLFIELDSOBLIGATORY'] = 'All fields are obligatory.';
$lang['NOPERMISSIONS'] = 'You have no permissions to use this service.';
$lang['COULDNTLOGIN'] = 'Could not log in.';
$lang['FILEISNOTANIMAGELOGO'] = 'File is not an image.';
$lang['FILEISTOOLARGELOGO'] = 'Image is too large.';
$lang['FILEWASNOTUPLOADEDLOGO'] = 'Image was not uploaded.';
$lang['FILEISNOTANIMAGEVOUCHER'] = 'Voucher file is not an image.';
$lang['FILEISTOOLARGEVOUCHER'] = 'Voucher is too large.';
$lang['FILEWASNOTUPLOADEDVOUCHER'] = 'Voucher was not uploaded.';
$lang['USERNAMEERROR'] = 'Please, insert your username or email.';
$lang['PASSWORDERROR'] = 'Please, insert your password.';
$lang['NUMBERERROR'] = 'The phone number may contain only digits.';
$lang['EMAILERROR'] = 'Invalid email.';
$lang['YOUHAVEANINVITE'] = 'You have an invite.';
$lang['NODATA'] = 'No data returned.';
$lang['INVESTCONFIRM'] = 'Are you sure you want to block investigator?';
$lang['OK'] = 'Ok';
$lang['CANCEL'] = 'Cancel';
$lang['CHALLENGECONFIRM'] = 'Are you sure you want to delete the challenge?';
$lang['UPDATECONFIRM'] = 'Are you sure you want to update investigator data?';
$lang['CHALLNAMEPT'] = 'You must set the title of the challenge in Portuguese.';
$lang['CHALLDESCPT'] = 'You must set the description of the challenge in Portuguese.';
$lang['CHALLNAMEENG'] = 'You must set the title of the challenge in English.';
$lang['CHALLDESCENG'] = 'You must set the description of the challenge in English.';
$lang['CHALLIMG'] = 'You must insert the logo of the challenge.';
$lang['CHALLVOUCHER'] = 'You must insert the voucher of the challenge.';
$lang['CHALLDATAINI'] = 'You must insert the begining date of the challenge.';
$lang['CHALLDATAFIM'] = 'You must insert the ending date of the challenge.';
$lang['CHALLMEDIA'] = 'You must choose at least one of the media types.';
$lang['CHALLMETATXT'] = 'You must choose at least one count for the text.';
$lang['CHALLMETAIMG'] = 'You must choose at least one count for the image.';
$lang['CHALLMETAVID'] = 'You must choose at least one count for the video.';
$lang['CHALLMETAAUD'] = 'You must choose at least one count for the audio.';
$lang['CHALLWORD'] = 'You must choose a word to count.';
$lang['CHALLTAGS'] = 'You must choose tags separate with commas.';
$lang['REJECTED'] = 'Number of rejected emails: ';
$lang['CHALLTAGSINFO'] = '(Separate with commas)';




// Menu
 
$lang['MENU_LOGOUT'] = 'Logout';

?>