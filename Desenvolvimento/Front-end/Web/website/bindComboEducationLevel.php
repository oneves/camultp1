<div class="text"><?php echo $lang['EDUCATION_LEVEL']; ?></div>
<select multiple id = "cmbEducationLevel" name = "cmbEducationLevel[]" onchange="">
	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT EduL_PK_IdEducationLevel, EduL_DescEducationLevel, EduL_DescEducationLevel_ENG FROM TEducationLevel');
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					?>
					<option value = "<?php echo $row["EduL_PK_IdEducationLevel"]; ?>" 
									id="<?php echo $row["EduL_PK_IdEducationLevel"]; ?>" title = "<?php 
																									if ($_SESSION["language"] == 'ENG')
																										echo $row["EduL_DescEducationLevel_ENG"]; 
																									else
																										echo $row["EduL_DescEducationLevel"];?>">
									<?php 
									if ($_SESSION["language"] == 'ENG')
										echo $row["EduL_DescEducationLevel_ENG"]; 
									else
										echo $row["EduL_DescEducationLevel"];?></option>
						
						<?php
							
						}
						
				}
				else
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
				}
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
							 
</select>