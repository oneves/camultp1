
<?php
	
	error_reporting(0);
	
	$DB_host = "localhost";
	$DB_user = "root";
	$DB_pass = "Asist2015";
	$DB_name = "camul";
	
	//echo 'DEBUG: chall'.$_GET['chall'];
	
	//echo 'cheguei aqui';
	if (isset($_GET['chall']) && $_GET['chall'] != "" && isset($_GET['lang']) && $_GET['lang'] != "")
	{
		
		$idchallenge = $_GET['chall'];

		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			
			
			$sql = 'SELECT DISTINCT CInv_UserEmail, CASE WHEN CRCf_Count > 0 THEN \'S\' ELSE \'N\' END as Sit 
			FROM TChallengeInvites 
			LEFT JOIN TUsers ON CInv_UserEmail = User_Email 
			AND User_FK_IdTypeUser = 3 
			LEFT JOIN TChallengeResults ON CRCf_XK_IdChallenge = CInv_XK_IdChallenge 
										AND CRCf_XK_IdUser = User_PK_IdUser 
										AND CRCf_Count > 0 
			WHERE CInv_XK_IdChallenge = '.$idchallenge;
			
			$stmt = $DB_con->prepare($sql);
			
			//echo $sql;
			
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
					$coluna = 1;

					echo '<table id="tbl_friends"><tr>';
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
						if ($row['Sit'] == 'N')
						$estado = '';
						else
						$estado = 'images/icons/check.png';
						
						//echo '<input type="checkbox" '.$estado.' readonly>'.$row['CInv_UserEmail'];
						
						if ($coluna == 1)
						{
							echo '<tr><td><img id="ck'.$coluna.'" src="'.$estado.'" />'.$row['CInv_UserEmail'].'</td>';
							$coluna++;
						}
						else
						{
							echo '<td><img id="ck'.$coluna.'" src="'.$estado.'" />'.$row['CInv_UserEmail'].'</td></tr>';
							$coluna = 1;
						}
					}
					
					if ($coluna == 2)
					{
						echo '<td></td></tr>';
					}
					echo '</table>';
				}
				else
				{
					$msg = 'Sem convites até ao momento.';
		
					if (isset($_GET['lang']) && $_GET['lang'] == 'ENG')
						$msg = 'No invitations so far.';
					
					echo $msg;

				}
				
				
			}
		}
	
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	
	}
	else
	{
		$msg = 'Houve um problema de comunicação. Por favor, tente novamente.';
		
		if (isset($_GET['lang']) && $_GET['lang'] == 'ENG')
			$msg = 'There was a communication problem. Please try again.';
		
		echo $msg;
	}
	
?>
