<?php
	session_start();
	setSessionData();
	require_once("PHPMailer_5.2.4/class.phpmailer.php");
	
	// functions
	function setSessionData() {
		// constants
		include('Config.php');
		
		// variables
				
		// user info
		
		if (!isset($_SESSION['userName'])) $_SESSION['userName']='';
		if (!isset($_SESSION['userEmail'])) $_SESSION['userEmail']='';
		if (!isset($_SESSION['userUserName'])) $_SESSION['userUserName']='';
		if (!isset($_SESSION['userIdUser'])) $_SESSION['userIdUser']='';
		if (!isset($_SESSION['userType'])) $_SESSION['userType']='';
	}
	
	function login($login, $pwd) {
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT User_Name, User_Email, User_UserName, User_PK_IdUser, User_FK_IdTypeUser 
			FROM TUsers WHERE (User_Email =:login OR User_UserName =:login) 
			AND User_Password =:pass AND User_DeletedOn IS NULL');
			
			$stmt->execute(array(':login'=>$login, ':pass'=>$pwd));
			
			$utilizador=$stmt->fetch(PDO::FETCH_ASSOC);
			
			if($stmt->rowCount() > 0)
			{
				
				$_SESSION['userName'] = $utilizador['User_Name'];
				$_SESSION['userEmail'] = $utilizador['User_Email'];
				$_SESSION['userUserName'] = $utilizador['User_UserName'];
				$_SESSION['userIdUser'] = $utilizador['User_PK_IdUser'];
				$_SESSION['userType'] = $utilizador['User_FK_IdTypeUser'];
				
                return true;
			}
			else
			{
				$_SESSION['userName'] = '';
				$_SESSION['userEmail'] = '';
				$_SESSION['userUserName'] = '';
				$_SESSION['userIdUser'] = '';
				$_SESSION['userType'] = '';
				
				if ($_SESSION["language"] == 'ENG')
					$Message='Wrong username or password.';
				else
					$Message='O utilizador não existe ou a palavra-passe encontra-se incorreta.'; 
				echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
				
                return false;
			}
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
	}
	
	function putChallenge($chalNamePT,
	$chalNameENG,
	$descPT, 
	$descENG, 
	$tpUtil, //Guest or registed
	$URL_image, 
	$URL_voucher, 
	$arr_ageRange, 
	$arr_gender, 
	$arr_marital, 
	$country, 
	$arr_city, 
	$arr_studies, 
	$arr_childrange, 
	$arr_jobs, 
	$dtIni, 
	$dtEnd, 
	$arr_audioID, 
	$arr_audioText,
	$arr_image, 
	$arr_video, 
	$arr_textID, 
	$arr_textText,
	$tags, 
	$friends) 
	{
		
		/*echo '--------- DEBUG--------------<br/>';
			echo '$chalNamePT: '.$chalNamePT.'<br/>';
			echo '$chalNameENG: '.$chalNameENG.'<br/>';
			echo '$descPT: '.$descPT.'<br/>';
			echo '$descENG: '.$descENG.'<br/>';
			echo '$tpUtil: '.$tpUtil.'<br/>';
			echo '$URL_image: '.$URL_image.'<br/>';
			echo '$URL_voucher: '.$URL_voucher.'<br/>';
			echo '$arr_ageRange: '.print_r(array_values($arr_ageRange)).'<br/>';
			echo '$arr_gender: '.print_r(array_values($arr_gender)).'<br/>';
			echo '$arr_marital: '.print_r(array_values($arr_marital)).'<br/>';
			echo '$country: '.$country.'<br/>';
			echo '$arr_city: '.print_r(array_values($arr_city)).'<br/>';
			echo '$arr_studies: '.print_r(array_values($arr_studies)).'<br/>';
			echo '$arr_childrange: '.print_r(array_values($arr_childrange)).'<br/>';
			echo '$arr_jobs: '.print_r(array_values($arr_jobs)).'<br/>';
			echo '$dtIni: '.$dtIni.'<br/>';
			echo '$dtEnd: '.$dtEnd.'<br/>';
			echo '$arr_audioID: '.print_r(array_values($arr_audioID)).'<br/>';
			echo '$arr_audioText: '.print_r(array_values($arr_audioText)).'<br/>';
			echo '$arr_image: '.print_r(array_values($arr_image)).'<br/>';
			echo '$arr_video: '.print_r(array_values($arr_video)).'<br/>';
			echo '$arr_textID: '.print_r(array_values($arr_textID)).'<br/>';
			echo '$arr_textText: '.print_r(array_values($arr_textText)).'<br/>';
			echo '$tags: '.$tags.'<br/>';
		echo '$friends: '.$friends.'<br/>';*/
		
		$_SESSION["CHALNamePT"] = $chalNamePT;
		$_SESSION["CHALNameENG"] = $chalNameENG;
		$_SESSION["CHALDescPT"] = $descPT;
		$_SESSION["CHALDescENG"] = $descENG;
		//$_SESSION["CHALURL_image"] = $URL_image;
		//$_SESSION["CHALURL_voucher"] = $URL_voucher;
		$_SESSION["CHALdtIni"] = $dtIni;
		$_SESSION["CHALdtEnd"] = $dtEnd;
		$_SESSION["CHALtags"] = $tags;
		$_SESSION["CHALfriends"] = $friends;
		
		$_SESSION["LAST_CHALNamePT"] = $chalNamePT;
		$_SESSION["LAST_CHALNameENG"] = $chalNameENG;
		$_SESSION["LAST_CHALDescPT"] = $descPT;
		$_SESSION["LAST_CHALDescENG"] = $descENG;
		//$_SESSION["CHALURL_image"] = $URL_image;
		//$_SESSION["CHALURL_voucher"] = $URL_voucher;
		$_SESSION["LAST_CHALdtIni"] = $dtIni;
		$_SESSION["LAST_CHALdtEnd"] = $dtEnd;
		$_SESSION["LAST_CHALtags"] = $tags;
		$_SESSION["LAST_CHALfriends"] = $friends;
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		if ($tpUtil == "1")
		$allowGuests = 1;
		else 
		$allowGuests = 0;
		
		$success = 0;
		
		if (isset($_SESSION['userIdUser']))
		{
			try
			{
				$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
				$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				
				// Vamos criar uma transação pois ou grava tudo ou nada
				$DB_con->beginTransaction();
				
				// Inserir o challenge e obter o ID
				
				$sql = "INSERT INTO TChallenge (Chal_Title, Chal_Title_ENG, Chal_Description, Chal_Description_ENG, Chal_BeginDate, 
				Chal_EndDate, Chal_FK_IdUserInvestigator, Chal_VoucherURL, Chal_LogoURL, 
				Chal_InsertedOn, Chal_AllowGuests, Chal_Tags)
				VALUES ('".$chalNamePT."','".$chalNameENG."','".$descPT."','".$descENG."','".$dtIni."',
				'".$dtEnd."',".$_SESSION['userIdUser'].",'".$URL_voucher."','".$URL_image."','".date('Y-m-d H:i:s')."',
				".$allowGuests.",'".$tags."')";
				//echo $sql;
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					$insert_id = $DB_con->lastInsertId();
					
					if ($insert_id != "")
					{
						$_SESSION["idChallenge"] = $insert_id;
						
						echo '<script type="text/javascript">
								$(\'#hdfieldIdChallenge\').val('.$_SESSION["idChallenge"].');
								</script>';
				
						//Inserir o público alvo
						//$arr_ageRange - 1
						foreach($arr_ageRange as $item)
						{
							if(putChallengeTarget($insert_id, 1 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Target 1 - ".$success."<br/>";
						}
						
						//$arr_gender - 4
						foreach($arr_gender as $item)
						{
							if(putChallengeTarget($insert_id, 4 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							//echo "Target 4 - ".$success."<br/>";
						}
						
						//$arr_marital - 5
						foreach($arr_marital as $item)
						{
							if(putChallengeTarget($insert_id, 5 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Target 5 - ".$success."<br/>";
						}
						
						//$country - 6
						if(putChallengeTarget($insert_id, 6 , $country , $DB_con))
						$success = 1;
						else 
						$success = 0;
						
						//echo "Target 6 - ".$success."<br/>";
						
						
						//$arr_city - 7
						foreach($arr_city as $item)
						{
							if(putChallengeTarget($insert_id, 7 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							//echo "Target 7 - ".$success."<br/>";
						}
						
						//$arr_studies - 3
						foreach($arr_studies as $item)
						{
							if(putChallengeTarget($insert_id, 3 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Target 3 - ".$success."<br/>";
						}
						
						//$arr_childrange - 9
						foreach($arr_childrange as $item)
						{
							if(putChallengeTarget($insert_id, 9 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Target 9 - ".$success."<br/>";
						}
						
						//$arr_jobs - 2
						foreach($arr_jobs as $item)
						{
							if(putChallengeTarget($insert_id, 2 , $item , $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Target 2 - ".$success."<br/>";
						}
						
						// Inserir os Metadados
						//$arr_audioID //$arr_audioText - 4
						if (count($arr_audioID) == count($arr_audioText))
						{
							for ($i = 0; $i < count($arr_audioID); $i++)
							{
								if(putChallengeResultsConfig($insert_id, 4, $arr_audioID[$i], $arr_audioText[$i], $DB_con))
								$success = 1;
								else 
								$success = 0;
								
								//echo "Meta 4 - ".$success."<br/>";
							}
						}
						else $success = 0;
						//echo "Meta 4 - ".$success."<br/>";
						
						//$arr_image - 2
						for ($i = 0; $i < count($arr_image); $i++)
						{
							if(putChallengeResultsConfig($insert_id, 2, $arr_image[$i], null, $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Meta 2 - ".$success."<br/>";
						}
						//$arr_video - 3
						for ($i = 0; $i < count($arr_video); $i++)
						{
							if(putChallengeResultsConfig($insert_id, 3, $arr_video[$i], null, $DB_con))
							$success = 1;
							else 
							$success = 0;
							
							//echo "Meta 3 - ".$success."<br/>";
						}
						
						//$arr_textID //$arr_textText - 1
						if (count($arr_textID) == count($arr_textText))
						{
							for ($i = 0; $i < count($arr_textID); $i++)
							{
								if(putChallengeResultsConfig($insert_id, 1, $arr_textID[$i], $arr_textText[$i], $DB_con))
								$success = 1;
								else 
								$success = 0;
							}
							
							
						}
						else $success = 0;
						
						//echo "Meta 1 - ".$success."<br/>";
						
						
						//echo 'Cheguei até aqui com estado: '.$success;
						
						if ($success == 1)
						{
							$DB_con->commit();
							$_SESSION["PutChallengeSuccess"] = 1;
				
							// Fazer os convites por e-mail
							//$friends
							if (isset($friends) && $friends != "")
							sendEmail($insert_id ,$friends);
							
							if ($_SESSION["language"] == 'ENG')
								$Message='Inserted with success.';
							else
								$Message='Gravado com sucesso.'; 
							//echo "<script>document.getElementById('challengeOK').click();</script>";
							echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
							
							$_SESSION["LAST_CHALNamePT"] = $_SESSION["CHALNamePT"];
							$_SESSION["LAST_CHALNameENG"] = $_SESSION["CHALNameENG"];
							$_SESSION["LAST_CHALDescPT"] = $_SESSION["CHALDescPT"];
							$_SESSION["LAST_CHALDescENG"] = $_SESSION["CHALDescENG"];
							//$_SESSION["CHALURL_image"] = $URL_image;
							//$_SESSION["CHALURL_voucher"] = $URL_voucher;
							$_SESSION["LAST_CHALdtIni"] = $_SESSION["CHALdtIni"];
							$_SESSION["LAST_CHALdtEnd"] = $_SESSION["CHALdtEnd"];
							$_SESSION["LAST_CHALtags"] = $_SESSION["CHALtags"];
							$_SESSION["LAST_CHALfriends"] = $_SESSION["CHALfriends"];
		
							unset($_SESSION["CHALNamePT"]);
							unset($_SESSION["CHALNameENG"]);
							unset($_SESSION["CHALDescPT"]);
							unset($_SESSION["CHALDescENG"]);
							//unset($_SESSION["CHALURL_image"]);
							//unset($_SESSION["CHALURL_voucher"]);
							unset($_SESSION["CHALdtIni"]);
							unset($_SESSION["CHALdtEnd"]);
							unset($_SESSION["CHALtags"]);
							unset($_SESSION["CHALfriends"]);
		
						}
						else 
						{
							$DB_con->rollBack();
							$_SESSION["PutChallengeSuccess"] = 1;
							
							if ($_SESSION["language"] == 'ENG')
								$Message='Could not insert. Please try again.';
							else
								$Message='Não foi possível gravar. Por favor tente novamente.'; 
							echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
						}
						
						
					}
					else
					{
						$DB_con->rollBack();
						echo $lang['CHALLENGEINSUCCESS'];
					}
					
					
				}
				else
				{
					$DB_con->rollBack();
					
				}
				
			}
			
			catch(PDOException $e)
			{
				$DB_con->rollBack();
				echo $e->getMessage();
			}
			
		}
		
		
	}
	
	function putChallengeTarget($idChallenge, $idSectorType, $idSector, $DB_con)
	{
		try
		{
			
			$sql = "INSERT INTO TChallengeTargetConfig (CCnf_FK_IdChallenge, CCnf_FK_IdSectorType, CCnf_IdSector) 
			VALUES (".$idChallenge.",".$idSectorType.",".$idSector.")";
			
			$stmt = $DB_con->prepare($sql);
			
			$stmt->execute();
			
			if($stmt->rowCount() > 0)
			RETURN TRUE;
			else 
			RETURN FALSE;
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
			RETURN FALSE;
		}
		
	}
	
	function putChallengeResultsConfig($idChallenge, $idMediaType, $idMediaMetaData, $Values, $DB_con)
	{
		try
		{
			$sql = "INSERT INTO TChallengeResultsConfig (CRCf_XK_IdChallenge, CRCf_XK_IdMediaType, CRCf_XK_IdMediaMetaData, CRCf_Values) 
			VALUES (".$idChallenge.",".$idMediaType.",".$idMediaMetaData.",'".$Values."')";
			
			$stmt = $DB_con->prepare($sql);
			
			$stmt->execute();
			
			if($stmt->rowCount() > 0)
			RETURN TRUE;
			else 
			RETURN FALSE;
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
			RETURN FALSE;
		}
		
	}
	
	function logout() {
		
		session_destroy();
        unset($_SESSION['userName']);
		unset($_SESSION['userEmail']);
		unset($_SESSION['userUserName']);
		unset($_SESSION['userIdUser']);
		unset($_SESSION['userType']);
		unset($_SESSION['task']);
		
		$vars = array_keys(get_defined_vars());
		for ($i = 0; $i < sizeOf($vars); $i++) {
			unset($$vars[$i]);
		}
		unset($vars,$i);
		unset($Message);
		
        return true;
		
	}
	
	function generateRandomString($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function sendEmail($idchallenge, $emailsList) {
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		$emailsList = str_replace(' ',',',$emailsList);
		$emailsList = str_replace(';',',',$emailsList);
		
		$arr_emailsList = explode(',', $emailsList);
		//print_r($arr_emailsList);
		
		//$link = Config::urlInvitation.$idchallenge;
		
		$linkPT = Config::urlInvitation.$idchallenge.'&lang=PT';
		$linkENG = Config::urlInvitation.$idchallenge.'&lang=ENG';
		
		//$body = 'You have an invite! '.$link ;
		$body = 'Recebeu um convite! '.$linkPT.' (em Português) / You have an invite! '.$linkENG.' (in English)' ;
		
		foreach($arr_emailsList as $e)
		{
			try
			{
			if ($e!="")
			{
				if (!filter_var($e, FILTER_VALIDATE_EMAIL)) {
				  $nrRecusedEmails++; 
				}
				else
				{
					//echo $e;
					$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
					$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					
					$sql = 'INSERT INTO TChallengeInvites (CInv_XK_IdChallenge, 
					CInv_UserEmail, 
					CInv_InvitationDate, 
					CInv_AllowFeedback)
					VALUES ('.$idchallenge.', \''.$e.'\',NOW(),NULL)';
					
					$stmt = $DB_con->prepare($sql);
					
					//echo $sql;
					
					
					if ($stmt->execute())
					{
						
						if($stmt->rowCount() > 0)
						{
							
							//echo 'Debug: Sucesso ao gravar;';
							
							//$body = $lang['YOUHAVEANINVITE'].' -> '.$link ;
							$body = 'Recebeu um convite! '.$linkPT.' (em Português) / You have an invite! '.$linkENG.' (in English)' ;
							
							global $error;
							$mail = new PHPMailer();
							$mail->IsSMTP();		// Ativar SMTP
							$mail->SMTPDebug = 0;		// Debugar: 1 = erros e mensagens, 2 = mensagens apenas
							$mail->SMTPAuth = true;		// Autenticação ativada
							$mail->SMTPSecure = 'tls';	// SSL REQUERIDO pelo GMail
							$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
							$mail->Port = 587;  		// A porta 587 deverá estar aberta em seu servidor
							$mail->Username = 'camul.isep.2015@gmail.com';
							$mail->Password = 'CAMUL20152016';
							$mail->SetFrom('camul.isep.2015@gmail.com');
							$mail->Subject = 'Mobile Diary Invite';
							$mail->Body = $body;
							$mail->AddAddress('camul.isep.2015@gmail.com');

							$mail->AddBCC($e);
							if(!$mail->Send()) 
							{
								$error = 'Mail error: '.$mail->ErrorInfo; 
								//echo $error;
							} 
							else {
								

								$error = 'Mensagem enviada!';
								//echo $error;
							}
						}
						else
						{
							//echo 'Debug: Insucesso ao gravar;';
						}
					}
				}
				
				if ($nrRecusedEmails > 0)
				{
					if ($_SESSION["language"] == 'ENG')
						$Message='Number of rejected emails: '.$nrRecusedEmails;
							else
						$Message='Número de e-mails rejeitados: '.$nrRecusedEmails; 
					
				echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
				}
			}
			}
			catch(PDOException $e)
			{
				echo $e->getMessage();
			}
		}
	
	}
	
	/*function sendEmail($idchallenge, $emailsList) {
		
		$arr_emailsList = explode(',', $emailsList);
		//print_r($arr_emailsList);
		
		$link = Config::urlInvitation.$idchallenge;
		
		$body = 'You have an invite! '.$link ;
		
		global $error;
		$mail = new PHPMailer();
		$mail->IsSMTP();		// Ativar SMTP
		$mail->SMTPDebug = 0;		// Debugar: 1 = erros e mensagens, 2 = mensagens apenas
		$mail->SMTPAuth = true;		// Autenticação ativada
		$mail->SMTPSecure = 'tls';	// SSL REQUERIDO pelo GMail
		$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
		$mail->Port = 587;  		// A porta 587 deverá estar aberta em seu servidor
		$mail->Username = 'camul.isep.2015@gmail.com';
		$mail->Password = 'CAMUL20152016';
		$mail->SetFrom('camul.isep.2015@gmail.com');
		$mail->Subject = 'Mobile Diary Invite';
		$mail->Body = $body;
		$mail->AddAddress('camul.isep.2015@gmail.com');
		foreach($arr_emailsList as $e)
		$mail->AddBCC($e);
		if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo; 
			
			//echo $error;
			return false;
			} else {
			$error = 'Mensagem enviada!';
			
			//echo $error;
			
			return true;
		}	
		
	}*/
	
	function registerInvest($name, $email, $pass, $phone, $company, $country, $city)
	{
		$_SESSION["AdminInputName"] = $name;
		$_SESSION["AdminInputEmail"] = $email;
		$_SESSION["AdminInputPhone"] = $phone;
		$_SESSION["AdminInputCompany"] = $company;
		$_SESSION["AdminInputCountry"] = $country;
		$_SESSION["AdminInputCity"] = $city;
		$_SESSION["AdminInputPass"] = $pass;
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			if (!emailExists($email, $DB_con))
			{
				
				// Inserir o challenge e obter o ID
				
				$sql = "INSERT INTO TUsers (User_FK_IdTypeUser, User_Name, User_Email, User_UserName, User_Password, 
				User_FK_IdCountry, User_FK_IdCity,User_InsertedOn, User_Phone, User_Company) 
				VALUES (2, '".$name."', '".$email."', '".$email."', '".$pass."',".$country.",".$city.",
				'".date('Y-m-d H:i:s')."',".$phone.",'".$company."')";
				
				//echo $sql;
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					if ($_SESSION["language"] == 'ENG')
								$Message='Inserted with success.';
							else
								$Message='Gravado com sucesso.'; 
					echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
					
					unset($_SESSION["AdminInputName"]);
					unset($_SESSION["AdminInputEmail"]);
					unset($_SESSION["AdminInputPhone"]);
					unset($_SESSION["AdminInputCompany"]);
					unset($_SESSION["AdminInputCountry"]);
					unset($_SESSION["AdminInputCity"]);
					unset($_SESSION["AdminInputPass"]);
				}
				else
				{
					if ($_SESSION["language"] == 'ENG')
								$Message='Could not insert. Please try again.';
							else
								$Message='Não foi possível gravar. Por favor tente novamente.'; 
					echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
					
				}
			}
			else
			{
				if ($_SESSION["language"] == 'ENG')
								$Message='Email already exists in the database.';
							else
								$Message='E-mail já existente na base de dados.'; 
				echo "<script type=\"text/javascript\">generate('warning', \"".$Message."\");</script>";
			}
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
		
		
	}
	
	function editInvestigator($idInvestigator)
	{
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			

				
				// Inserir o challenge e obter o ID
				
				$sql = 'SELECT User_Name, User_Email, User_UserName, User_Password, 
				User_FK_IdCountry, User_FK_IdCity, User_Phone, User_Company
				FROM TUsers
				WHERE User_PK_IdUser = '.$idInvestigator.'
				AND User_FK_IdTypeUser = 2';
				
				
				//echo $sql;
				
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					$row = $stmt->fetch(PDO::FETCH_ASSOC);
					
					//echo 'DEBUG: Contei: '.$stmt->rowCount();
					$_SESSION["InvestID"] = $idInvestigator;
					$_SESSION["InvestNome"] = $row["User_Name"];
					$_SESSION["InvestEmail"] = $row["User_Email"];
					$_SESSION["InvestTelefone"] = $row["User_Phone"];
					$_SESSION["InvestEmpresa"] = $row["User_Company"];
					$_SESSION["InvestPais"] = $row["User_FK_IdCountry"];
					$_SESSION["InvestCidade"] = $row["User_FK_IdCity"];
				}
				else
				{
				if ($_SESSION["language"] == 'ENG')
								$Message='Unable to set the user details.';
							else
								$Message='Não foi possível preencher os detalhes do utilizador.'; 
					echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
					
				}
			
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
		
		
	}
	
	function updateInvest($idInvest, $name, $email, $pass, $phone, $company, $country, $city)
	{
		
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			

				
				// Inserir o challenge e obter o ID
				
				$sql = "UPDATE TUsers
						SET User_Name = '".$name."',
							User_Email = '".$email."',
							User_Password = '".$pass."',
							User_FK_IdCountry = ".$country.",
							User_FK_IdCity = ".$city.",
							User_InsertedOn = '".date('Y-m-d H:i:s')."',
							User_Phone = ".$phone.",
							User_Company = '".$company."' 
						WHERE User_PK_IdUser = ".$idInvest." 
						AND User_FK_IdTypeUser = 2";
				
				
				//echo $sql;
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					$_SESSION["InvestID"] = $idInvest;
					$_SESSION["InvestNome"] = $name;
					$_SESSION["InvestEmail"] = $email;
					$_SESSION["InvestTelefone"] = $phone;
					$_SESSION["InvestEmpresa"] = $company;
					$_SESSION["InvestPais"] = $country;
					$_SESSION["InvestCidade"] = $city;
					
					if ($_SESSION["language"] == 'ENG')
								$Message='Updated with success.';
							else
								$Message='Atualizado com sucesso.'; 
					echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
				}
				else
				{
					if ($_SESSION["language"] == 'ENG')
								$Message='An error occurred while updating. Please try again.';
							else
								$Message='Ocorreu um erro ao atualizar. Por favor, tente novamente.'; 
					echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
					
				}
			
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
		
		
	}
	
	function emailExists($email, $DB_con)
	{
		try
		{
			$sql = "SELECT * FROM TUsers
			WHERE User_Email = '".$email."'
			OR User_UserName = '".$email."'";
			
			$stmt = $DB_con->prepare($sql);
			
			$stmt->execute();
			
			if($stmt->rowCount() > 0)
			RETURN TRUE;
			else 
			RETURN FALSE;
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
			RETURN FALSE;
		}
	}
	
	function blockInvestigator($idInvestigator)
	{
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			
			// Inserir o challenge e obter o ID
			
			$sql = "UPDATE TUsers 
					SET User_DeletedOn = '".date('Y-m-d H:i:s')."' 
					WHERE User_FK_IdTypeUser = 2 
					AND User_PK_IdUser = ".$idInvestigator.";";
			
			//echo $sql;
			$stmt = $DB_con->prepare($sql);
			
			$stmt->execute();
			
			if($stmt->rowCount() > 0)
			{
				if ($_SESSION["language"] == 'ENG')
								$Message='Blocked successfully.';
							else
								$Message='Bloqueado com sucesso.';
				echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
			}
			else
			{
				if ($_SESSION["language"] == 'ENG')
								$Message='Error during the blocking process. Please try again.';
							else
								$Message='Ocorreu um erro ao bloquear. Por favor, tente novamente.';
				echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
				
			}
			
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
		
		
		
	}
	
function deleteChallenge ($idChallenge)
	{
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			
			// Inserir o challenge e obter o ID
			
			$sql = "UPDATE TChallenge 
					SET Chal_DeletedOn = '".date('Y-m-d H:i:s')."' 
					WHERE Chal_PK_IdChallenge = ".$idChallenge.";";
			
			//echo $sql;
			$stmt = $DB_con->prepare($sql);
			
			$stmt->execute();
			
			if($stmt->rowCount() > 0)
			{
				if ($_SESSION["language"] == 'ENG')
								$Message='Successfully deleted.';
							else
								$Message='Eliminado com sucesso.';
				echo "<script type=\"text/javascript\">generate('success', \"".$Message."\");</script>";
			}
			else
			{
				if ($_SESSION["language"] == 'ENG')
								$Message='An error occurred while deleting. Please try again.';
							else
								$Message='Ocorreu um erro ao eliminar. Por favor, tente novamente.';
				echo "<script type=\"text/javascript\">generate('error', \"".$Message."\");</script>";
				
			}
			
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
		
		
		
		
	}
	
?>			