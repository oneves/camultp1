<div class="text"><?php echo $lang['MARITAL_STATUS']; ?></div>
<select multiple id = "cmbMaritalStatus" name = "cmbMaritalStatus[]" >
	
	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT MStt_PK_IdMaritalStatus, MStt_DescMaritalSatus, MStt_DescMaritalSatus_ENG FROM TMaritalStatus');
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					?>
					<option value = "<?php echo $row["MStt_PK_IdMaritalStatus"]; ?>" 
					id="<?php echo $row["MStt_PK_IdMaritalStatus"]; ?>" title = "<?php 
							if ($_SESSION["language"] == 'ENG')
							echo $row["MStt_DescMaritalSatus_ENG"];
							else
							echo $row["MStt_DescMaritalSatus"];
						?>">
						<?php 
							if ($_SESSION["language"] == 'ENG')
							echo $row["MStt_DescMaritalSatus_ENG"];
							else
							echo $row["MStt_DescMaritalSatus"];
						?></option>
						
						<?php
							
						}
						
				}
				else
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
				}
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
