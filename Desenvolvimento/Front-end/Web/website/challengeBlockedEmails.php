
<?php
	
	error_reporting(0);
	
	$DB_host = "localhost";
	$DB_user = "root";
	$DB_pass = "Asist2015";
	$DB_name = "camul";
	
	//echo 'DEBUG: chall'.$_GET['chall'];
	
	//echo 'cheguei aqui';
	if (isset($_GET['chall']) && $_GET['chall'] != "" && isset($_GET['lang']) && $_GET['lang'] != "")
	{
		
		$idchallenge = $_GET['chall'];

		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$sql = 'SELECT DISTINCT CInv_UserEmail 
				FROM TChallengeInvites 
				WHERE CInv_AllowFeedback = 0 
				AND CInv_XK_IdChallenge = '.$idchallenge;
			
			$stmt = $DB_con->prepare($sql);
			
			//echo $sql;
			
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
					$coluna = 1;

					echo '<table id="tbl_friends"><tr>';
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					
						//echo '<input type="checkbox" '.$estado.' readonly>'.$row['CInv_UserEmail'];
						
						if ($coluna == 1)
						{
							echo '<tr><td>'.$row['CInv_UserEmail'].'</td>';
							$coluna++;
						}
						else
						{
							echo '<td>'.$row['CInv_UserEmail'].'</td></tr>';
							$coluna = 1;
						}
					}
					
					if ($coluna == 2)
					{
						echo '<td></td></tr>';
					}
					echo '</table>';
				}
				else
				{
			
					$msg = 'Sem bloqueios até ao momento.';
		
					if (isset($_GET['lang']) && $_GET['lang'] == 'ENG')
						$msg = 'No blocks so far.';
					
					echo $msg;

				}
				
				
			}
		}
	
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	
	}
	else
	{
		$msg = 'Houve um problema de comunicação. Por favor, tente novamente.';
		
		if (isset($_GET['lang']) && $_GET['lang'] == 'ENG')
			$msg = 'There was a communication problem. Please try again.';
		
		echo $msg;
	}
	
?>
