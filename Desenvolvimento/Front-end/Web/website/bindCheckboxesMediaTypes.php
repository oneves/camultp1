
<?php
	$DB_host = Config::sgbd_server_name;
	$DB_user = Config::public_login;
	$DB_pass = Config::public_pwd;
	$DB_name = Config::db_name;
	
	try
	{
		$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
		$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		$stmt = $DB_con->prepare('SELECT TMed_PK_IdMediaType, TMed_DescMediaType, TMed_DescMediaType_ENG  FROM TTMedia');
		if ($stmt->execute())
		{
			
			if($stmt->rowCount() > 0)
			{
			?>
			<div id ="chkMedias">
			<table id="tbl_medias">
				<tr>
					<?php
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC))
						{
						?>
						<td>
							<div class="text">
								<?php
									if ($_SESSION["language"] == 'ENG')
									echo $row["TMed_DescMediaType_ENG"]; 
									else
								echo $row["TMed_DescMediaType"];?>
							<input type="checkbox" class="check" id="chk_<?php echo $row["TMed_PK_IdMediaType"]; ?>" onchange = "javascript:openDetail('chk_<?php echo $row["TMed_PK_IdMediaType"]; ?>');"></div>
							<div class="one">*</div>
						</td>
						
						<?php
						}?>
				</tr>
			</table>
			</div>
			<?php
			}
		}
	}
	
	catch(PDOException $e)
	{
		echo $e->getMessage();
	}
	
?>
