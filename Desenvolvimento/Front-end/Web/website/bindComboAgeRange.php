<div class="text"><?php echo $lang['AGE_RANGE']; ?></div>
<select multiple id = "cmbAgeRange" name="cmbAgeRange[]" onchange="">
	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT ARan_PK_IdAgeRange, ARan_DescAgeRange FROM TAgeRange');
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_F']; ?>)</option>
				<?php
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					?>
					<option value = "<?php echo $row["ARan_PK_IdAgeRange"]; ?>" 
							id="<?php echo $row["ARan_PK_IdAgeRange"]; ?>" title = "<?php echo $row["ARan_DescAgeRange"]; ?>">
								<?php echo $row["ARan_DescAgeRange"]; ?></option>
						
						<?php
							
						}
						
				}
				else
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_F']; ?>)</option>
				<?php
					
				}
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
</select>
