<div class="text"><?php echo $lang['GENDER']; ?></div>
<select multiple id = "cmbGender" name = "cmbGender[]" onchange="" >
	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT Gend_PK_IdGender, Gend_DescGender, Gend_DescGender_ENG FROM TGender');
			if ($stmt->execute())
			{
			
			if($stmt->rowCount() > 0)
			{
			?>
			<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
			<?php
				
				while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					
				?>
				<option value = "<?php echo $row["Gend_PK_IdGender"].$stmt->rowCount();?>2" 
				id="<?php echo $row["Gend_PK_IdGender"]; ?>" title = "<?php if ($_SESSION["language"] == 'ENG') 
						echo $row["Gend_DescGender_ENG"]; 
						else 
					echo $row["Gend_DescGender"];?>">
					<?php if ($_SESSION["language"] == 'ENG') 
						echo $row["Gend_DescGender_ENG"]; 
						else 
					echo $row["Gend_DescGender"];?>
				</option>
				
				<?php
					
				}
				
			}
			else
			{
			?>
			<option value="0" selected>(<?php echo $lang['ALL_M']; ?>3)</option>
			<?php
				
			}
			}
			
			
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
	
</select>