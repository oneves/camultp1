<div class="wrap">
<div class="lang" >
	<a href ="javascript:setLanguage('PT')"><?php if(isset($_SESSION['language']) && $_SESSION['language'] == 'PT') echo '<strong>PT</strong>'; else echo 'PT'; ?></a> | 
	<a href ="javascript:setLanguage('ENG')"><?php if(isset($_SESSION['language']) && $_SESSION['language'] == 'ENG') echo '<strong>ENG</strong>'; else echo 'ENG'; ?></a></div>
	<img id="logo" src="images/investigator.png" />
        <img id="logout" src="images/logout.png" onclick = "document.frmParameters.task.value='logout'; document.frmParameters.submit();" />
    </div>
    <div>
<section class="wrapper">

            <ul class="tabs">
                <li><a href="#tab1"><?php echo $lang['NEWCHALLENGE']; ?></a></li>
                <li><a href="#tab2"><?php echo $lang['LISTACTIVE']; ?></a></li>
            </ul>
            <div class="clr"></div>
            <section class="block">
                <?php include "investTab1.php";?>
                <?php include "investTab2.php";?>
				<?php include "investTab3.php";?>
            </section>
</section><!-- content -->
</div>
<script type="text/javascript">
$("#logout").click(function () {	
	localStorage['visited']="";
	localStorage['details']=0;
	localStorage['detalhes']=0;
});
</script>