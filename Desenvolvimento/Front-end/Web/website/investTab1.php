<form id="main_form" name="main_form" action = "index.php" method = "POST" enctype="multipart/form-data">
	<article id="tab1">
		<div id="challenge_tab" style="display: <?php if ($_SESSION["PutChallengeSuccess"] == 1 ) echo 'none'; else echo 'block';?>;">
			<span class="text2 bolda" id="title_pt"><?php echo $lang['CHALLENGENAME']; ?> PT</span><span><strong> | </strong></span>
			<span class="text2" id="title_eng"><?php echo $lang['CHALLENGENAME']; ?> ENG</span>
			<input type="text" name="input_title_pt" id="input_title_pt"  value="<?php if (isset($_SESSION["CHALNamePT"])) echo $_SESSION["CHALNamePT"]; ?>"   required><div class="req">*</div>
			<input type="text" name="input_title_eng" id="input_title_eng"  value="<?php if (isset($_SESSION["CHALNameENG"])) echo $_SESSION["CHALNameENG"]; ?>"   required><div class="req">*</div>	
			<div class="text"><?php echo $lang['DESCRIPTION']; ?></div>
			<textarea id="input_desc_pt" name="input_desc_pt" required ><?php if (isset($_SESSION["CHALDescPT"])) echo $_SESSION["CHALDescPT"]; ?></textarea><div class="req">*</div>
			<textarea id="input_desc_eng" name ="input_desc_eng" required><?php if (isset($_SESSION["CHALDescENG"])) echo $_SESSION["CHALDescENG"]; ?></textarea><div class="req">*</div>
			<br>
			<table id="tbl_file">
				<tr>
					<td>
						<div class="text"><?php echo $lang['IMAGE']; ?></div>
						<input id="myfiles" name="myfiles" type="file"><div class="req">*</div>
					</td>
					
					<td id="sel_rwd" style="display: visible">
						<div class="text">Voucher</div>
						<input id="myfiles2" name="myfiles2" type="file"><div class="req">*</div>
					</td>
				</tr>
			</table>
			<br>
			<table id="tbl_file3">
				<tr>
					<td>
						<div class="text"><?php echo $lang['USERS']; ?></div>
						<select id="user" name = "userType">
							<option value="1" selected><?php echo $lang['GUEST']; ?></option>
							<option value="2"><?php echo $lang['REGISTERED']; ?></option>
						</select>
					</td>
					<td>
						<div class="text"><?php echo $lang['BEGINDATE']; ?></div>
						<input id="dtIni" name="dtIni" class="time" value="<?php if (isset($_SESSION["CHALdtIni"])) echo $_SESSION["CHALdtIni"]; ?>"  readonly><div class="req">*</div>
					</td>
					
					<td id="sel_rwd" style="display: visible">
						<div class="text"><?php echo $lang['ENDDATE']; ?></div>
						<input id="dtFim" name="dtFim" class="time" value="<?php if (isset($_SESSION["CHALdtEnd"])) echo $_SESSION["CHALdtEnd"]; ?>"  readonly><div class="req">*</div>
					</td>
				</tr>
			</table>
			<br>
			<div id="registred" style="display: none">
				<table>
					<tr>
						<td>
							<?php include("bindComboAgeRange.php");?>
						</td>
						<td>
							<?php include("bindComboGender.php");?>
						</td>
						<td>
							<?php include("bindComboMaritalStatus.php");?>
						</td>
						<td>
							<?php include("bindComboChildrenNumber.php");?>
						</td>
						
					</tr>
					<tr>
						<td colspan="2">
							<?php include("bindComboEducationLevel.php");?>
						</td>
						<td colspan="2">
							<?php include("bindComboJobSector.php");?>
						</td>
					</tr>
					<tr>
						<td>
							<?php include("bindComboCountry.php");?>
						</td>
						<td>
							<div class="text"><?php echo $lang['CITY']; ?></div>
								<select multiple id = "cmbcity" name = "cmbcity[]" onchange="" >
									<option value="0" selected>(<?php echo $lang['ALL_F']; ?>)</option>
							</select>
						</td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
			<br>
			<div id="media">
				<?php include("bindCheckboxesMediaTypes.php");?>
				
				<div id="sel_txt" class="media" style="display: none">
					<?php
						$DB_host = Config::sgbd_server_name;
						$DB_user = Config::public_login;
						$DB_pass = Config::public_pwd;
						$DB_name = Config::db_name;
						
						try
						{
							$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
							$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							
							$stmt = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
							TMed_DescMediaType,
							TMed_DescMediaType_ENG,
							TRMD_XK_IdMediaMetaData, 
							TMDt_DescMediaMetaData,
							TMDt_DescMediaMetaData_ENG
							FROM TRMediaMetaData
							INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
							INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
							WHERE TRMD_XK_IdMediaType = 1');
							
							
							
														
							if ($stmt->execute())
							{
								
								if($stmt->rowCount() > 0)
								{
									$stmt2 = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
									TMed_DescMediaType,
									TMed_DescMediaType_ENG,
									TRMD_XK_IdMediaMetaData, 
									TMDt_DescMediaMetaData,
									TMDt_DescMediaMetaData_ENG
									FROM TRMediaMetaData
									INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
									INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
									WHERE TRMD_XK_IdMediaType = 1');
									
									if ($stmt2->execute())
									{
									$firstrow = $stmt2->fetch(PDO::FETCH_ASSOC);
								?>
								<div class="text linha">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $firstrow["TMed_DescMediaType_ENG"];
										else 
									echo $firstrow["TMed_DescMediaType"];	?> 
								</div>
								<?php 
									}
									while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
									{
									?>
									<input type="checkbox" class="check" 
											id="conta_txt" value="<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>"
											name="chkTexts[]" >
									<?php if ($_SESSION["language"] == 'ENG')
											echo $row["TMDt_DescMediaMetaData_ENG"];
										else 
											echo $row["TMDt_DescMediaMetaData"];?> 
									<input id="<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>" class="word"
											name="txtTexts[]"><br/>
									
									<?php
									}
								}
									
							}
								
							
						}
						
						catch(PDOException $e)
						{
							echo $e->getMessage();
						}
						
					
			?>
		</div>
		<div id="sel_img" class="media" style="display: none">
			
			<?php
						$DB_host = Config::sgbd_server_name;
						$DB_user = Config::public_login;
						$DB_pass = Config::public_pwd;
						$DB_name = Config::db_name;
						
						try
						{
							$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
							$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							
							$stmt = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
							TMed_DescMediaType,
							TMed_DescMediaType_ENG,
							TRMD_XK_IdMediaMetaData, 
							TMDt_DescMediaMetaData,
							TMDt_DescMediaMetaData_ENG
							FROM TRMediaMetaData
							INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
							INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
							WHERE TRMD_XK_IdMediaType = 2');
							
							
							
														
							if ($stmt->execute())
							{
								
								if($stmt->rowCount() > 0)
								{
									$stmt2 = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
									TMed_DescMediaType,
									TMed_DescMediaType_ENG,
									TRMD_XK_IdMediaMetaData, 
									TMDt_DescMediaMetaData,
									TMDt_DescMediaMetaData_ENG
									FROM TRMediaMetaData
									INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
									INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
									WHERE TRMD_XK_IdMediaType = 2');
									
									if ($stmt2->execute())
									{
									$firstrow = $stmt2->fetch(PDO::FETCH_ASSOC);
								?>
								<div class="text linha">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $firstrow["TMed_DescMediaType_ENG"];
										else 
									echo $firstrow["TMed_DescMediaType"];	?> 
								</div>
								<?php 
									}
									while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
									{
									?>
									<input type="checkbox" class="check" id="conta_img" name="conta_img[]" value = "<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $row["TMDt_DescMediaMetaData_ENG"];
										else 
									echo $row["TMDt_DescMediaMetaData"];?> 
									<br/>
									
									<?php
									}
								}
									
							}
								
							
						}
						
						catch(PDOException $e)
						{
							echo $e->getMessage();
						}
						
					
			?>
			</div>
			<div id="sel_video" class="media" style="display: none">
			
			<?php
						$DB_host = Config::sgbd_server_name;
						$DB_user = Config::public_login;
						$DB_pass = Config::public_pwd;
						$DB_name = Config::db_name;
						
						try
						{
							$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
							$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							
							$stmt = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
							TMed_DescMediaType,
							TMed_DescMediaType_ENG,
							TRMD_XK_IdMediaMetaData, 
							TMDt_DescMediaMetaData,
							TMDt_DescMediaMetaData_ENG
							FROM TRMediaMetaData
							INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
							INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
							WHERE TRMD_XK_IdMediaType = 3');
							
							
							
														
							if ($stmt->execute())
							{
								
								if($stmt->rowCount() > 0)
								{
									$stmt2 = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
									TMed_DescMediaType,
									TMed_DescMediaType_ENG,
									TRMD_XK_IdMediaMetaData, 
									TMDt_DescMediaMetaData,
									TMDt_DescMediaMetaData_ENG
									FROM TRMediaMetaData
									INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
									INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
									WHERE TRMD_XK_IdMediaType = 3');
									
									if ($stmt2->execute())
									{
									$firstrow = $stmt2->fetch(PDO::FETCH_ASSOC);
								?>
								<div class="text linha">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $firstrow["TMed_DescMediaType_ENG"];
										else 
									echo $firstrow["TMed_DescMediaType"];	?> 
								</div>
								<?php 
									}
									while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
									{
									?>
									<input type="checkbox" class="check" id="conta_vid" name="conta_vid[]" value = "<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $row["TMDt_DescMediaMetaData_ENG"];
										else 
									echo $row["TMDt_DescMediaMetaData"];?> 
									<br/>
									
									<?php
									}
								}
									
							}
								
							
						}
						
						catch(PDOException $e)
						{
							echo $e->getMessage();
						}
						
					
			?>
			</div>
			<div id="sel_audio" class="media" style="display: none">
			
			<?php
						$DB_host = Config::sgbd_server_name;
						$DB_user = Config::public_login;
						$DB_pass = Config::public_pwd;
						$DB_name = Config::db_name;
						
						try
						{
							$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
							$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
							
							$stmt = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
							TMed_DescMediaType,
							TMed_DescMediaType_ENG,
							TRMD_XK_IdMediaMetaData, 
							TMDt_DescMediaMetaData,
							TMDt_DescMediaMetaData_ENG
							FROM TRMediaMetaData
							INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
							INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
							WHERE TRMD_XK_IdMediaType = 4');
														
							if ($stmt->execute())
							{
								
								if($stmt->rowCount() > 0)
								{
									$stmt2 = $DB_con->prepare('SELECT DISTINCT TRMD_XK_IdMediaType,
									TMed_DescMediaType,
									TMed_DescMediaType_ENG,
									TRMD_XK_IdMediaMetaData, 
									TMDt_DescMediaMetaData,
									TMDt_DescMediaMetaData_ENG
									FROM TRMediaMetaData
									INNER JOIN TTMediaMetaData ON TRMD_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
									INNER JOIN TTMedia ON TRMD_XK_IdMediaType = TMed_PK_IdMediaType
									WHERE TRMD_XK_IdMediaType = 4');
									
									if ($stmt2->execute())
									{
									$firstrow = $stmt2->fetch(PDO::FETCH_ASSOC);
								?>
								<div class="text linha">
									<?php if ($_SESSION["language"] == 'ENG')
										echo $firstrow["TMed_DescMediaType_ENG"];
										else 
									echo $firstrow["TMed_DescMediaType"];	?> 
								</div>
								<?php 
									}
									while($row = $stmt->fetch(PDO::FETCH_ASSOC)) 
									{
									?>
									<input type="checkbox" class="check" 
											id="conta_aud" value="<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>"
											name="chkAudio[]" >
									<?php if ($_SESSION["language"] == 'ENG')
											echo $row["TMDt_DescMediaMetaData_ENG"];
										else 
											echo $row["TMDt_DescMediaMetaData"];?> 
									<input id="<?php echo $row["TRMD_XK_IdMediaMetaData"]; ?>" class="word"
											name="txtAudio[]"><br/>
									
									<?php
									}
								}
									
							}
								
							
						}
						
						catch(PDOException $e)
						{
							echo $e->getMessage();
						}
						
					
			?>
			</div>
			<table id="tbl_file2">
				<tr>
					<td>
						<div class="text">Tags <span style="font-size:8pt;"><?php echo $lang['CHALLTAGSINFO']; ?></span></div>
						<input id="tags" class="middle" name="tags" type="text"  value="<?php if (isset($_SESSION["CHALtags"])) echo $_SESSION["CHALtags"]; ?>"><div class="req">*</div>
					</td>
					
					<td id="sel_rwd" style="display: visible">
						<div class="text"><?php echo $lang['INVITEPARTICIPANTS']; ?></div>
						<input id="invite" class="middle" name="invite" type="text"  value="<?php if (isset($_SESSION["CHALfriends"])) echo $_SESSION["CHALfriends"]; ?>"><div class="req">*</div>
					</td>
				</tr>
			</table>
			<br>
			<br>
			<div id="oblig">(*) <?php echo $lang['OBLIGATORYFIELDS']; ?></div>
			<div id="one">(*) <?php echo $lang['CHOOSEONE']; ?></div>
			<input type="hidden" name="upload" value="uploadImage">
			<input type="button" id="bt_create" value="<?php echo $lang['CREATE'];?>">
			
			</div>
			<br>
		</div>		
			<div id="finish_tab" style="text-align:center;display: <?php if ($_SESSION["PutChallengeSuccess"] != 1 ) echo 'none'; else echo 'block';?>;">
				<div class="bigtext"><?php echo $lang['CHALLENGESUCCESS']; ?></div>
				<input type="button" id="bt_invite" value="<?php echo $lang['INVITEPARTICIPANTSUPPER']; ?>">
				<br>
				<input type="button" id="bt_backToNewChallenge" value="<?php echo $lang['CREATENEW']; ?>" onclick="javascript:challengeback();">
			</div>
			<div id="fail_tab" >
				<div class="bigtext">Ocorreu um problema na criação do challenge.</div>
				<input type="button" id="bt_backToNewChallenge2" value="Voltar">
			</div>
			<div id="friends_tab" style="display: none">
			<div class="header_friends"><?php echo $lang['INVITEPARTICIPANTS']; ?></div>
			<input id="friend_list" class="list" type="text">
			<input type="button" id="bt_sendInvites" value="INVITE">
			<div id="friends_tab" style="display: nones">
			
				<div class="header_friends"><?php echo $lang['PARTICIPANTSINVITER']; ?></div>
				<div id = "friendsList">
				<?php echo $lang['NOINVITES']; ?>
				</div>
				<input type="button" id="bt_back" value="BACK" >
			</div>
			</div>
	</article>
	<input type = "hidden" name = "task" value = "createChallenge"/>
</form>		

<script type="text/javascript">
$(".req").hide();
$(".one").hide();
$("#oblig").hide();
$("#one").hide();
$("#fail_tab").hide();


$("#input_desc_eng").hide();
$("#input_title_eng").hide();

$("#title_eng").click(function () {	
	$("#input_title_pt").hide();
	$("#input_title_eng").show();
	$('#title_eng').addClass('bolda');
	$('#title_pt').removeClass('bolda');
	
	$("#input_desc_pt").hide();
	$("#input_desc_eng").show();
	$('#desc_eng').addClass('bolda');
	$('#desc_pt').removeClass('bolda');
});

$("#title_pt").click(function () {	
	$("#input_title_pt").show();
	$("#input_title_eng").hide();
	$('#title_pt').addClass('bolda');
	$('#title_eng').removeClass('bolda');
	
	$("#input_desc_pt").show();
	$("#input_desc_eng").hide();
	$('#desc_pt').addClass('bolda');
	$('#desc_eng').removeClass('bolda');
});


 $("#bt_backToNewChallenge2").click(function () {
	$("#challenge_tab").show();
	$("#fail_tab").hide();
});

 $("#bt_create").click(function () {
	
	verifica_obrigatorios();
	//verifica_at_least_one();
	tryPutChallenge();
	//$("#challenge_tab").hide();
	//$("#finish_tab").show();
});



$("#bt_invite").click(function () {	
	$("#finish_tab").hide();
	$("#friends_tab").show();
	bindInvites();
});

 $("#bt_back").click(function () {	
	$("#friends_tab").hide();
	$("#challenge_tab").show();
});

$(function () {
	$("#dtIni").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0  });
});
$(function () {
	$("#dtFim").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 });
});

$('#dtFim').change(function() {
	if($(this).val() < $("#dtIni").val()) {
		$(this).val("");
	}		
});

$('#dtIni').change(function() {
	if($(this).val() > $("#dtFim").val()) {
		$(this).val("");
	}		
});

$('#tbl_friends tr td input').change(function() {
	if($(this).is(":checked")) {
		var str=$('#friend_list').val();
       $('#friend_list').val(str + $(this).parent().text().replace("					","")+";");  
	}		
});

function verifica_obrigatorios(){
	if($('#input_title_pt').val()==""){
		$("#input_title_pt").next().show();
	}
	else{
		$("#input_title_pt").next().hide();
	}
	/****/
	if($('#input_title_eng').val()==""){
		$("#input_title_eng").next().show();
	}
	else{
		$("#input_title_eng").next().hide();
	}
	/****/
	if($('#input_desc_eng').val()==""){
		$("#input_desc_eng").next().show();
	}
	else{
		$("#input_desc_eng").next().hide();
	}
	/****/
	if($('#input_desc_pt').val()==""){
		$("#input_desc_pt").next().show();
	}
	else{
		$("#input_desc_pt").next().hide();
	}
	/****/
	if($('#dtIni').val()==""){
		$("#dtIni").next().show();
	}
	else{
		$("#dtIni").next().hide();
	}
	/****/
	if($('#dtFim').val()==""){
		$("#dtFim").next().show();
	}
	else{
		$("#dtFim").next().hide();
	}
	/****/
	/*if($('#tags').val()==""){
		$("#tags").next().show();
	}
	else{
		$("#tags").next().hide();
	}
	/***
	if($('#invite').val()==""){
		$("#invite").next().show();
	}
	else{
		$("#invite").next().hide();
	}*/
	/****/
	if($('#myfiles').val()==""){
		$("#myfiles").next().show();
	}
	else{
		$("#myfiles").next().hide();
	}
	/****/
	if($('#myfiles2').val()==""){
		$("#myfiles2").next().show();
	}
	else{
		$("#myfiles2").next().hide();
	}
}

/*function verifica_at_least_one(){
	if($('#tbl_medias tr td input:checkbox:checked').length == 0){
		$(".one").next().show();
		$("#one").show();
	}
	else{
		$(".one").next().hide();
		$("#one").show();
	}
}*/

function challengeback()
		{
			$("#hdfieldChallengeSuccess").val("0");
			$("#challenge_tab").show();
			$("#finish_tab").hide();
		}
		
$(document).on('click','#bt_sendInvites',function(){
			var val = $('#friend_list').val();
			var challenge = $('#hdfieldIdChallenge').val()
			
			var language;
			if ($('#hdfieldLanguage').val() != '')
			language = $('#hdfieldLanguage').val()
			else
			language = 'PT';
		
			if (val != "")
			{
				$.ajax({
					url: 'inviteFriends.php',
					data: {list:val, chall:challenge, lang:language},
					type: 'GET',
					dataType: 'html',
					success: function(result){
						$('#friendsList').html();  
						$('#friendsList').html(result); 
						generate('success', '<?php echo $lang['INVITESSEND'];?>');
					}
				});
			}
		});	
		
	function bindInvites(){

			var challenge = $('#hdfieldIdChallenge').val()
			var language;
			if ($('#hdfieldLanguage').val() != '')
			language = $('#hdfieldLanguage').val()
			else
			language = 'PT';
		
			if (challenge != "")
			{
				$.ajax({
					url: 'getFriendsInvited.php',
					data: {chall:challenge, lang:language},
					type: 'GET',
					dataType: 'html',
					success: function(result){
						$('#friendsList').html();  
						$('#friendsList').html(result); 
					}
				});
			}
		}
		

</script>

			
