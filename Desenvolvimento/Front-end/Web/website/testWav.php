<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Mobile Diary</title>
		<link rel="stylesheet" type="text/css" href="styles.css">
		<!--JQuery-->
		<script src="import/jquery-1.12.0.min.js"></script>
		<script src="import/jquery-1.12.2.min.js"></script>
		<script src="import/jquery-1.12.2.js"></script>
		<!--Calendar-->
		<link rel="stylesheet" href="import/jquery-ui.css" />
		<script src="import/jquery-ui.js"></script>
		<!--TimePicker-->
		<link rel="stylesheet" href="import/time/jquery-ui-timepicker.css" />
		<script src="import/time/jquery-ui-timepicker.js"></script>
		<script src="import/time/date.js"></script>
		<script type="text/javascript" src="noty-2.3.8/js/noty/packaged/jquery.noty.packaged.js"></script>
	</head>
	<body>
		<div id="header1">build to serve you</div>
		<div id="header2"></div>

		<div>
			<?php

				if (isset($_POST['task']) && $_POST['task'] == 'sendWav')
				{
						/****** Imagem do challenge **************/
						$target_dir = "uploads/";
						
						$target_file = $target_dir .'_'. basename($_FILES["myfiles"]["name"]);

						$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

						echo 'O ficheiro é um '.$imageFileType;

						// Allow certain file formats
						if($imageFileType != "wav") {
							echo "Sorry, only WAV is allowed";
							$uploadOk = 0;
						}
						else
							$uploadOk = 1;
						
						// Check if file already exists
						if (file_exists($target_file)) {
							echo "Sorry, file already exists.";
							$uploadOk = 0;
						}
						
						// Check file size
						if ($_FILES["myfiles"]["size"] > 500000) {
							echo "Sorry, file is too big.";
							$uploadOk = 0;
						}
						
						// Check if $uploadOk is set to 0 by an error
						if ($uploadOk == 0) {
							echo "Alguma coisa falhou";
							// if everything is ok, try to upload file
							} else {
							if (move_uploaded_file($_FILES["myfiles"]["tmp_name"], $target_file)) {
								echo "The file ". basename( $_FILES["myfiles"]["name"]). " has been uploaded.";
								} else {
								echo "Alguma coisa falhou no envio";
							}
						}
						
			}

				
			?>
		<form name = "frmTestWav" action = "testWav.php" method = "POST" enctype="multipart/form-data">	
			<input id="myfiles" name="myfiles" type="file">
			<input type = "hidden" name = "task" value = "sendWav">
			<input type = "submit">
		</form>
		</div>
		
<!--************************ RODAPÉ ************************ -->
		<?php include("footer.php"); ?> 
	</body>
	
</html>

