<div class="text"><?php echo $lang['JOB_SECTOR']; ?></div>
<select multiple id = "cmbJobSector" name = "cmbJobSector[]" onchange="" >
	<?php
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare('SELECT JobS_PK_IdJobSector,JobS_DescJobSector,JobS_DescJobSector_ENG  FROM TJobSectors');
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>)</option>
				<?php
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
						
					?>
					<option value = "<?php echo $row["JobS_PK_IdJobSector"]; ?>" 
					id="<?php echo $row["JobS_PK_IdJobSector"]; ?>" title = "<?php 
							if ($_SESSION["language"] == 'ENG')
							echo $row["JobS_DescJobSector_ENG"];
							else 
						echo $row["JobS_DescJobSector"];	?>">
						<?php 
							if ($_SESSION["language"] == 'ENG')
							echo $row["JobS_DescJobSector_ENG"];
							else 
						echo $row["JobS_DescJobSector"];	?>
					</option>
					
					<?php
						
					}
					
				}
				else
				{
				?>
				<option value="0" selected>(<?php echo $lang['ALL_M']; ?>3)</option>
				<?php
					
				}
			}
			
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		
	?>
	
</select>