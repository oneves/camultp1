
<?php
	
		$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
		$DB_name = Config::db_name;
		
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
			$sql = "SELECT DISTINCT Chal_PK_IdChallenge, Chal_Title, Chal_Title_ENG,
							Chal_Description, Chal_Description_ENG, 
							Chal_BeginDate, Chal_EndDate, Chal_VoucherURL, 
							Chal_LogoURL, DATE(Chal_InsertedOn) as dtIns, 
							Chal_AllowGuests, Chal_Tags, cont.nr as count 
					FROM TChallenge c
					LEFT JOIN TChallengeResults ON Chal_PK_IdChallenge = CRCf_XK_IdChallenge
					LEFT JOIN (SELECT t.Chal_PK_IdChallenge as chal, SUM(t.nr) as nr
								FROM (SELECT Chal_PK_IdChallenge, COUNT(DISTINCT CRCf_TimeStamp) as nr
								FROM TChallenge
								LEFT JOIN TChallengeResults ON Chal_PK_IdChallenge = CRCf_XK_IdChallenge
								WHERE Chal_DeletedOn IS NULL
								AND CRCf_XK_IdUser < 0
								GROUP BY Chal_PK_IdChallenge
								UNION
								SELECT Chal_PK_IdChallenge, COUNT(DISTINCT CRCf_XK_IdUser) as nr
								FROM TChallenge
								LEFT JOIN TChallengeResults ON Chal_PK_IdChallenge = CRCf_XK_IdChallenge
								WHERE Chal_DeletedOn IS NULL
								AND CRCf_XK_IdUser > 0
								GROUP BY Chal_PK_IdChallenge) as t
								GROUP BY t.Chal_PK_IdChallenge) as cont ON Chal_PK_IdChallenge = cont.chal
					WHERE Chal_DeletedOn IS NULL
					AND Chal_FK_IdUserInvestigator = ".$_SESSION['userIdUser'].";";
			
							
			//echo $sql;
				$stmt = $DB_con->prepare($sql);
				
				$stmt->execute();
				
				if($stmt->rowCount() > 0)
				{
					?>
						<div id="div_time0">
							<table id="tbl_challenge">
					<?php
						while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
							?>
								<tr>
									<td class="td1"><strong><?php if(isset($_SESSION['language']) && $_SESSION['language'] == 'PT') echo $row["Chal_Title"]; else echo $row["Chal_Title_ENG"]; ?></strong></td>
									<td class="td2"><?php echo $lang['CREATED']; ?> <?php echo $row["dtIns"]; ?>, <?php if ($row["count"] == '') echo '0'; else echo $row["count"]; ?> <?php echo $lang['REPLIES']; ?></td>
									<td class="edit" onclick="detailChallenge(<?php echo $row["Chal_PK_IdChallenge"];?>);"><?php echo $lang['DETAIL']; ?></td>
									<td class="remove" onclick="deleteChallenge(<?php echo $row["Chal_PK_IdChallenge"];?>);">X</td>
								</tr>
							
							<?php
							
						
						}
						?>
							</table>
							<!--<div class="dot">....</div>-->
						</div>
											<?php
				}
				else
				{
					echo $lang['NOCHALLENGES'];
				}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
			RETURN FALSE;
		}
	
	?>
<input type="hidden" id="hdfieldIdChallenge" value = "<?php echo $_SESSION["idChallenge"]; ?>">


