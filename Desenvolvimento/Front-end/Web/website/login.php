<div class="wrap">
	<div class="lang" >
	<a href ="javascript:setLanguage('PT')"><?php if(isset($_SESSION['language']) && $_SESSION['language'] == 'PT') echo '<strong>PT</strong>'; else echo 'PT'; ?></a> | 
	<a href ="javascript:setLanguage('ENG')"><?php if(isset($_SESSION['language']) && $_SESSION['language'] == 'ENG') echo '<strong>ENG</strong>'; else echo 'ENG'; ?></a></div>
	<img id="logo" src="images/logo.png" />
    </div>
    <div>
        <section class="wrapper">
            <ul class="tabs" style="margin-left:0px">
                <li><a href="#tab1"><?php echo $lang['LOGIN']; ?></a></li>
            </ul>
            <div class="clr"></div>
            <section class="block">
                <article id="tab1">
                    <form name = "frmLogin" action="index.php" method = "POST">
						<br>
						<br>
                        <div class="text"><!--< ?php echo $lang['USERNAME']; ? >/ --><?php echo $lang['EMAIL']; ?></div>
                        <input type="text" id="username" name="username" class="input" required><br>
                        <div class="text"><?php echo $lang['PASSWORD']; ?></div>
                        <input type="password" id="password" name="password" class="input" required><br>
						<input type="hidden" name="task" value="login">
                        <br>
                        <br>
                        <div id="buttons">
                            <button type="button" id="bt_clean" onClick="javascript:clean();"><?php echo $lang['CLEAR']; ?></button>
                            <input type="button" id="bt_login" onClick="javascript:tryLogin();" value="<?php echo $lang['LOGIN']; ?>">
                        </div>
                    </form>
                </article>
            </section>
        </section>
</div>