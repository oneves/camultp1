<?php
/*
------------------
Language: Português
------------------
*/
 
$lang = array();

$lang['TESTE_ADMIN'] = 'Sou administrador!';
$lang['TESTE_INVESTIGADOR'] = 'Sou um investigador!';

$lang['AGE_RANGE'] = 'Faixa etária';
$lang['JOB_SECTOR'] = 'Setor de emprego';
$lang['EDUCATION_LEVEL'] = 'Nível educacional';
$lang['GENDER'] = 'Género';
$lang['MARITAL_STATUS'] = 'Estado civil';
$lang['COUNTRY'] = 'País';
$lang['CITY'] = 'Cidade';
$lang['NUMBER_CHILDREN'] = 'N.º de filhos';
$lang['ALL_M'] = 'Todos';
$lang['ALL_F'] = 'Todas';
$lang['SELECT'] = 'Selecione...';
$lang['NAME'] = 'Nome';
$lang['EMAIL'] = 'Email';
$lang['PASSWORD'] = 'Palavra-passe';
$lang['PHONE'] = 'Telefone';
$lang['COMPANY'] = 'Empresa';
$lang['REGISTER'] = 'REGISTAR INVESTIGADORES';
$lang['CREATE'] = 'CRIAR';
$lang['CLEAR'] = 'LIMPAR';
$lang['LISTACTIVE'] = 'LISTAR ACTIVOS';
$lang['CREATED'] = 'criado em';
$lang['CHALLENGES'] = 'desafios';
$lang['EDIT'] = 'Editar';
$lang['LISTACTIVEINV'] = 'LISTA INVESTIGADORES ATIVOS';
$lang['REPLIES'] = 'respostas';
$lang['DETAIL'] = 'Detalhe';
$lang['DETAILS'] = 'Detalhes';
$lang['NEWCHALLENGE'] = 'CRIAR DESAFIO';
$lang['LISTACTIVE'] = 'LISTA DE ATIVOS';
$lang['CHALLENGENAME'] = 'Título do Desafio';
$lang['DESCRIPTION'] = 'Descrição';
$lang['IMAGE'] = 'Imagem';
$lang['BEGINDATE'] = 'Data Início';
$lang['ENDDATE'] = 'Data Fim';
$lang['USERS'] = 'Público-alvo';
$lang['GUEST'] = 'Visitantes';
$lang['REGISTERED'] = 'Registados';
$lang['INVITEPARTICIPANTS'] = 'Convidar participantes';
$lang['OBLIGATORYFIELDS'] = 'Campos obrigatórios';
$lang['CHOOSEONE'] = 'Escolher pelo menos 1';
$lang['CHALLENGESUCCESS'] = 'DESAFIO CRIADO COM SUCESSO!';
$lang['CHALLENGEINSUCCESS'] = 'Não foi possível criar o desafio.';
$lang['CREATENEW'] = 'CRIAR NOVO DESAFIO';
$lang['INVITEPARTICIPANTSUPPER'] = 'CONVIDAR PARTICIPANTES';
$lang['PARTICIPANTSINVITER'] = 'Participantes convidados';
$lang['PARTICIPANTSBLOCKED'] = 'Participantes bloqueados';
$lang['NOINVITES'] = 'Ninguém convidado até ao momento';
$lang['1DAY'] = '1 Dia';
$lang['5DAYS'] = '5 Dias';
$lang['15DAYS'] = '15 Dias';
$lang['1HOUR'] = '1 Hora';
$lang['5HOURS'] = '5 Horas';
$lang['1MINUTE'] = '1 Minuto';
$lang['5MINUTES'] = '5 Minute';
$lang['NUMBERPARTICIPANTES'] = 'Número de participantes';
$lang['SHARE'] = 'Partilhar no Facebook';
$lang['NOCHALLENGES'] = 'Não existem desafios criados';
$lang['CHALLENGENOTIDENTIFIED'] = 'Desafio não identificado';
$lang['BLOCKADICIONALINFORMATION'] = 'Bloquear informação adicional';
$lang['EMAILSSEPARATEDBYCOMMA'] = 'emails separados por vírgula';
$lang['INSERTEMAIL'] = 'Inserir email';
$lang['SUBMIT'] = 'Enviar';
$lang['LIST'] = 'Listar';
$lang['SEARCHRESULT'] = 'Resultados da pesquisa';
$lang['EXPORT'] = 'Exportar';
$lang['NOINVESTIGATORSCREATED'] = 'Sem investigadores criados';
$lang['SAVE'] = 'Guardar';
$lang['LOGIN'] = 'ENTRAR';
$lang['CLEAR'] = 'LIMPAR';
$lang['USERNAME'] = 'Nome de utilizador';
$lang['NORESULTS'] = 'Sem resultados';
$lang['EMAILNOTFOUND'] = 'E-mail não encontrado na base de dados';
$lang['LOGINERROR'] = 'O utilizador não existe ou a palavra-passe encontra-se incorreta.';
$lang['INVITESSEND'] = 'Convite(s) envidado(s) com sucesso.';
$lang['ERRORMETADATA'] = 'Tem de definir os metadados.';
$lang['ALLFIELDSOBLIGATORY'] = 'Todos os campos são de preenchimento obrigatório.';
$lang['NOPERMISSIONS'] = 'Não tem permissões para utilizar este serviço.';
$lang['COULDNTLOGIN'] = 'Não foi possivel fazer login.';
$lang['FILEISNOTANIMAGELOGO'] = 'O ficheiro não é uma imagem.';
$lang['FILEISTOOLARGELOGO'] = 'A imagem é demasiado grande.';
$lang['FILEWASNOTUPLOADEDLOGO'] = 'A imagem não foi enviada.';
$lang['FILEISNOTANIMAGEVOUCHER'] = 'O ficheiro não é uma imagem.';
$lang['FILEISTOOLARGEVOUCHER'] = 'O voucher é demasiado grande.';
$lang['FILEWASNOTUPLOADEDVOUCHER'] = 'O voucher não foi enviado.';
$lang['USERNAMEERROR'] = 'Por favor, indique o seu nome de utilizador ou email.';
$lang['PASSWORDERROR'] = 'Por favor, indique a sua palavra-passe.';
$lang['NUMBERERROR'] = 'O número de telefone apenas pode conter algarismos.';
$lang['EMAILERROR'] = 'Email inválido.';
$lang['YOUHAVEANINVITE'] = 'Recebeu um convite.';
$lang['NODATA'] = 'Não retornou dados.';
$lang['INVESTCONFIRM'] = 'Tem a certeza que pretende bloquear o utilizador?';
$lang['OK'] = 'Sim';
$lang['CANCEL'] = 'Cancelar';
$lang['CHALLENGECONFIRM'] = 'Tem a certeza que pretende eliminar o desafio?';
$lang['UPDATECONFIRM'] = 'Tem a certeza que pretende atualizar os dados do investigador?';
$lang['CHALLNAMEPT'] = 'Deve definir o título do desafio em Português.';
$lang['CHALLDESCPT'] = 'Deve definir a descrição do desafio em Português.';
$lang['CHALLNAMEENG'] = 'Deve definir o título do desafio em Inglês.';
$lang['CHALLDESCENG'] = 'Deve definir a descrição do desafio em Inglês.';
$lang['CHALLIMG'] = 'Deve inserir o logótipo do desafio.';
$lang['CHALLVOUCHER'] = 'Deve inserir o voucher do desafio.';
$lang['CHALLDATAINI'] = 'Deve inserir a data início do desafio.';
$lang['CHALLDATAFIM'] = 'Deve inserir a data fim do desafio.';
$lang['CHALLTAGS'] = 'Tem de escolher as tags separadas por vírgula.';
$lang['CHALLMEDIA'] = 'Tem de escolher pelo menos um dos tipos de média.';
$lang['CHALLMETATXT'] = 'Tem de escolher pelo menos uma contagem para o texto.';
$lang['CHALLMETAIMG'] = 'Tem de escolher pelo menos uma contagem para a imagem.';
$lang['CHALLMETAVID'] = 'Tem de escolher pelo menos uma contagem para o vídeo.';
$lang['CHALLMETAAUD'] = 'Tem de escolher pelo menos uma contagem para o aúdio.';
$lang['CHALLWORD'] = 'Tem de escolher a palavra a contar.';
$lang['REJECTED'] = 'Número de e-mails rejeitados: ';
$lang['CHALLTAGSINFO'] = '(Separadas por vírgula)';

 
// Menu
 
$lang['MENU_LOGOUT'] = 'Sair';

?>