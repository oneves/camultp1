<?php
	
	
	
	error_reporting(0);
	
	/*$DB_host = Config::sgbd_server_name;
		$DB_user = Config::public_login;
		$DB_pass = Config::public_pwd;
	$DB_name = Config::db_name;*/
	
	$DB_host = "localhost";
	$DB_user = "root";
	$DB_pass = "Asist2015";
	$DB_name = "camul";
	
	
	//$referencedate = $_GET['current_time']
	
	if (isset($_POST['idChallange']) && $_POST['idChallange'] != "" && isset($_POST['refDate']) && $_POST['refDate'] != "")
	{
		if (isset($_POST['langu']) && $_POST['langu'] != "")
			$lang = $_POST['langu'];
		else
			$lang = 'PT';
		
		$idChallange = $_POST['idChallange'];
		$refDate = $_POST['refDate'];
		//$lang = $_POST['langu'];
		
		if ($lang == 'ENG') 
			$filename = "ChallengeResults_".$_POST['idChallange']."_".$_POST['refDate'].".xls";
		else
			$filename = "ResultadosDesafio_".$_POST['idChallange']."_".$_POST['refDate'].".xls";
		header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename=".$filename );
	
		
		
		$sql = 'SELECT r.CRCf_XK_IdChallenge,
									 Chal_Title, 
									 Chal_Title_ENG, 
									 Chal_Description, 
									 Chal_Description_ENG, 
									 Chal_BeginDate, 
									 Chal_EndDate, 
									 Chal_InsertedOn,
									 Chal_AllowGuests, 
									 r.CRCf_XK_IdMediaType,
									 TMed_DescMediaType, 
									 TMed_DescMediaType_ENG,
									 r.CRCf_XK_IdMediaMetaData,
									 TMDt_DescMediaMetaData,
									 TMDt_DescMediaMetaData_ENG,
									 r.CRCf_XK_IdMediaMetaDataDetails,
									 MDDt_DescMediaMetaDataDetails, 
									 MDDt_DescMediaMetaDataDetails_ENG,
									 c.CRCf_Values,
									 SUM(r.CRCf_Count) as count,
									 (SELECT COUNT(DISTINCT CRCf_XK_IdUser)
										FROM TChallengeResults
										WHERE CRCf_XK_IdChallenge = '.$idChallange.'
										AND CRCf_TimeStamp BETWEEN Chal_BeginDate AND \''.$refDate.'\') as total
										FROM 
										TChallengeResults r
										INNER JOIN TChallengeResultsConfig c ON r.CRCf_XK_IdChallenge = c.CRCf_XK_IdChallenge
																				AND r.CRCf_XK_IdMediaType = c.CRCf_XK_IdMediaType
																				AND r.CRCf_XK_IdMediaMetaData = c.CRCf_XK_IdMediaMetaData
										INNER JOIN TChallenge ON Chal_PK_IdChallenge = c.CRCf_XK_IdChallenge
										INNER JOIN TTMedia ON c.CRCf_XK_IdMediaType = TMed_PK_IdMediaType
										INNER JOIN TTMediaMetaData ON c.CRCf_XK_IdMediaMetaData = TMDt_PK_IdMediaMetaData
										INNER JOIN TTMediaMetaDataDetails ON r.CRCf_XK_IdMediaMetaDataDetails = MDDt_PK_IdMediaMetaDataDetails
										WHERE r.CRCf_XK_IdChallenge = '.$idChallange.'
										AND r.CRCf_TimeStamp BETWEEN Chal_BeginDate AND \''.$refDate.'\' 
										GROUP BY r.CRCf_XK_IdChallenge, 
													r.CRCf_XK_IdMediaType,
													TMed_DescMediaType, 
													TMed_DescMediaType_ENG,
													r.CRCf_XK_IdMediaMetaData,
													TMDt_DescMediaMetaData,
													TMDt_DescMediaMetaData_ENG,
													r.CRCf_XK_IdMediaMetaDataDetails,
													MDDt_DescMediaMetaDataDetails, 
													MDDt_DescMediaMetaDataDetails_ENG,
													c.CRCf_Values
										UNION SELECT r.CRCf_XK_IdChallenge,
									 Chal_Title, 
									 Chal_Title_ENG, 
									 Chal_Description, 
									 Chal_Description_ENG, 
									 Chal_BeginDate, 
									 Chal_EndDate, 
									 Chal_InsertedOn,
									 Chal_AllowGuests, 
									 NULL,
									 \'Tag\', 
									 \'Tag\',
									 NULL,
									 NULL,
									 NULL,
									 NULL,
									 NULL,
									 NULL,
									 CUFd_Feedback, COUNT(DISTINCT CUFd_XK_IdUser, f.CUFd_Feedback) as count,
									 (SELECT COUNT(DISTINCT CRCf_XK_IdUser)
										FROM TChallengeResults
										WHERE CRCf_XK_IdChallenge = '.$idChallange.'
										AND CRCf_TimeStamp BETWEEN Chal_BeginDate AND \''.$refDate.'\') as total
										FROM TChallengeResults r
										INNER JOIN TChallengeResultsConfig c ON r.CRCf_XK_IdChallenge = c.CRCf_XK_IdChallenge
																				AND r.CRCf_XK_IdMediaType = c.CRCf_XK_IdMediaType
																				AND r.CRCf_XK_IdMediaMetaData = c.CRCf_XK_IdMediaMetaData
										INNER JOIN TChallenge ON Chal_PK_IdChallenge = c.CRCf_XK_IdChallenge
										LEFT JOIN TChallengeUserFeedback f ON r.CRCf_XK_IdChallenge = f.CUFd_XK_IdChallenge
																			AND r.CRCf_XK_IdUser = f.CUFd_XK_IdUser
										INNER JOIN TUsers ON User_PK_IdUser = CRCf_XK_IdUser 
                                        LEFT JOIN TChallengeInvites ON CInv_UserEmail = User_Email 
									AND r.CRCf_XK_IdUser = f.CUFd_XK_IdUser
										WHERE r.CRCf_XK_IdChallenge = '.$idChallange.'
										AND r.CRCf_TimeStamp BETWEEN Chal_BeginDate AND \''.$refDate.'\' 
										AND (CInv_AllowFeedback IS NULL OR CInv_AllowFeedback <> 0)
										GROUP BY CUFd_XK_IdChallenge,
		CUFd_Feedback;';
		try
		{
			$DB_con = new PDO("mysql:host={$DB_host};dbname={$DB_name}",$DB_user,$DB_pass);
			$DB_con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			
			$stmt = $DB_con->prepare($sql);
			
			//echo $sql;
			
			
			if ($stmt->execute())
			{
				
				if($stmt->rowCount() > 0)
				{
					if ($lang == 'ENG')
					{
						$IdChallenge = 'Id. Challenge';
						$Title = 'Challenge Title';
						$Description = 'Description';
						$BeginDate = 'Begin Date';
						$EndDate = 'End Date';
						$InsertedOn = 'Inserted On';
						$AllowGuests = 'Allow Guests';
						$DescMediaType = 'Media Type';
						$DescMediaMetaData = 'Meta Data Type';
						$DescMediaMetaDataDetails = 'Details';
						$Values = 'Values';
						$Count = 'Count';
						$Total = 'Total replies';
						$DataRef = 'Reference Date';
					}
					else
					{
						$IdChallenge = 'Cód. Desafio';
						$Title = 'Título desafio';
						$Description = 'Descrição';
						$BeginDate = 'Data Início';
						$EndDate = 'Data Fim';
						$InsertedOn = 'Inserido Em';
						$AllowGuests = 'Permite Não Registados';
						$DescMediaType = 'Tipos de Media';
						$DescMediaMetaData = 'Tipos de MetaDados';
						$DescMediaMetaDataDetails = 'Detalhes';
						$Values = 'Valores';
						$Count = 'Contagem';
						$Total = 'Nr total respostas';
						$DataRef = 'Data Referência';
					}
					
					
						echo $IdChallenge . "\t" . $Title . "\t" . $Description . "\t" . $BeginDate . "\t" .
							 $EndDate . "\t" . $InsertedOn . "\t" . $AllowGuests . "\t" . $DescMediaType . "\t" .
							 $DescMediaMetaData . "\t" . $DescMediaMetaDataDetails . "\t" . $Values . "\t" . $Count . "\t" .
							 $Total . "\t" . $DataRef . "\t" . "\n";
					
					
					while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
					
					if ($lang == 'ENG')
					{
						$ValIdChallenge = $row["CRCf_XK_IdChallenge"];
						$ValTitle = $row["Chal_Title_ENG"];
						$ValDescription = $row["Chal_Description_ENG"];
						$ValBeginDate = $row["Chal_BeginDate"];
						$ValEndDate = $row["Chal_EndDate"];
						$ValInsertedOn = $row["Chal_InsertedOn"];
						$ValAllowGuests = $row["Chal_AllowGuests"];
						$ValDescMediaType = $row["TMed_DescMediaType_ENG"];
						$ValDescMediaMetaData = $row["TMDt_DescMediaMetaData_ENG"];
						$ValDescMediaMetaDataDetails = $row["MDDt_DescMediaMetaDataDetails_ENG"];
						$ValValues = $row["CRCf_Values"];
						$ValCount = $row["count"];
						$ValTotal = $row["total"];
					}
					else
					{
						$ValIdChallenge = $row["CRCf_XK_IdChallenge"];
						$ValTitle = $row["Chal_Title"];
						$ValDescription = $row["Chal_Description"];
						$ValBeginDate = $row["Chal_BeginDate"];
						$ValEndDate = $row["Chal_EndDate"];
						$ValInsertedOn = $row["Chal_InsertedOn"];
						$ValAllowGuests = $row["Chal_AllowGuests"];
						$ValDescMediaType = $row["TMed_DescMediaType"];
						$ValDescMediaMetaData = $row["TMDt_DescMediaMetaData"];
						$ValDescMediaMetaDataDetails = $row["MDDt_DescMediaMetaDataDetails"];
						$ValValues = $row["CRCf_Values"];
						$ValCount = $row["count"];
						$ValTotal = $row["total"];
					}
					
					echo $ValIdChallenge . "\t" . $ValTitle . "\t" . $ValDescription . "\t" . $ValBeginDate . "\t" .
							 $ValEndDate . "\t" . $ValInsertedOn . "\t" . $ValAllowGuests . "\t" . $ValDescMediaType . "\t" .
							 $ValDescMediaMetaData . "\t" . $ValDescMediaMetaDataDetails . "\t" . $ValValues . "\t" . $ValCount . "\t" .
							 $ValTotal . "\t" . $refDate . "\t" . "\n";

					}
				}
				
				
			}
		}
		
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}
	else
		
	{	header( "Content-Type: application/vnd.ms-excel" );
		header( "Content-disposition: attachment; filename= Error.xlsx" );
		echo "Error exporting/ Erro ao exportar." . "\t" . "\n";
	}

	?>				