/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package authentication;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.plus.Plus;
import com.google.api.services.plus.PlusScopes;
import com.google.api.services.plus.model.Person;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

public class googlePlus {

    public static void auth() {
        try {
            String APPLICATION_NAME = ""; // TODO: registar APP na google+ developer console...

            java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), "where to save"); // TODO: where to save...
            FileDataStoreFactory dataStoreFactory = null;

            // Set up the HTTP transport and JSON factory
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            JacksonFactory jsonFactory = new JacksonFactory();

            // Load client secrets
            Reader reader = new FileReader("client_secrets.json"); // TODO
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(jsonFactory, reader);

            // Set up authorization code flow
            GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                    httpTransport, jsonFactory, clientSecrets,
                    Collections.singleton(PlusScopes.PLUS_ME)).setDataStoreFactory(dataStoreFactory)
                    .build();

            // Authorize
            Credential credential
                    = new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

            // Set up the main Google+ class
            Plus plus = new Plus.Builder(httpTransport, jsonFactory, credential)
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            // Make a request to access your profile and display it to console
            Person profile = plus.people().get("me").execute();
            System.out.println("ID: " + profile.getId());
            System.out.println("Name: " + profile.getDisplayName());
            System.out.println("Image URL: " + profile.getImage().getUrl());
            System.out.println("Profile URL: " + profile.getUrl());

        } catch (GeneralSecurityException ex) {
            Logger.getLogger(googlePlus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(googlePlus.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(googlePlus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
