package services;

import components.audio_text.AudioComponent;
import components.audio_text.DefaultAttributes;
import components.audio_text.Transcoder;
import components.image_video.Image;
import databaseConnectivity.DBServices;
import databaseConnectivity.DatabaseConn;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.codec.binary.Base64;

/**
 * Dummy test that call our back-end components.
 *
 * @author Alface
 */
public class OfflineTest {

    public static void main(String[] args) throws Exception {

//        System.out.println("Image analysis test with binary data...");
//        imageTestImg();
//        System.out.println("Speech analysis test with binary data...");
//        speechTest();

//        Timestamp ts = saveTimeStamp();
//        System.out.println(ts);
//        DatabaseConn.getInstance();
//        System.out.println(DBServices.getInstance().checkIfConditionsMatch(1, 3, 2));
//
//        
//        List<String> lines = Arrays.asList(encodeFileToBase64Binary(AudioComponent.audioFile));
//        Path file = Paths.get("base64_audio.txt");
//        Files.write(file, lines, Charset.forName("UTF-8"));
          //File flacFile=AudioComponent.wavToFlacEncoder(new File(AudioComponent.audioFile_wav));
          Transcoder.transcode(AudioComponent.audioFile_wav,     AudioComponent.audioFile_wav.replace(".wav", ".flac")    , DefaultAttributes.FLAC_MONO_16bit_8KHZ);
          System.out.println(AudioComponent.speechToText(AudioComponent.English_US, AudioComponent.audioFile_wav.replace(".wav", ".flac")));
    }

    private static void imageTestImg() {

        String imgPath = System.getProperty("user.dir") + "\\src\\java\\resources\\image_samples\\groupPhoto2.jpg";///src/java/resources/groupPhoto2.jpg";

        byte[] img = null;
        try {
            img = Files.readAllBytes(Paths.get(imgPath));
        } catch (IOException ex) {
            System.out.println("Error reading the image: " + ex.getMessage());
        }

        if (img != null) {
            Image.processImageImgTest(img);
        }

        System.out.println("Finished...");
    }

    private static void speechTest() throws Exception {
        List<String> targets = new ArrayList();
        targets.add("business");
        targets.add("invoices");
        targets.add("your");
        System.out.println(AudioComponent.countTargetWordsInAudio(targets, AudioComponent.lang, AudioComponent.audioFile));

    }

    public static Timestamp saveTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);

        return Timestamp.valueOf(strDate);
    }

    private static String encodeFileToBase64Binary(String fileName)
            throws IOException {

        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);

        return encodedString;
    }

    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }
}
