package services;

import components.audio_text.AudioComponent;
import components.audio_text.DefaultAttributes;
import components.audio_text.Transcoder;
import components.audio_text.ffmpeg.EncoderException;
import components.image_video.Image;
import databaseConnectivity.DBServices;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import sun.misc.BASE64Decoder;

@Path("REST")
public class RESTServices {

    @Context
    private UriInfo context;
    private static long nrOfAudioHits = 0;
    private static long nrOfTextHits = 0;
    private static long nrOfImageHits = 0;
    private static long nrOfVideoHits = 0;

    public RESTServices() {
    }

    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

//    @POST
//    @Path("audioAnalysis")
//    @Produces("text/plain; charset=UTF-8")
//    public Boolean putString_audioAnalysis(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @HeaderParam("speechLang") String speechLang, @FormParam("content") String content, @FormParam("feedback") String feedback) {
//
//        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge, - DONE
//            //                           - check if this challenge involves audio analysis - DONE
//            //                           - check what is meant to be analysed in the audio - DONE
//            //                           - check if speechLang exists
//            try {
//                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Audio"))) {
//                    return false;
//                }
//                if (content.isEmpty()) {
//                    return false;
//                }
//            } catch (Exception e) {
//                return false;
//            }
//
//            nrOfAudioHits++;
//            String path = System.getProperty("user.home") + "/samples/audio";
//            File file = decodeBase64(content, path, "wav");
//            File file2 = new File(file.getAbsolutePath().replace(".wav", ".flac"));
//            try {
//                Transcoder.transcode(file, file2, DefaultAttributes.FLAC_MONO_16bit_8KHZ);
//            } catch (EncoderException ex) {
//                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
//                return false;
//            }
//            if (file == null) {
//                return false;
//            }
//            if (file2 == null) {
//                return false;
//            }
//            file.delete();
//
//            //run analizis on audio vvvv
//            boolean ret = true;
//            try {
//                Timestamp ts = saveTimeStamp();
//                List<Integer> resList = AudioComponent.countTargetWordsInAudio(
//                        DBServices.getInstance().getTargetWordsByChalengeID(Integer.parseInt(challengeID), DBServices.getInstance().DB_MEDIATYPES.get("Audio")),
//                        speechLang, file2.getAbsolutePath()
//                );
//                ret = DBServices.getInstance().saveResuls_sound(Integer.parseInt(challengeID), Integer.parseInt(userID), resList, ts);
//
//            } catch (Exception ex) {
//                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            //delete no longer necessary audio file
//            file2.delete();
//            DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
//            sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
//
//            return ret;
//        }
//        return false;
//    }
    @POST
    @Path("audioAnalysisURL")
    @Produces("text/plain; charset=UTF-8")
    public Boolean putString_audioAnalysisURL(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @HeaderParam("speechLang") String speechLang, @FormParam("url") String content, @FormParam("feedback") String feedback) {

        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge, - DONE
            //                           - check if this challenge involves audio analysis - DONE
            //                           - check what is meant to be analysed in the audio - DONE
            //                           - check if speechLang exists
            try {
                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Audio"))) {
                    return false;
                }
                if (content.isEmpty()) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }

            nrOfAudioHits++;
            String path = System.getProperty("user.home") + "/samples/audio";
            File file = null;
            File file2 = null;
            try {
                file = this.fetchRemoteFile2(content); //decodeBase64(content, path, "wav");
                file2 = new File(file.getAbsolutePath().replace(".wav", ".flac"));

                Transcoder.transcode(file, file2, DefaultAttributes.FLAC_MONO_16bit_8KHZ);
            } catch (EncoderException ex) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            } catch (MalformedURLException malEx) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, malEx);
                return false;
            }
            if (file == null) {
                return false;
            }
            if (file2 == null) {
                return false;
            }
            file.delete();

            //run analizis on audio vvvv
            boolean ret = true;
            try {
                Timestamp ts = saveTimeStamp();
                List<Integer> resList = AudioComponent.countTargetWordsInAudio(
                        DBServices.getInstance().getTargetWordsByChalengeID(Integer.parseInt(challengeID), DBServices.getInstance().DB_MEDIATYPES.get("Audio")),
                        speechLang, file2.getAbsolutePath()
                );
                ret = DBServices.getInstance().saveResuls_sound(Integer.parseInt(challengeID), Integer.parseInt(userID), resList, ts);

            } catch (Exception ex) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
            }
            //delete no longer necessary audio file
            file2.delete();
            DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
            sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));

            return ret;
        }
        return false;
    }

    @POST
    @Path("textAnalysisURL")
    @Produces("text/plain; charset=UTF-8")
    public Boolean putString_textAnalysisURL(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @FormParam("url") String content, @FormParam("feedback") String feedback) {

        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge, - DONE
            //                           - check if this challenge involves text analysis - DONE
            //                           - check what is meant to be analysed in the text - DONE

            try {
                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Text"))) {
                    return false;
                }
                if (content.isEmpty()) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }

            //run analizis on text vvvv
            nrOfTextHits++;
            boolean ret = true;
            try {
                Timestamp ts = saveTimeStamp();
                List<Integer> resList = AudioComponent.countTargetWords(
                        DBServices.getInstance().getTargetWordsByChalengeID(Integer.parseInt(challengeID), DBServices.getInstance().DB_MEDIATYPES.get("Text")),
                        content
                );
                ret = DBServices.getInstance().saveResuls_text(Integer.parseInt(challengeID), Integer.parseInt(userID), resList, ts);
                DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
                sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
                return ret;

            } catch (Exception ex) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return false;
    }

//    @POST
//    @Path("imageAnalysis")
//    @Produces("text/plain; charset=UTF-8")
//    public Boolean putString_imageAnalysis(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @FormParam("content") String content, @FormParam("feedback") String feedback) {
//        System.out.println("ch id: " + challengeID);
//        System.out.println("us id: " + userID);
//        System.out.println("content: " + content);
//        System.out.println("feedback: " + feedback);
//        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge,
//            //                           - check if this challenge involves img analysis
//            //                           - check what is meant to be analysed in the img
//            try {
//                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Image"))) {
//                    return false;
//                }
//                if (content.isEmpty()) {
//                    return false;
//                }
//            } catch (Exception e) {
//                return false;
//            }
//
//            nrOfImageHits++;
//            try {
//                System.out.println("Content:\n" + content);
//                System.out.println("Feedback:\n" + feedback);
//                byte[] imgR = org.apache.commons.codec.binary.Base64.decodeBase64(content.getBytes());
//
//                int[] analyse = DBServices.getInstance().checkWhatToAnalyse(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Image"));
//                System.out.println("Anal:" + analyse.toString());
//                // 0-nr cara 1-idades 2-generos 3-emocoes
//                //int[] analyse = {1, 0, 0, 0};
//
//                ArrayList<Double> ages = new ArrayList<>();
//                ArrayList<String> genders = new ArrayList<>();
//                ArrayList<Map<String, Double>> emotions = new ArrayList<>();
//
//                Timestamp ts = saveTimeStamp();
//
//                if (analyse[0] == 1) {
//                    // contar caras
//                    genders = Image.basicInfo(imgR).getGenders();
//                    if (genders.isEmpty()) {
//                        return false;
//                    } else {
//                        DBServices.getInstance().saveResulsNrFaces_image(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
//                    }
//                }
//                if (analyse[1] == 1) {
//                    // contar idades
//                    ages = Image.basicInfo(imgR).getAges();
//                    if (ages.isEmpty()) {
//                        return false;
//                    } else {
//                        DBServices.getInstance().saveResulsAges_image(Integer.parseInt(challengeID), Integer.parseInt(userID), ages, ts);
//                    }
//                }
//                if (analyse[2] == 1) {
//                    // contar generos
//                    genders = Image.basicInfo(imgR).getGenders();
//                    if (genders.isEmpty()) {
//                        return false;
//                    } else {
//                        DBServices.getInstance().saveResulsGenders_image(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
//                    }
//                }
//                if (analyse[3] == 1) {
//                    // contar emoções
//                    emotions = Image.emotions(imgR).getEmotions();
//                    if (emotions.isEmpty()) {
//                        return false;
//                    } else {
//                        DBServices.getInstance().saveResulsEmotions_image(Integer.parseInt(challengeID), Integer.parseInt(userID), emotions, ts);
//                    }
//                }
//                DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
//                sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
//                return true;
//
//            } catch (Exception ex) {
//                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return false;
//    }
    @POST
    @Path("imageAnalysisURL")
    @Produces("text/plain; charset=UTF-8")
    public Boolean putString_imageAnalysisURL(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @FormParam("url") String url, @FormParam("feedback") String feedback) {
        System.out.println("ch id: " + challengeID);
        System.out.println("us id: " + userID);
        System.out.println("url: " + url);
        System.out.println("feedback: " + feedback);
        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge,
            //                           - check if this challenge involves img analysis
            //                           - check what is meant to be analysed in the img
            try {
                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Image"))) {
                    return false;
                }
                if (url.isEmpty()) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }

            nrOfImageHits++;
            try {
                System.out.println("URL:\n" + url);
                System.out.println("Feedback:\n" + feedback);
                byte[] imgR = fetchRemoteFile(url);
                //= org.apache.commons.codec.binary.Base64.decodeBase64(content.getBytes());

                int[] analyse = DBServices.getInstance().checkWhatToAnalyse(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Image"));
//                System.out.println("Anal:" + analyse.toString());
                // 0-nr cara 1-idades 2-generos 3-emocoes
                //int[] analyse = {1, 0, 0, 0};

                ArrayList<Double> ages = new ArrayList<>();
                ArrayList<String> genders = new ArrayList<>();
                ArrayList<Map<String, Double>> emotions = new ArrayList<>();

                Timestamp ts = saveTimeStamp();

                if (analyse[0] == 1) {
                    // contar caras
                    genders = Image.basicInfo(imgR).getGenders();
                    if (genders.isEmpty()) {
                        return false;
                    } else {
                        DBServices.getInstance().saveResulsNrFaces_image(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
                    }
                }
                if (analyse[1] == 1) {
                    // contar idades
                    ages = Image.basicInfo(imgR).getAges();
                    if (ages.isEmpty()) {
                        return false;
                    } else {
                        DBServices.getInstance().saveResulsAges_image(Integer.parseInt(challengeID), Integer.parseInt(userID), ages, ts);
                    }
                }
                if (analyse[2] == 1) {
                    // contar generos
                    genders = Image.basicInfo(imgR).getGenders();
                    if (genders.isEmpty()) {
                        return false;
                    } else {
                        DBServices.getInstance().saveResulsGenders_image(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
                    }
                }
                if (analyse[3] == 1) {
                    // contar emoções
                    emotions = Image.emotions(imgR).getEmotions();
                    if (emotions.isEmpty()) {
                        return false;
                    } else {
                        DBServices.getInstance().saveResulsEmotions_image(Integer.parseInt(challengeID), Integer.parseInt(userID), emotions, ts);
                    }
                }
                DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
                sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
                return true;

            } catch (Exception ex) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    private byte[] fetchRemoteFile(String location) throws Exception {
        URL url = new URL(location);
        BufferedImage image = ImageIO.read(url);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "png", baos);
        baos.flush();
        byte[] imageInByte = baos.toByteArray();
        baos.close();
        return imageInByte;
    }

//    @POST
//    @Path("videoAnalysis")
//    @Produces("text/plain; charset=UTF-8")
//    public Boolean putString_videoAnalysis(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @FormParam("content1") String content1, @FormParam("content2") String content2, @FormParam("content3") String content3, @FormParam("content4") String content4, @FormParam("content5") String content5, @FormParam("feedback") String feedback) {
//
//        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge,
//            //                           - check if this challenge involves img analysis
//            //                           - check what is meant to be analysed in the img
//            try {
//                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Video"))) {
//                    return false;
//                }
//                if (content1.isEmpty() || content2.isEmpty() || content3.isEmpty() || content4.isEmpty() || content5.isEmpty()) {
//                    return false;
//                }
//            } catch (Exception e) {
//                return false;
//            }
//
//            ArrayList<String> contents = new ArrayList<>();
//
//            contents.add(content1);
//            contents.add(content2);
//            contents.add(content3);
//            contents.add(content4);
//            contents.add(content5);
//
//            nrOfVideoHits++;
//            try {
//
//                int[] analyse = DBServices.getInstance().checkWhatToAnalyse(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Video"));
//                // 0-nr cara 1-idades 2-generos 3-emocoes
//
//                ArrayList<Integer> faces = new ArrayList<>();
//                ArrayList< ArrayList<Double>> ages = new ArrayList<>();
//                ArrayList<ArrayList<String>> genders = new ArrayList<>();
//                ArrayList< ArrayList<Map<String, Double>>> emotions = new ArrayList<>();
//
//                for (int i = 0; i < contents.size(); i++) {
//                    //String result = java.net.URLDecoder.decode(contents.get(i), "UTF-8");
//                    byte[] img = org.apache.commons.codec.binary.Base64.decodeBase64(contents.get(i).getBytes());
//
//                    if (analyse[0] == 1) {
//                        // contar caras
//                        faces.add(Image.basicInfo(img).getGenders().size());
//                    }
//                    if (analyse[1] == 1) {
//                        // contar idades
//                        ages.add(Image.basicInfo(img).getAges());
//                    }
//                    if (analyse[2] == 1) {
//                        // contar generos
//                        genders.add(Image.basicInfo(img).getGenders());
//                    }
//                    if (analyse[3] == 1) {
//                        // contar emoções
//                        emotions.add(Image.emotions(img).getEmotions());
//                    }
//                }
//
//                Timestamp ts = saveTimeStamp();
//
//                //TODO: tornar dinamico com a BD/arranjar outro algoritmos de media (actual regista o maior numero)...
//                if (analyse[0] == 1) {
//                    // contar caras
//                    boolean save = false;
//                    for (Integer face : faces) {
//                        if (face != 0) {
//                            save = true;
//                        }
//                    }
//                    if (save) {
//                        DBServices.getInstance().saveResulsNrFaces_video(Integer.parseInt(challengeID), Integer.parseInt(userID), faces, ts);
//                    } else {
//                        return false;
//                    }
//                }
//                if (analyse[1] == 1) {
//                    // contar idades
//                    boolean save = false;
//                    for (ArrayList<Double> age : ages) {
//                        if (!age.isEmpty()) {
//                            save = true;
//                        }
//                    }
//                    if (save) {
//                        DBServices.getInstance().saveResulsAges_video(Integer.parseInt(challengeID), Integer.parseInt(userID), ages, ts);
//                    } else {
//                        return false;
//                    }
//                }
//                if (analyse[2] == 1) {
//                    // contar generos
//                    boolean save = false;
//                    for (ArrayList<String> gender : genders) {
//                        if (!gender.isEmpty()) {
//                            save = true;
//                        }
//                    }
//                    if (save) {
//                        DBServices.getInstance().saveResulsGenders_video(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
//                    } else {
//                        return false;
//                    }
//                }
//                if (analyse[3] == 1) {
//                    // contar emoções
//                    boolean save = false;
//                    for (ArrayList<Map<String, Double>> emotion : emotions) {
//                        if (!emotion.isEmpty()) {
//                            save = true;
//                        }
//                    }
//                    if (save) {
//                        DBServices.getInstance().saveResulsEmotions_video(Integer.parseInt(challengeID), Integer.parseInt(userID), emotions, ts);
//                    } else {
//                        return false;
//                    }
//                }
//                DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
//                sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
//                return true;
//
//            } catch (Exception ex) {
//                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//        return false;
//    }
    @POST
    @Path("videoAnalysis")
    @Produces("text/plain; charset=UTF-8")
    public Boolean putString_videoAnalysisURL(@HeaderParam("challengeID") String challengeID, @HeaderParam("userID") String userID, @FormParam("url1") String url1, @FormParam("url2") String url2, @FormParam("url3") String url3, @FormParam("url4") String url4, @FormParam("url5") String url5, @FormParam("feedback") String feedback) {

        if (challengeID != null && !challengeID.isEmpty()) { // TODO: Use this param to check the DB for this challenge,
            //                           - check if this challenge involves img analysis
            //                           - check what is meant to be analysed in the img
            try {
                if (!DBServices.getInstance().checkIfConditionsMatch(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Video"))) {
                    return false;
                }
                if (url1.isEmpty() || url2.isEmpty() || url3.isEmpty() || url4.isEmpty() || url5.isEmpty()) {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }

            ArrayList<String> contents = new ArrayList<>();

            contents.add(url1);
            contents.add(url2);
            contents.add(url3);
            contents.add(url4);
            contents.add(url5);

            nrOfVideoHits++;
            try {

                int[] analyse = DBServices.getInstance().checkWhatToAnalyse(Integer.parseInt(challengeID), Integer.parseInt(userID), DBServices.getInstance().DB_MEDIATYPES.get("Video"));
                // 0-nr cara 1-idades 2-generos 3-emocoes

                ArrayList<Integer> faces = new ArrayList<>();
                ArrayList< ArrayList<Double>> ages = new ArrayList<>();
                ArrayList<ArrayList<String>> genders = new ArrayList<>();
                ArrayList< ArrayList<Map<String, Double>>> emotions = new ArrayList<>();

                for (int i = 0; i < contents.size(); i++) {
                    //String result = java.net.URLDecoder.decode(contents.get(i), "UTF-8");
                    byte[] img = fetchRemoteFile(contents.get(i));

                    if (analyse[0] == 1) {
                        // contar caras
                        faces.add(Image.basicInfo(img).getGenders().size());
                    }
                    if (analyse[1] == 1) {
                        // contar idades
                        ages.add(Image.basicInfo(img).getAges());
                    }
                    if (analyse[2] == 1) {
                        // contar generos
                        genders.add(Image.basicInfo(img).getGenders());
                    }
                    if (analyse[3] == 1) {
                        // contar emoções
                        emotions.add(Image.emotions(img).getEmotions());
                    }
                }

                Timestamp ts = saveTimeStamp();

                //TODO: tornar dinamico com a BD/arranjar outro algoritmos de media (actual regista o maior numero)...
                if (analyse[0] == 1) {
                    // contar caras
                    boolean save = false;
                    for (Integer face : faces) {
                        if (face != 0) {
                            save = true;
                        }
                    }
                    if (save) {
                        DBServices.getInstance().saveResulsNrFaces_video(Integer.parseInt(challengeID), Integer.parseInt(userID), faces, ts);
                    } else {
                        return false;
                    }
                }
                if (analyse[1] == 1) {
                    // contar idades
                    boolean save = false;
                    for (ArrayList<Double> age : ages) {
                        if (!age.isEmpty()) {
                            save = true;
                        }
                    }
                    if (save) {
                        DBServices.getInstance().saveResulsAges_video(Integer.parseInt(challengeID), Integer.parseInt(userID), ages, ts);
                    } else {
                        return false;
                    }
                }
                if (analyse[2] == 1) {
                    // contar generos
                    boolean save = false;
                    for (ArrayList<String> gender : genders) {
                        if (!gender.isEmpty()) {
                            save = true;
                        }
                    }
                    if (save) {
                        DBServices.getInstance().saveResulsGenders_video(Integer.parseInt(challengeID), Integer.parseInt(userID), genders, ts);
                    } else {
                        return false;
                    }
                }
                if (analyse[3] == 1) {
                    // contar emoções
                    boolean save = false;
                    for (ArrayList<Map<String, Double>> emotion : emotions) {
                        if (!emotion.isEmpty()) {
                            save = true;
                        }
                    }
                    if (save) {
                        DBServices.getInstance().saveResulsEmotions_video(Integer.parseInt(challengeID), Integer.parseInt(userID), emotions, ts);
                    } else {
                        return false;
                    }
                }
                DBServices.getInstance().saveFeedback(Integer.parseInt(challengeID), Integer.parseInt(userID), feedback);
                sendEmail(DBServices.getInstance().getUserEmail(userID), DBServices.getInstance().getVoucherURL(challengeID));
                return true;

            } catch (Exception ex) {
                Logger.getLogger(RESTServices.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    @POST
    @Path("login")
    @Produces("text/plain; charset=UTF-8")
    public int putString_login(@HeaderParam("userName") String userName, @HeaderParam("pass") String content) {

        if (userName != null && !userName.isEmpty()) { // TODO: Use this param to check the DB for this challenge, - DONE
            //                           - check if this challenge involves text analysis - DONE
            //                           - check what is meant to be analysed in the text - DONE

            try {
                if (content.isEmpty()) {
                    return -1;
                }
                int usrID = DBServices.getInstance().checkLogin(userName, content);
                if (usrID != -1) {
                    return usrID;
                }
            } catch (Exception e) {
                return -1;
            }

        }
        return -1;
    }

    @POST
    @Path("RegisterRegularUser")
    @Produces("text/plain; charset=UTF-8")
    public int putString_RegisterRegularUser(
            @HeaderParam("userName") String userName,
            @HeaderParam("password") String password,
            @HeaderParam("email") String email,
            @HeaderParam("name") String name,
            @HeaderParam("birthdate") String birthdate, //YYYY-MM-DD
            @HeaderParam("jobSector") String jobSector,
            @HeaderParam("educatLvl") String educatLvl,
            @HeaderParam("gender") String gender,
            @HeaderParam("maritStat") String maritStat,
            @HeaderParam("country") String country,
            @HeaderParam("city") String city,
            @HeaderParam("zipCode") String zipCode,
            @HeaderParam("nrChildr") String nrChildr
    ) {
        Timestamp tstmp = saveTimeStamp();
        return DBServices.getInstance().RegisterRegularUser(false, "3", userName, password, email, name, birthdate, jobSector, educatLvl, gender, maritStat, country, city, zipCode, nrChildr, tstmp.toString().substring(0, tstmp.toString().length() - 2));
    }

    public File decodeBase64(String content, String path, String fileExtension) {

        File file = null;

        try {
            //decoding
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] data = decoder.decodeBuffer(content);
            java.nio.file.Path parentDir = Paths.get(path);
            if (!Files.exists(parentDir)) {
                Files.createDirectories(parentDir);
            }

            //writting to file
            String filePath = path + "/" + nrOfAudioHits + "." + fileExtension;
            try (OutputStream stream = new FileOutputStream(filePath)) {
                stream.write(data);
                stream.close();
            }

            file = new File(filePath);

        } catch (Exception e) {
            System.out.println("Audio file nr " + nrOfAudioHits + " failed decoding process");
        }
        return file;
    }

    public static Timestamp saveTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); //TODO wrong format
        Date now = new Date();
        String strDate = sdfDate.format(now);

        return Timestamp.valueOf(strDate);
    }

    //RETURN EXAMPLE
    //   ';' separates diferent jobs and '#' separates the id and PT and ENG descriptions
    //
    //   jobID#jobDescription_PT#jobDescription_EN;jobID2#jobDescription2_PT#jobDescription2_EN;...
    //   the same applies to the remaining "getAvail" services
    @GET
    @Path("get_JobSectors")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_JobSector() {
        List<String> res = DBServices.getInstance().getAvailJobSectors();
        return comaSeparatedList_toString(res);
    }

    @GET
    @Path("get_educationalLvls")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_educatLvl() {
        List<String> res = DBServices.getInstance().getAvailEducLvl();
        return comaSeparatedList_toString(res);
    }

    @GET
    @Path("get_gender")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_gender() {
        List<String> res = DBServices.getInstance().getAvailGenders();
        return comaSeparatedList_toString(res);
    }

    @GET
    @Path("get_maritalStatus")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_maritStat() {
        List<String> res = DBServices.getInstance().getAvailMaritStat();
        return comaSeparatedList_toString(res);
    }

    @GET
    @Path("get_countries")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_country() {
        List<String> res = DBServices.getInstance().getAvailCountry();
        return comaSeparatedList_toString(res);
    }

    @POST
    @Path("get_cities")
    @Produces("text/plain; charset=UTF-8")
    public String putString_getAvail_city(@HeaderParam("country") String countryName) {
        if (countryName.isEmpty()) {
            return "";
        }
        List<String> res = DBServices.getInstance().getAvailCity(countryName);
        return comaSeparatedList_toString(res);
    }

    @POST
    @Path("get_myChallenges")
    @Produces("text/plain; charset=UTF-8") //if usr is a non registered user pls send a negative integer as usrID
    public String putString_getAvail_challenges(@HeaderParam("usrID") String usrID) {
        List<String> res = null;
        try {
            int id = Integer.parseInt(usrID);
            if (usrID.isEmpty()) {
                return "";
            } else if (id < 0) {
                // NON REGISTERED USER   VVVVV   still TODO
                res = DBServices.getInstance().getUsersAvailChallenges(id);

            } else {
                // REGISTERED USER
                res = DBServices.getInstance().getUsersAvailChallenges(id);
            }
        } catch (Exception e) {
            return "";
        }
        return comaSeparatedList_toString(res);
    }

    private String comaSeparatedList_toString(List<String> res) {
        if (res != null) {

            String ret = "";
            boolean firstEntry = true;

            String separator = ";";
            for (String str : res) {
                if (!firstEntry) {
                    ret += separator;
                }
                ret += str;
                firstEntry = false;
            }

            return ret;
        }
        return "";
    }

    private File fetchRemoteFile2(String location) throws MalformedURLException {
        
        File f = null;
        URL url = new URL(location);
        try {
            f = new File(url.toURI());
        } catch (URISyntaxException | IllegalArgumentException e2) {
            String path = url.getPath().replace("\\", "/");
            path = path.replace("mobilediary", "home/asist/site/www/html");
            f = new File(path);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return f;
    }

    public void sendEmail(String to, String voucherURL) {
        final String username = "camul.isep.2015@gmail.com";
        final String password = "CAMUL20152016";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("camul.isep.2015@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));
            message.setSubject("Thank you/Obrigado");
            message.setText("Thank you for your participation!\nYou can find your voucher in [1]!\n\n\n"
                    + "Obrigado pela sua participação!\nPode encontrar o seu voucher em [1]!\n\n\n"
                    + "[1] - " + "http://10.8.154.245/mobilediary/" + voucherURL);

            Transport.send(message);

        } catch (MessagingException e) {
            System.out.println("Error when sending email...");
        }
    }
}
