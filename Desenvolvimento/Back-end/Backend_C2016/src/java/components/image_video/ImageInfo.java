package components.image_video;

import java.util.ArrayList;
import java.util.Map;

/**
 * This class represents the info that an image/frame can have.
 *
 * @author Alface
 */
public class ImageInfo {

    private int nrOfFaces;

    private ArrayList<Double> ages;
    private ArrayList<String> genders;
    private ArrayList<Map<String, Double>> emotions;

    public ImageInfo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ImageInfo(int nrOfFaces, ArrayList<Double> ages, ArrayList<String> genders, ArrayList<Map<String, Double>> emotions) {
        this.nrOfFaces = nrOfFaces;
        this.ages = ages;
        this.genders = genders;
        this.emotions = emotions;
    }

    public ImageInfo(int nrOfFaces, ArrayList<Double> ages, ArrayList<String> genders) {
        this.nrOfFaces = nrOfFaces;
        this.ages = ages;
        this.genders = genders;
        this.emotions = null;
    }

    public ImageInfo(ArrayList<Map<String, Double>> emotions) {
        this.nrOfFaces = 0;
        this.ages = null;
        this.genders = null;
        this.emotions = emotions;
    }

    public ImageInfo(ImageInfo basic, ImageInfo emotions) {
        this.nrOfFaces = basic.getNrOfFaces();
        this.ages = basic.getAges();
        this.genders = basic.getGenders();
        this.emotions = emotions.getEmotions();
    }

    @Override
    public String toString() {
        return "ImageInfo{" + "nrOfFaces=" + nrOfFaces + ", ages=" + ages + ", genders=" + genders + ", emotions=" + emotions + '}';
    }

    public int getNrOfFaces() {
        return nrOfFaces;
    }

    public ArrayList<Double> getAges() {
        return ages;
    }

    public ArrayList<String> getGenders() {
        return genders;
    }

    public ArrayList<Map<String, Double>> getEmotions() {
        return emotions;
    }
}
