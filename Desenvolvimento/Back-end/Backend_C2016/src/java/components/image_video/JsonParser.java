package components.image_video;

// retrieved from: http://mvnrepository.com/artifact/org.json/json/20160212
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONArray;

/**
 * This class does the parsing of JSON to workable object.
 *
 * @author Alface
 */
public class JsonParser {

    public static ImageInfo parseBasicInfo(String str) {
        try {
            JSONObject obj = new JSONObject(str);
            JSONArray arr = obj.getJSONArray("faces");

            ArrayList<String> genders = new ArrayList<>();
            ArrayList<Double> ages = new ArrayList<>();
            for (int i = 0; i < arr.length(); i++) {

                genders.add(arr.getJSONObject(i).getJSONObject("faceAttributes").getString("gender"));
                ages.add(arr.getJSONObject(i).getJSONObject("faceAttributes").getDouble("age"));
            }

            return new ImageInfo(genders.size(), ages, genders);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public static ImageInfo parseEmotionsInfo(String str) {
        try {
            JSONObject obj = new JSONObject(str);
            JSONArray arr = obj.getJSONArray("faces");

            ArrayList<Map<String, Double>> emotions = new  ArrayList<>();
            for (int i = 0; i < arr.length(); i++) {

                Map<String, Double> emotion = new HashMap<>();
                emotion.put("anger", arr.getJSONObject(i).getJSONObject("scores").getDouble("anger"));
                emotion.put("contempt", arr.getJSONObject(i).getJSONObject("scores").getDouble("contempt"));
                emotion.put("disgust", arr.getJSONObject(i).getJSONObject("scores").getDouble("disgust"));
                emotion.put("fear", arr.getJSONObject(i).getJSONObject("scores").getDouble("fear"));
                emotion.put("happiness", arr.getJSONObject(i).getJSONObject("scores").getDouble("happiness"));
                emotion.put("neutral", arr.getJSONObject(i).getJSONObject("scores").getDouble("neutral"));
                emotion.put("sadness", arr.getJSONObject(i).getJSONObject("scores").getDouble("sadness"));
                emotion.put("surprise", arr.getJSONObject(i).getJSONObject("scores").getDouble("surprise"));
                emotions.add(emotion);
            }

            return new ImageInfo(emotions);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public static String getFaceId(String str) {
        try {
            JSONObject obj = new JSONObject(str);
            JSONArray arr = obj.getJSONArray("faces");

            return arr.getJSONObject(0).get("faceId").toString();

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "";
    }
}
