package components.image_video;

import java.net.URI;

// retrieved from: http://hc.apache.org/downloads.cgi
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * This class does the communication with the Microsoft API, sending the image
 * for processing.
 *
 * @author Alface
 */
public class ImageProcessor {

    public static ImageInfo processImageImgTest(byte[] img) {

        ByteArrayEntity reqEntity = new ByteArrayEntity(img, ContentType.APPLICATION_OCTET_STREAM);

        ImageInfo imgBasicInfo = processImageImgBasic(img);
        ImageInfo imgEmotionsInfo = processImageImgEmotions(img);

        return new ImageInfo(imgBasicInfo, imgEmotionsInfo);
    }

    public static ImageInfo processImageImgBasic(byte[] img) {

        ImageInfo imgBasicInfo = processImageImgData(img, "basic");

        return imgBasicInfo;
    }

    public static ImageInfo processImageImgEmotions(byte[] img) {

        ImageInfo imgEmotionsInfo = processImageImgData(img, "emotions");

        return imgEmotionsInfo;
    }

    private static ImageInfo processImageImgData(byte[] img, String method) {

        HttpClient httpclient = HttpClients.createDefault();

        try {
            String URI = "";
            if (method.equals("basic")) {
                URI = "https://api.projectoxford.ai/face/v1.0/detect";
            } else if (method.equals("emotions")) {
                URI = "https://api.projectoxford.ai/emotion/v1.0/recognize";
            }

            URIBuilder builder = new URIBuilder(URI);

            if (method.equals("basic")) {
                builder.setParameter("returnFaceId", "false");
                builder.setParameter("returnFaceLandmarks", "false");
                builder.setParameter("returnFaceAttributes", "age,gender");
            } else if (method.equals("emotions")) {

            }

            URI uri = builder.build();
            HttpPost request = new HttpPost(uri);
            request.setHeader("Content-Type", "application/octet-stream");
            String key = "";
            if (method.equals("basic")) {
                key = "9a3677060ff149ee9822f7edd88f257b";
            } else if (method.equals("emotions")) {
                key = "c8dcc72c09794812a72e7f808fc46f75";
            }
            request.setHeader("Ocp-Apim-Subscription-Key", key);
            ByteArrayEntity reqEntity = new ByteArrayEntity(img, ContentType.APPLICATION_OCTET_STREAM);
            request.setEntity(reqEntity);

            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null) {
                String str = "{faces:" + EntityUtils.toString(entity) + "}";
                System.out.println(str);
                ImageInfo imgInfo = null;
                if (method.equals("basic")) {
                    imgInfo = JsonParser.parseBasicInfo(str);
                } else if (method.equals("emotions")) {
                    imgInfo = JsonParser.parseEmotionsInfo(str);
                }

                return imgInfo;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return null;
    }
}
