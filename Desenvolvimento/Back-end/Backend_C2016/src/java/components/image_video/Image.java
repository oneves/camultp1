package components.image_video;

/**
 * This class "controls" the action when a image is received by the server.
 *
 * @author Alface
 */
public class Image {

    public static void processImageImgTest(byte[] img) {

        ImageInfo imgInfo = ImageProcessor.processImageImgTest(img);
        if (imgInfo == null) {
            System.out.println("Error when processing image...");
        } else {
            System.out.println(imgInfo);
        }

        // processar imgInfo...
    }

    public static int countFaces(byte[] img) {

        ImageInfo imgInfo = ImageProcessor.processImageImgBasic(img);
        if (imgInfo == null) {
            System.out.println("Error when processing image...");
        } else {
            System.out.println(imgInfo);
        }

        return imgInfo.getNrOfFaces();
    }

    public static ImageInfo basicInfo(byte[] img) {

        ImageInfo imgInfo = ImageProcessor.processImageImgBasic(img);
        if (imgInfo == null) {
            System.out.println("Error when processing image...");
        } else {
            System.out.println(imgInfo);
        }

        return imgInfo;
    }

    public static ImageInfo emotions(byte[] img) {

        ImageInfo imgInfo = ImageProcessor.processImageImgEmotions(img);
        if (imgInfo == null) {
           System.out.println("Error when processing image...");
        } else {
            System.out.println(imgInfo);
        }

        return imgInfo;
    }
}
