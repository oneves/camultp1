package components.audio_text;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javaFlacEncoder.EncodingConfiguration;
import javaFlacEncoder.FLACEncoder;
import javaFlacEncoder.FLACFileOutputStream;
import javaFlacEncoder.FLAC_FileEncoder;
import javaFlacEncoder.StreamConfiguration;
import javax.net.ssl.HttpsURLConnection;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AudioComponent {

    public static final String Afrikaans = "af";
    public static final String Basque = "eu";
    public static final String Bulgarian = "bg";
    public static final String Catalan = "ca";
    public static final String Arabic_Egypt = "ar-EG";
    public static final String Arabic_Jordan = "ar-JO";
    public static final String Arabic_Kuwait = "ar-KW";
    public static final String Arabic_Lebanon = "ar-LB";
    public static final String Arabic_SaudiArabia = "ar-SA";
    public static final String Arabic_Tunisia = "ar-TN";
    public static final String Czech = "cs";
    public static final String Dutch = "nl-NL";
    public static final String English = "en-EN";
    public static final String English_Aus = "en-AU";
    public static final String English_UK = "en-GB";
    public static final String English_US = "en-US";
    public static final String Finnish = "fi";
    public static final String French = "fr-FR";
    public static final String Galician = "gl";
    public static final String German = "de-DE";
    public static final String Hebrew = "he";
    public static final String Hungarian = "hu";
    public static final String Icelandic = "is";
    public static final String Italian = "it-IT";
    public static final String Indonesian = "id";
    public static final String Japanese = "ja";
    public static final String Korean = "ko";
    public static final String Latin = "la";
    public static final String Mandarin_Chinese = "zh-CN";
    public static final String Traditional_Taiwan = "zh-TW";
    public static final String Simplified_China = "zh-CN";
    public static final String Simplified_HongKong = "zh-HK";
    public static final String Malaysian = "ms-MY";
    public static final String Norwegian = "no-NO";
    public static final String Polish = "pl";
    public static final String Pig_Latin = "xx-piglatin";
    public static final String Portuguese = "pt-PT";
    public static final String Portuguese_br = "pt-BR";
    public static final String Romanian = "ro-RO";
    public static final String Russian = "ru";
    public static final String Serbian = "sr-SP";
    public static final String Slovak = "sk";
    public static final String Spanish_Argentina = "es-AR";
    public static final String Spanish_Bolivia = "es-BO";
    public static final String Spanish = "es-ES";
    public static final String Spanish_US = "es-US";
    public static final String Swedish = "sv-SE";
    public static final String Turkish = "tr";
    public static final String Zulu = "zu";

    public static String apiKey = "AIzaSyDh87u335ahytgKRPlOjrdVN7EDbbDjpYE";
    public static String audioFile = System.getProperty("user.dir") + "\\src\\java\\resources\\audio_samples\\linusTest.flac";
    public static String audioFile_wav = System.getProperty("user.dir") + "\\src\\java\\resources\\audio_samples\\wavTest2.wav";
    public static String lang = English;
    public static JSONParser jsonParser = new JSONParser();

    public static List<Integer> countTargetWordsInAudio(List<String> targets, String lang, String audioFile_flac) throws Exception {
        if (targets.size() > 0 && lang.length() == 5 && audioFile_flac.endsWith(".flac")) {
            return countTargetWords(targets, speechToText(lang, audioFile_flac));
        }
        return null;
    }

    public static List<Integer> countTargetWords(List<String> target, String str) {
        List<Integer> targetWordCount = new ArrayList<Integer>();
        String str_lowerCase = str.toLowerCase();
        for (int i = 0; i < target.size(); i++) {
            targetWordCount.add(0);
            if (str_lowerCase.contains(target.get(i).toLowerCase())) {

                Pattern p = Pattern.compile(target.get(i).toLowerCase());
                Matcher m = p.matcher(str_lowerCase);
                while (m.find()) {
                    targetWordCount.set(i, targetWordCount.get(i) + 1);
                }

            }
        }

        return targetWordCount;
    }

    public static String speechToText(String lang, String audioFile_flac) throws Exception {
        String USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2",
                url = "https://www.google.com/speech-api/v2/recognize?output=json&lang=" + lang + "&key=" + apiKey + "&client=chromium&maxresults=6&pfilter=2";

        URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "audio/x-flac; rate=8000");
        //con.setRequestProperty("Content-Type", "audio/l16; rate=16000");
        con.setRequestProperty("AcceptEncoding", "gzip,deflate,sdch");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.write(Files.readAllBytes(Paths.get(audioFile_flac)));
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        //System.out.println("\nSending 'POST' request to URL : " + url);
        //System.out.println("Response Code : " + responseCode);

        if (responseCode == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            try {
                String cutFirstResult = response.toString().substring(13, response.toString().length());
                JSONObject jsonObj = (JSONObject) jsonParser.parse(cutFirstResult);
                JSONArray jsonObj2 = (JSONArray) jsonObj.get("result");
                JSONObject jsonObj3 = (JSONObject) jsonObj2.get(0);
                JSONArray jsonObj4 = (JSONArray) jsonObj3.get("alternative");
                JSONObject jsonObj5 = (JSONObject) jsonObj4.get(0);
                String retrn = jsonObj5.values().toArray()[0].toString();

                System.out.println("Google Speech: "+retrn);
                return retrn;

            } catch (Exception e) {
                System.out.println("No Response from Google Speech");
                return "";
            }

        }

        return "";
    }

    //DEPRECATED - PLEASE USE TRANSCODER CLASS
    private static void wavToFlacEncoder(File inputFile, File outputFile) {

        StreamConfiguration streamConfiguration = new StreamConfiguration();
        streamConfiguration.setSampleRate(8000);
        streamConfiguration.setBitsPerSample(16);
        streamConfiguration.setChannelCount(1);

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(inputFile);
            AudioFormat format = audioInputStream.getFormat();

            int frameSize = format.getFrameSize();

            FLACEncoder flacEncoder = new FLACEncoder();
            
            FLACFileOutputStream flacOutputStream = new FLACFileOutputStream(outputFile);

            flacEncoder.setStreamConfiguration(streamConfiguration);
            flacEncoder.setOutputStream(flacOutputStream);

            flacEncoder.openFLACStream();

            int frameLength = (int) audioInputStream.getFrameLength();
            if (frameLength <= AudioSystem.NOT_SPECIFIED) {
                frameLength = 16384;//Arbitrary file size
            }
            int[] sampleData = new int[frameLength];
            byte[] samplesIn = new byte[frameSize];

            int i = 0;

            while (audioInputStream.read(samplesIn, 0, frameSize) != -1) {
                if (frameSize != 1) {
                    ByteBuffer bb = ByteBuffer.wrap(samplesIn);
                    bb.order(ByteOrder.LITTLE_ENDIAN);
                    short shortVal = bb.getShort();
                    sampleData[i] = shortVal;
                } else {
                    sampleData[i] = samplesIn[0];
                }

                i++;
            }

            sampleData = truncateNullData(sampleData, i);

            flacEncoder.addSamples(sampleData, i);
            flacEncoder.encodeSamples(i, false);
            flacEncoder.encodeSamples(flacEncoder.samplesAvailableToEncode(), true);

            audioInputStream.close();
            flacOutputStream.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static int[] truncateNullData(int[] sampleData, int index) {
        if (index == sampleData.length) {
            return sampleData;
        }
        int[] out = new int[index];
        for (int i = 0; i < index; i++) {
            out[i] = sampleData[i];
        }
        return out;
    }

    private static File wavToFlacEncoder(File wavFile) {
        File flacFile = new File(wavFile.getAbsolutePath().replace(".wav", ".flac"));
        wavToFlacEncoder(wavFile, flacFile);
        return flacFile;
    }

}
