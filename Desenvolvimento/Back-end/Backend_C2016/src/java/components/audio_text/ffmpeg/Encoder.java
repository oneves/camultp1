/*
 * JAVE - A Java Audio/Video Encoder (based on FFMPEG)
 * 
 * Copyright (C) 2008-2009 Carlo Pelliccia (www.sauronsoftware.it)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package components.audio_text.ffmpeg;

import components.audio_text.Attributes;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Main class of the package. Instances can encode audio and video streams.
 *
 * @author Carlo Pelliccia
 * @author Joren Six
 * @PersonWhoCompletlyF**kedThisClass Nuno
 */
public class Encoder {

    //private static final Logger LOG = Logger.getLogger(Encoder.class.getName());
    private static ArrayList<FFMPEGLocator> locators = new ArrayList<FFMPEGLocator>();

    public static void addFFMPEGLocator(FFMPEGLocator locator) {
        locators.add(locator);
    }

    public static boolean hasLocators() {
        return locators.size() > 0;
    }

    private static final Pattern BIT_RATE_PATTERN = Pattern.compile("(\\d+)\\s+kb/s",
            Pattern.CASE_INSENSITIVE);

    private static final Pattern SAMPLING_RATE_PATTERN = Pattern.compile("(\\d+)\\s+Hz",
            Pattern.CASE_INSENSITIVE);

    private static final Pattern CHANNELS_PATTERN = Pattern.compile("(mono|stereo|.*(\\d+).*channels)",
            Pattern.CASE_INSENSITIVE);

    private FFMPEGLocator locator;

    public Encoder() {
        for (FFMPEGLocator loc : locators) {
            if (loc.pickMe()) {
                this.locator = loc;
            }
        }
        if (this.locator == null) {
            throw new Error("Could not find an ffmpeg locator for this operating system.");
        }
    }

    public void encode(File source, File target, Attributes attributes) throws EncoderException {
        if (attributes == null) {
            throw new IllegalArgumentException("Audio attributes are null");
        }

        target = target.getAbsoluteFile();
        target.getParentFile().mkdirs();
        FFMPEGExecutor ffmpeg = construcExecutor(attributes, source.getAbsolutePath());

        //add output file
        ffmpeg.addArgument("-y");
        //ffmpeg.addArgument("-c:a");
        ffmpeg.addArgument(target.getAbsolutePath());
        //ffmpeg.addFileArgument(target.getAbsolutePath());

        String out = "";
        try {
            out = ffmpeg.execute();
        } catch (IOException ex) {
            System.out.println(out+"\n\n\n\n\n");
            Logger.getLogger(Encoder.class.getName()).log(Level.SEVERE, null, out+"\n\n\n"+ex);
        }

        if (target.length() == 0) {
            System.out.println(out+"\n\n\n\n\n");
            throw new EncoderException(String.format(
                    "The size of the target (%s) is zero bytes, something went wrong.",
                    target.getAbsolutePath()));
        }
    }

    private FFMPEGExecutor construcExecutor(Attributes attributes, String source) {
        FFMPEGExecutor ffmpeg = locator.createExecutor();

        ffmpeg.addArgument("-i");
        ffmpeg.addArgument(source);

        // no video
        ffmpeg.addArgument("-vn");

        Integer channels = attributes.getChannels();
        if (channels != null) {
            ffmpeg.addArgument("-ac");
            ffmpeg.addArgument(String.valueOf(channels.intValue()));
        }
        Integer samplingRate = attributes.getSamplingRate();
        if (samplingRate != null) {
            ffmpeg.addArgument("-ar");
            ffmpeg.addArgument(String.valueOf(samplingRate.intValue()));
        }
        Integer volume = attributes.getVolume();
        if (volume != null) {
            ffmpeg.addArgument("-vol");
            ffmpeg.addArgument(String.valueOf(volume.intValue()));
        }

        //ffmpeg.addArgument("-sample_fmt");
        //ffmpeg.addArgument("s16");
        ffmpeg.addArgument("-ab");
        ffmpeg.addArgument("16");

        return ffmpeg;
    }

}
