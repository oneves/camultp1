package components.audio_text;

import java.io.File;


import components.audio_text.ffmpeg.Encoder;
import components.audio_text.ffmpeg.EncoderException;
import components.audio_text.ffmpeg.LinuxFFMPEGLocator;
import components.audio_text.ffmpeg.MacFFMPEGLocator;
import components.audio_text.ffmpeg.PathFFMPEGLocator;
import components.audio_text.ffmpeg.WindowsFFMPEGLocator;

/**
 * The main interface to transcode audio.
 * 
 * @author Joren Six
 */
public class Transcoder {

	//private static final Logger LOG = Logger.getLogger(Transcoder.class.getName());


	/**
	 * Adds default locators to encoder.
	 */
	private static void initialize() {
		if(!Encoder.hasLocators()) {
			Encoder.addFFMPEGLocator(new WindowsFFMPEGLocator());
			Encoder.addFFMPEGLocator(new MacFFMPEGLocator());
			Encoder.addFFMPEGLocator(new LinuxFFMPEGLocator());
			Encoder.addFFMPEGLocator(new PathFFMPEGLocator());
		}
	}

	
	private Transcoder() {

	}

	
	public static void transcode(final String source, final String target, final Attributes targetEncoding)
			throws EncoderException {
		transcode(new File(source), new File(target), targetEncoding);
	}

	
	public static void transcode(final String source, final String target,
			final DefaultAttributes targetEncoding) throws EncoderException {
		transcode(source, target, targetEncoding.getAttributes());
	}

	
	public static void transcode(final File source, final File target, final DefaultAttributes targetEncoding)
			throws EncoderException {
		transcode(source, target, targetEncoding.getAttributes());
	}

	
	public static void transcode(final File source, final File target, final Attributes targetEncoding)
			throws EncoderException {
		// sanity checks
		if (!source.exists()) {
			throw new IllegalArgumentException(source + " does not exist. It should"
					+ " be a readable audiofile.");
		}
		if (source.isDirectory()) {
			throw new IllegalArgumentException(source + " is a directory. It should "
					+ "be a readable audiofile.");
		}
		if (!source.canRead()) {
			throw new IllegalArgumentException(source
					+ " can not be read, check file permissions. It should be a readable audiofile.");
		}

		initialize();
		// encode the source to directory
		final Encoder e = new Encoder();
		//LOG.info("Try to transcode " + source + " to " + target);
		e.encode(source, target, targetEncoding);
		//LOG.info("Successfully transcoded " + source + " to " + target);
	}

}
