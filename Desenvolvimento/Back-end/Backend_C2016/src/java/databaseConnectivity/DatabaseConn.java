/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databaseConnectivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseConn {

    private static DatabaseConn instance;

    public static DatabaseConn getInstance() throws ClassNotFoundException {
        if (instance == null) {
            instance = new DatabaseConn();
        }
        return instance;
    }

    //static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/camul?";
    //static final String DB_URL_altPort = "jdbc:mysql://127.0.0.1:8080/camul?";
    static final String DB_URL = "jdbc:mysql://127.0.0.1:3306/camul?autoReconnect=true&useSSL=false&";
    static final String DB_URL_altPort = "jdbc:mysql://127.0.0.1:8080/camul?autoReconnect=true&useSSL=false&";

    static final String USER = "root";
    static final String PASS = "Asist2015";

    private static Connection conn = null;

    private DatabaseConn() {

        conn = null;

        boolean failedFirstTry = false;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(DB_URL + "user=" + USER + "&password=" + PASS);
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException ex) {
            System.out.println("Findme");
            System.out.println(ex.getMessage());
            failedFirstTry = true;
        }

        if (failedFirstTry) {
            try {
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                conn = DriverManager.getConnection(DB_URL_altPort + "user=" + USER + "&password=" + PASS);
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException ex) {
                System.out.println("Findme");
                System.out.println(ex.getMessage());
            }
        }

        if (conn == null) {
            System.out.println("Conn is null");
        } else {
            System.out.println("Conn is NOT null");
        }
    }

    public ResultSet dbQuery(String sql) throws SQLException {
        Statement stmt = conn.createStatement();
        return stmt.executeQuery(sql);
    }

    //uses '?' in place of the actual params in the sql string
    public ResultSet dbSafeQuery(String sql, List<String> param) {
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            int i = 1;
            for (String str : param) {
                stmt.setString(i, str);
                i++;
            }

            return stmt.executeQuery();

        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConn.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int dbSafeQuery_dataManipulation(String sql, List<String> param) {
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            int i = 1;
            for (String str : param) {
                stmt.setString(i, str);
                i++;
            }

            return stmt.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Safe query error - " + ex.getMessage());
            return -1;
        }
    }

    public int dbQuery_dataManipulation(String sql) throws SQLException {
        Statement stmt = conn.createStatement();
        return stmt.executeUpdate(sql);
    }
}