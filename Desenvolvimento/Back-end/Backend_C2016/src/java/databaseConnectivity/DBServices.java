package databaseConnectivity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nuno
 */
public class DBServices {

    private static DBServices instance;

    private DBServices() {
        init();
    }

    public static DBServices getInstance() {
        if (instance == null) {
            instance = new DBServices();
        }
        return instance;
    }

    private void init() {
        buildMediaTypeMap();
    }

    //STRING IN ENG FOLLOWED BY ITS DB VALUE
    public Map<String, Integer> DB_MEDIATYPES;
    public Map<String, Integer> DB_MEDIAMETADATA;

    public int[] checkWhatToAnalyse(int chID, int usrID, int mediaTypeRequested) throws SQLException {
        // 0-nr cara 1-idades 2-generos 3-emocoes
        int[] analyse = {0, 0, 0, 0};

        ResultSet res = null;

        String sql = "SELECT * FROM `TChallengeResultsConfig` WHERE `CRCf_XK_IdChallenge`=" + chID + " AND `CRCf_XK_IdMediaType`=" + mediaTypeRequested;
        try {
            res = DatabaseConn.getInstance().dbQuery(sql);
            while (res.next()) {
                int anal = res.getInt(3);

                if (anal == DB_MEDIAMETADATA.get("Nr faces")) {
                    analyse[0] = 1;
                }
                if (anal == DB_MEDIAMETADATA.get("Count age")) {
                    analyse[1] = 1;
                }
                if (anal == DB_MEDIAMETADATA.get("Count gender")) {
                    analyse[2] = 1;
                }
                if (anal == DB_MEDIAMETADATA.get("Count emotions")) {
                    analyse[3] = 1;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBServices.class.getName()).log(Level.SEVERE, null, ex);
        }

        // ver na base de dados...
        return analyse;
    }

    public boolean checkIfConditionsMatch(int chID, int usrID, int mediaTypeRequested) throws SQLException {
        ResultSet res = null;
        try {

            //String sql = "SELECT * FROM `tchallengeinvites` WHERE `CInv_XK_IdChallenge`=" + chID + " AND `CInv_XK_IdUser`=" + usrID;
            String sql = "SELECT * FROM `VUsersChallenges` WHERE `Chal_PK_IdChallenge`=" + chID + " AND (`User_PK_IdUser`=" + usrID + " OR `Chal_AllowGuests`=1)";
            res = DatabaseConn.getInstance().dbQuery(sql);
            if (!res.next()) {
                res.close();
                return false;
            }
            res.close();

            return checkIfChalengeHasMediaType(chID, mediaTypeRequested);

        } catch (ClassNotFoundException notFoundEx) {
            return false;
        } catch (SQLException SQLEx) {
            return false;
        } finally {
            if (res != null) {
                res.close();
            }
        }
    }

    private boolean checkIfChalengeHasMediaType(int chID, int mediaTypeRequested) throws SQLException {

        ResultSet res = null;
        try {
            String sql = "SELECT * FROM `TChallengeResultsConfig` WHERE `CRCf_XK_IdChallenge`=" + chID + " AND `CRCf_XK_IdMediaType`=" + mediaTypeRequested;
            res = DatabaseConn.getInstance().dbQuery(sql);
            if (!res.next()) {
                res.close();
                return false;
            }
            res.close();

            return true;

        } catch (ClassNotFoundException notFoundEx) {
            return false;
        } catch (SQLException SQLEx) {
            return false;
        } finally {
            if (res != null) {
                res.close();
            }
        }
    }

    public void buildMediaTypeMap() {
        DB_MEDIATYPES = new HashMap<String, Integer>();
        try {
            String sql = "SELECT * FROM `TTMedia`";
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);

            while (rs.next()) {
                DB_MEDIATYPES.put(rs.getString(3), rs.getInt(1));
            }

        } catch (ClassNotFoundException notFoundEx) {
        } catch (SQLException SQLEx) {
        }

        DB_MEDIAMETADATA = new HashMap<String, Integer>();
        DB_MEDIAMETADATA.put("Count age", 1);
        DB_MEDIAMETADATA.put("Count emotions", 3);
        DB_MEDIAMETADATA.put("Count gender", 4);
        DB_MEDIAMETADATA.put("Nr faces", 5);
    }

    /*
     mediatype can only be Audio or Text
     */
    public List<String> getTargetWordsByChalengeID(int chalID, int mediaType) {
        if (mediaType != DB_MEDIATYPES.get("Audio") && mediaType != DB_MEDIATYPES.get("Text")) {
            return null;
        }

        List<String> targets = new ArrayList();
        try {
            String sql = "SELECT * FROM `TChallengeResultsConfig` WHERE `CRCf_XK_IdChallenge`=" + chalID + " and `CRCf_XK_IdMediaType`=" + mediaType;
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);

            while (rs.next()) {
                String trgts = rs.getString(4);
                targets = new ArrayList<String>(Arrays.asList(trgts.split(";")));
                if (!targets.isEmpty()) {
                    break;
                }
            }

        } catch (ClassNotFoundException notFoundEx) {
        } catch (SQLException SQLEx) {
        }

        return targets;
    }

    //returns new guest ID
    private int insertNewGuest(Timestamp time) {
        int ret = RegisterRegularUser(true, "3", "guest", "guestPass123", "guest@guest.gue", "guest", "1111-11-11", "Vendas", "Educação primária", "Outro", "Single", "Portugal", "Porto", "0000-00", "-1", time.toString());
        if (ret < 0) {
            System.out.println("Error inserting new Guest!");
            return 0;
        }
        return -(ret - 100);
    }

    //returns 0 in case something went wrong
    private int getNewGuestID() {
        String sql = "SELECT MIN(`User_PK_IdUser`) FROM `TUsers`";
        try {
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);

            while (rs.next()) {
                if (rs.getInt(1) < 0) {
                    return rs.getInt(1) - 1;
                }
                return -1;
            }

        } catch (ClassNotFoundException notFoundEx) {
            System.out.println(notFoundEx + "      new guest could not be found");
        } catch (SQLException SQLEx) {
            System.out.println(SQLEx + "      new guest could not be found");
        }
        return 0;
    }

    public boolean saveResuls_text(int chID, int usrID, List<Integer> results, Timestamp time) {
        if (usrID < 0) {
            usrID = insertNewGuest(time);
        }

        int countRes = 0;
        for (Integer i : results) {
            countRes += i;
        }
        if (countRes == 0) {
            return false;
        }

        try {
            String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES (" + chID + "," + DB_MEDIATYPES.get("Text") + "," + 2 + "," + 13 + "," + usrID + "," + countRes + ",'" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql); // 4 2 13

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return true;
    }

    public boolean saveResuls_sound(int chID, int usrID, List<Integer> results, Timestamp time) {
        if (usrID < 0) {
            usrID = insertNewGuest(time);
        }

        int countRes = 0;
        for (Integer i : results) {
            countRes += i;
        }
        if (countRes == 0) {
            return false;
        }

        try {
            String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Audio") + "','" + 2 + "','" + 13 + "','" + usrID + "','" + countRes + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql); // 4 2 13

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
            return false;
        }
        return true;
    }

    public void saveResulsEmotions_image(int chID, int usrID, ArrayList<Map<String, Double>> emotions, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int disgust = 0, sadness = 0, contempt = 0, anger = 0, happiness = 0, neutral = 0, surprise = 0, fear = 0;
            for (Map<String, Double> emotion : emotions) {
                String emot = getBiggestEmotion(emotion);
                if (emot.equals("disgust")) {
                    disgust++;
                } else if (emot.equals("sadness")) {
                    sadness++;
                } else if (emot.equals("contempt")) {
                    contempt++;
                } else if (emot.equals("anger")) {
                    anger++;
                } else if (emot.equals("happiness")) {
                    happiness++;
                } else if (emot.equals("neutral")) {
                    neutral++;
                } else if (emot.equals("surprise")) {
                    surprise++;
                } else if (emot.equals("fear")) {
                    fear++;
                }
            }

            if (disgust > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 3 + "','" + usrID + "','" + disgust + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (sadness > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 7 + "','" + usrID + "','" + sadness + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (contempt > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 2 + "','" + usrID + "','" + contempt + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (anger > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 1 + "','" + usrID + "','" + anger + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (happiness > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 5 + "','" + usrID + "','" + happiness + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (neutral > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 6 + "','" + usrID + "','" + neutral + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (surprise > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 8 + "','" + usrID + "','" + surprise + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (fear > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 4 + "','" + usrID + "','" + fear + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    private String getBiggestEmotion(Map<String, Double> emotion) {
        double value = 0;
        String emot = "";
        for (Map.Entry<String, Double> entry : emotion.entrySet()) {
            if (value < entry.getValue()) {
                value = entry.getValue();
                emot = entry.getKey();
            }
        }
        return emot;
    }

    public void saveResulsGenders_image(int chID, int usrID, ArrayList<String> genders, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int male = 0, female = 0;
            for (String gender : genders) {
                if (gender.equals("male")) {
                    male++;
                } else if (gender.equals("female")) {
                    female++;
                }
            }

            String sql1 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count gender") + "','" + 14 + "','" + usrID + "','" + male + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql1);
            String sql2 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count gender") + "','" + 15 + "','" + usrID + "','" + female + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql2);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsAges_image(int chID, int usrID, ArrayList<Double> ages, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int lvl1 = 0, lvl2 = 0, lvl3 = 0, lvl4 = 0;
            for (Double age : ages) {
                if (age <= 18) {
                    lvl1++;
                } else if (age > 18 && age <= 40) {
                    lvl2++;
                } else if (age > 40 && age <= 65) {
                    lvl3++;
                } else if (age > 65) {
                    lvl4++;
                }
            }

            String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 9 + "','" + usrID + "','" + lvl1 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 10 + "','" + usrID + "','" + lvl2 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 11 + "','" + usrID + "','" + lvl3 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 12 + "','" + usrID + "','" + lvl4 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsNrFaces_image(int chID, int usrID, ArrayList<String> genders, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int nr = genders.size();

            String sql1 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Image") + "','" + DB_MEDIAMETADATA.get("Nr faces") + "','" + 16 + "','" + usrID + "','" + nr + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql1);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsNrFaces_video(int chID, int usrID, ArrayList<Integer> faces, Timestamp time) {
        int nr = 0;
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            for (int i = 0; i < faces.size(); i++) {
                if (nr < faces.get(i)) {
                    nr = faces.get(i);
                }
            }

            String sql1 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Nr faces") + "','" + 16 + "','" + usrID + "','" + nr + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql1);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsAges_video(int chID, int usrID, ArrayList<ArrayList<Double>> ages, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int mlvl1 = 0, mlvl2 = 0, mlvl3 = 0, mlvl4 = 0;
            for (int i = 0; i < ages.size(); i++) {
                int lvl1 = 0, lvl2 = 0, lvl3 = 0, lvl4 = 0;
                for (Double age : ages.get(i)) {
                    if (age <= 18) {
                        lvl1++;
                    } else if (age > 18 && age <= 40) {
                        lvl2++;
                    } else if (age > 40 && age <= 65) {
                        lvl3++;
                    } else if (age > 65) {
                        lvl4++;
                    }
                }
                if (mlvl1 < lvl1) {
                    mlvl1 = lvl1;
                }
                if (mlvl2 < lvl2) {
                    mlvl2 = lvl2;
                }
                if (mlvl3 < lvl3) {
                    mlvl3 = lvl3;
                }
                if (mlvl4 < lvl4) {
                    mlvl4 = lvl4;
                }
            }

            String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 9 + "','" + usrID + "','" + mlvl1 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 10 + "','" + usrID + "','" + mlvl2 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 11 + "','" + usrID + "','" + mlvl3 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count age") + "','" + 12 + "','" + usrID + "','" + mlvl4 + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsGenders_video(int chID, int usrID, ArrayList<ArrayList<String>> genders, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int mmale = 0, mfemale = 0;
            for (int i = 0; i < genders.size(); i++) {
                int male = 0, female = 0;
                for (String gender : genders.get(i)) {
                    if (gender.equals("male")) {
                        male++;
                    } else if (gender.equals("female")) {
                        female++;
                    }
                }
                if (mmale < male) {
                    mmale = male;
                }
                if (mfemale < female) {
                    mfemale = female;
                }
            }

            String sql1 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count gender") + "','" + 14 + "','" + usrID + "','" + mmale + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql1);
            String sql2 = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                    + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count gender") + "','" + 15 + "','" + usrID + "','" + mfemale + "','" + time + "')";
            DatabaseConn.getInstance().dbQuery_dataManipulation(sql2);

        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public void saveResulsEmotions_video(int chID, int usrID, ArrayList<ArrayList<Map<String, Double>>> emotions, Timestamp time) {
        try {
            if (usrID < 0) {
                usrID = insertNewGuest(time);
            }

            int mdisgust = 0, msadness = 0, mcontempt = 0, manger = 0, mhappiness = 0, mneutral = 0, msurprise = 0, mfear = 0;

            for (int i = 0; i < emotions.size(); i++) {
                int disgust = 0, sadness = 0, contempt = 0, anger = 0, happiness = 0, neutral = 0, surprise = 0, fear = 0;
                for (Map<String, Double> emotion : emotions.get(i)) {
                    String emot = getBiggestEmotion(emotion);
                    if (emot.equals("disgust")) {
                        disgust++;
                    } else if (emot.equals("sadness")) {
                        sadness++;
                    } else if (emot.equals("contempt")) {
                        contempt++;
                    } else if (emot.equals("anger")) {
                        anger++;
                    } else if (emot.equals("happiness")) {
                        happiness++;
                    } else if (emot.equals("neutral")) {
                        neutral++;
                    } else if (emot.equals("surprise")) {
                        surprise++;
                    } else if (emot.equals("fear")) {
                        fear++;
                    }
                }
                if (mdisgust < disgust) {
                    mdisgust = disgust;
                }
                if (msadness < sadness) {
                    msadness = sadness;
                }
                if (mcontempt < contempt) {
                    mcontempt = contempt;
                }
                if (manger < anger) {
                    manger = anger;
                }
                if (mhappiness < happiness) {
                    mhappiness = happiness;
                }
                if (mneutral < neutral) {
                    mneutral = neutral;
                }
                if (msurprise < surprise) {
                    msurprise = surprise;
                }
                if (mfear < fear) {
                    mfear = fear;
                }
            }

            if (mdisgust > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 3 + "','" + usrID + "','" + mdisgust + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (msadness > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 7 + "','" + usrID + "','" + msadness + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (mcontempt > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 2 + "','" + usrID + "','" + mcontempt + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (manger > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 1 + "','" + usrID + "','" + manger + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (mhappiness > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 5 + "','" + usrID + "','" + mhappiness + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (mneutral > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 6 + "','" + usrID + "','" + mneutral + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (msurprise > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 8 + "','" + usrID + "','" + msurprise + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
            if (mfear > 0) {
                String sql = "INSERT INTO `TChallengeResults`(`CRCf_XK_IdChallenge`, `CRCf_XK_IdMediaType`, `CRCf_XK_IdMediaMetaData`, `CRCf_XK_IdMediaMetaDataDetails`, `CRCf_XK_IdUser`, `CRCf_Count`, `CRCf_TimeStamp`) "
                        + "VALUES ('" + chID + "','" + DB_MEDIATYPES.get("Video") + "','" + DB_MEDIAMETADATA.get("Count emotions") + "','" + 4 + "','" + usrID + "','" + mfear + "','" + time + "')";
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
    }

    public int checkLogin(String userName, String content) {
        try {
            String sql = "SELECT `User_PK_IdUser` FROM `TUsers` WHERE `User_UserName`=? and `User_Password`=?";
            List<String> param = new ArrayList();
            param.add(userName);
            param.add(content);
            ResultSet rs = DatabaseConn.getInstance().dbSafeQuery(sql, param);

            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (ClassNotFoundException notFoundEx) {
        } catch (SQLException SQLEx) {
        }
        return -1;
    }

    //RETURNS 1 if registration was successful
    //ERROR CODES///////////////////
    // -1  : UserName already in use
    // -2  : jobSector not available/failed to connect to DB
    // -3  : educatLvl not available
    // -4  : gender    not available
    // -5  : maritStat not available
    // -6  : country   not available
    // -7  : city      not available
    // -8  : SQL insert has failed...?
    public int RegisterRegularUser(boolean isGuest, String userType, String userName, String password, String email, String name, String birthdate, String jobSector, String educatLvl, String gender, String maritStat, String country, String city, String zipCode, String nrChildr, String timeStamp) {
        try {
            int guestID = 0;
            if (isGuest) {
                guestID = getNewGuestID();
                if (guestID == 0) {
                    return -8;
                }
            }

            String usrSql = "SELECT * FROM `TUsers` WHERE `User_UserName`=?";
            List<String> aux = new ArrayList();
            aux.add(userName);
            ResultSet rs = DatabaseConn.getInstance().dbSafeQuery(usrSql, aux);
            if (rs.next()) {
                return -1;   //userName already in use!
            }

            String sql = "";
            List<String> param = new ArrayList();
            if (isGuest) {
                sql = "INSERT INTO `TUsers`(`User_PK_IdUser`, `User_FK_IdTypeUser`, `User_Name`, "
                        + "`User_Email`, `User_UserName`, `User_Password`, `User_DateOfBirth`, `User_FK_IdJobSector`, "
                        + "`User_FK_IdEducationLevel`, `User_FK_IdGender`, `User_IdMaritalStatus`, `User_FK_IdCountry`, "
                        + "`User_FK_IdCity`, `User_ZipCode`, `User_NumOfChildren`, `User_InsertedOn`, `User_TotalPoints`)"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                param.add("" + guestID);
            } else {
                sql = "INSERT INTO `TUsers`(`User_FK_IdTypeUser`, `User_Name`, "
                        + "`User_Email`, `User_UserName`, `User_Password`, `User_DateOfBirth`, `User_FK_IdJobSector`, "
                        + "`User_FK_IdEducationLevel`, `User_FK_IdGender`, `User_IdMaritalStatus`, `User_FK_IdCountry`, "
                        + "`User_FK_IdCity`, `User_ZipCode`, `User_NumOfChildren`, `User_InsertedOn`, `User_TotalPoints`)"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            }

            param.add(userType);
            param.add(name);
            param.add(email);
            if (isGuest) {
                param.add(userName + guestID);
            } else {
                param.add(userName);
            }
            param.add(password);
            param.add(birthdate);

            List<Integer> IDs = getAvailIDsFromDescriptions(jobSector, educatLvl, gender, maritStat, country, city);
            int errorIDCounter = -2;
            for (Integer id : IDs) {
                if (id < 0) { //means description not found!
                    return errorIDCounter;
                }
                errorIDCounter--;
            }
            param.add("" + IDs.get(0));
            param.add("" + IDs.get(1));
            param.add("" + IDs.get(2));
            param.add("" + IDs.get(3));
            param.add("" + IDs.get(4));
            param.add("" + IDs.get(5));

            param.add(zipCode);
            param.add(nrChildr);
            param.add(timeStamp);
            param.add("0");   //<<<<<< total of points = 0

            if (DatabaseConn.getInstance().dbSafeQuery_dataManipulation(sql, param) > 0) {
                if (isGuest) {
                    return -(guestID - 100);
                }
                return 1;
            }

        } catch (ClassNotFoundException | SQLException Ex) {
            System.out.println("Register error - " + Ex.getMessage());
        }
        return -8;
    }

    public void saveFeedback(int chID, int usrID, String feedback) {
        try {
            String sql = "INSERT INTO `TChallengeUserFeedback`(`CUFd_XK_IdChallenge`, `CUFd_XK_IdUser`, `CUFd_Feedback`) "
                    + "VALUES ('" + chID + "','" + usrID + "','" + feedback + "')";
            try {
                DatabaseConn.getInstance().dbQuery_dataManipulation(sql);
            } catch (SQLException ex) {
                Logger.getLogger(DBServices.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DBServices.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private List<Integer> getAvailIDsFromDescriptions(String jobSector, String educatLvl, String gender, String maritStat, String country, String city) {
        List<Integer> results = new ArrayList();

        List<String> job = getAvailJobSectors();
        List<String> edu = getAvailEducLvl();
        List<String> gen = getAvailGenders();
        List<String> mar = getAvailMaritStat();
        List<String> cou = getAvailCountry();

        results.add(getAvailIDfromSingleDescrip(job, jobSector));
        results.add(getAvailIDfromSingleDescrip(edu, educatLvl));
        results.add(getAvailIDfromSingleDescrip(gen, gender));
        results.add(getAvailIDfromSingleDescrip(mar, maritStat));
        results.add(getAvailIDfromSingleDescrip(cou, country));

        List<String> cit = getAvailCity(results.get(4));
        results.add(getAvailIDfromSingleDescrip(cit, city));

        return results;
    }

    private int getAvailIDfromSingleDescrip(List<String> l, String descript) {
        for (String str : l) {
            String stripedStr = stripAccents(str);
            if (stripedStr.contains(stripAccents(descript))) {
                try {
                    String[] splForID = str.split("#");
                    int id = Integer.parseInt(splForID[0]);
                    return id;
                } catch (Exception e) {
                    return -1;
                }
            }
        }
        return -1;
    }

    // returns String in this style:  "ID#Description_PT#Description_EN" separator is '#'
    private List<String> getAvailIDInfo(String sql) {
        List<String> jobsList = new ArrayList();

        try {
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);

            while (rs.next()) {
                jobsList.add(rs.getInt(1) + "#" + rs.getString(2) + "#" + rs.getString(3));
            }

        } catch (ClassNotFoundException notFoundEx) {
        } catch (SQLException SQLEx) {
        }

        return jobsList;
    }

    // returns String in this style:  "eduID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailEducLvl() {
        return getAvailIDInfo("SELECT * FROM `TEducationLevel`");
    }

    // returns String in this style:  "jobID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailJobSectors() {
        return getAvailIDInfo("SELECT * FROM `TJobSectors`");
    }

    // returns String in this style:  "genID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailGenders() {
        return getAvailIDInfo("SELECT * FROM `TGender`");
    }

    // returns String in this style:  "maritID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailMaritStat() {
        return getAvailIDInfo("SELECT * FROM `TMaritalStatus`");
    }

    // returns String in this style:  "countrID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailCountry() {
        return getAvailIDInfo("SELECT * FROM `TCountry`");
    }

    // returns String in this style:  "cityID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailCity(int countryID) {
        return getAvailIDInfo("SELECT `City_PK_IdCity`, `City_DescCity`, `City_DescCity_ENG` FROM `TCity` WHERE `City_FK_IdCountry`='" + countryID + "' ORDER BY `City_DescCity`");
    }

    // returns String in this style:  "cityID#Description_PT#Description_EN" separator is '#'
    public List<String> getAvailCity(String countryName) {
        int id = getAvailIDfromSingleDescrip(getAvailCountry(), countryName);
        return getAvailCity(id);
    }

    public List<String> getAvailAgeRange() {
        return getAvailIDInfo("SELECT `ARan_DescAgeRange`, `ARan_MinRange`, `ARan_MaxRange` FROM `TAgeRange`");
    }

    public List<String> getUsersAvailChallenges(int usrID) {

        List<String> results = new ArrayList();
        try {
            //String sql = "SELECT * FROM `VUsersChallenges` WHERE `User_PK_IdUser`=" + usrID;
            //String sql = "SELECT DISTINCT `Chal_PK_IdChallenge`, `Chal_Title`, `Chal_Title_ENG`, `Chal_Description`, `Chal_Description_ENG`, `Chal_BeginDate`, `Chal_EndDate`, `Chal_VoucherURL`, `Chal_LogoURL`,`CRCf_XK_IdMediaType`, `Chal_AllowGuests` FROM `VUsersChallenges` A, `TChallengeResultsConfig` B WHERE `Chal_PK_IdChallenge`=`User_PK_IdUser` and `Chal_PK_IdChallenge`=`CRCf_XK_IdChallenge` and `User_PK_IdUser`="+usrID;
            String sql = "";
            if (usrID < 0) {
                sql = "SELECT DISTINCT `Chal_PK_IdChallenge`, `Chal_Title`, `Chal_Title_ENG`, `Chal_Description`, `Chal_Description_ENG`, `Chal_BeginDate`, `Chal_EndDate`, `Chal_VoucherURL`, `Chal_LogoURL`,`CRCf_XK_IdMediaType`,`Chal_Tags` FROM `TChallenge` A, `TChallengeResultsConfig` B  WHERE Chal_EndDate >= CURDATE() and `Chal_PK_IdChallenge`=`CRCf_XK_IdChallenge` and `Chal_AllowGuests`=1";
            } else {
                sql = "SELECT DISTINCT * FROM (SELECT DISTINCT `Chal_PK_IdChallenge` , `Chal_Title` , `Chal_Title_ENG` , `Chal_Description` , `Chal_Description_ENG` , `Chal_BeginDate` , `Chal_EndDate` , `Chal_VoucherURL` , `Chal_LogoURL` ,  `CRCf_XK_IdMediaType` , `Chal_Tags` FROM  `TUsers` A, `TChallenge` B, `TChallengeInvites` C, `TChallengeResultsConfig` D WHERE Chal_EndDate >= CURDATE() AND A.`User_PK_IdUser` =  " + usrID + " AND A.`User_Email` = C.`CInv_UserEmail` AND A.`User_FK_IdTypeUser`=3 AND B.`Chal_PK_IdChallenge` = C.`CInv_XK_IdChallenge` AND B.`Chal_PK_IdChallenge` = `CRCf_XK_IdChallenge` UNION ALL SELECT DISTINCT A.`Chal_PK_IdChallenge` , A.`Chal_Title` ,A.`Chal_Title_ENG` , A.`Chal_Description` ,A.`Chal_Description_ENG` , A.`Chal_BeginDate` , A.`Chal_EndDate` , A.`Chal_VoucherURL` , A.`Chal_LogoURL` ,  `CRCf_XK_IdMediaType` , A.`Chal_Tags` FROM  `VUsersChallenges` A, `TChallengeResultsConfig` B,  `TChallenge` C  WHERE  A.Chal_EndDate >= CURDATE() AND A.`Chal_PK_IdChallenge` =  `CRCf_XK_IdChallenge` AND  `User_PK_IdUser` =  " + usrID + " AND C.`Chal_PK_IdChallenge` =  `CRCf_XK_IdChallenge` ORDER BY `Chal_PK_IdChallenge`) AS A WHERE Chal_PK_IdChallenge NOT IN (SELECT DISTINCT `CRCf_XK_IdChallenge` FROM `TChallengeResults` WHERE `CRCf_XK_IdUser`=" + usrID + ")";
            }

            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);
            int challID = -1;
            String resString = "";
            while (rs.next()) {
                if (challID == rs.getInt(1)) {
                    resString += "," + rs.getInt(10);
                } else {
                    if (!resString.equals("")) {
                        results.add(resString + "]");
                    }
                    resString = rs.getInt(1) + "#" + rs.getString(2) + "#" + rs.getString(3) + "#" + rs.getString(4) + "#" + rs.getString(5) + "#" + rs.getString(6) + "#" + rs.getString(7) + "#" + rs.getString(8) + "#" + rs.getString(9) + "#" + rs.getString(11) + "#[" + rs.getString(10);
                    challID = rs.getInt(1);
                }
            }
            if (!resString.equals("") && !results.contains(resString)) {
                results.add(resString + "]");
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }

        return results;
    }

    public String getUserEmail(String usrID) {
        String email = "";
        try {
            String sql = "SELECT * FROM `TUsers` WHERE `User_PK_IdUser`=" + usrID;
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);
            while (rs.next()) {
                email = rs.getString(4);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
        return email;
    }

    public String getVoucherURL(String chID) {
        String url = "";
        try {
            String sql = "SELECT * FROM `TChallenge` WHERE `Chal_PK_IdChallenge`=" + chID;
            ResultSet rs = DatabaseConn.getInstance().dbQuery(sql);
            while (rs.next()) {
                url = rs.getString(10);
            }
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex);
        }
        return url;
    }

    //TEST METHOD
    public static void main(String[] args) throws Exception {
        System.out.println(stripAccents("Ensino pós-secundário não superior"));
        //DatabaseConn.getInstance();
        //System.out.println(DBServices.getInstance().DB_MEDIATYPES);
        //System.out.println(DBServices.getInstance().getTargetWordsByChalengeID(1, 4));
    }

    public static String stripAccents(String s) {

        //s = Normalizer.normalize(s, Normalizer.Form.NFD);
        //s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        if (s == null) {
            return "";
        }
        String resultString = s.replaceAll("[^A-Za-z0-9]", "");
        return resultString;
    }
}
